__author__ = 'theo'


class Shell(object):


    WHITE = 255, 255, 255
    GREEN = 0, 102, 0
    PURPLE = 158, 0, 158

    COLORS = WHITE, GREEN, PURPLE

    NUMBER_OF_SHELLS = 16

    RADIUS = 60

    SHELL_COORD = {
        1: [1250,200],
        2: [1550,200],
        3: [1250,700],
        4: [1550,700],
        5: [1850,700],
        6: [1450,900],
        7: [1650,1200],
        8: [1825,75],
        9: [1925,75],
        10: [1925,175],

        100: [1550,1500],
        200: [1850,1500],

        11: [1250,2800],
        12: [1550,2800],
        13: [1250,2300],
        14: [1550,2300],
        15: [1850,2300],
        16: [1450,2100],
        17: [1650,1800],
        18: [1825,2925],
        19: [1925,2925],
        20: [1925,2825],
    }

    def __init__(self, x_coord, y_coord, color):
        self.x = x_coord
        self.y = y_coord
        self.color = color

    @staticmethod
    def get_map_shell(map_nb=1):
        map_shell = None
        if map_nb == 1:
            map_shell = {1:Shell.COLORS[0],
                2:Shell.COLORS[0],
                6:Shell.COLORS[2],
                7:Shell.COLORS[1],
                8:Shell.COLORS[2],
                9:Shell.COLORS[2],
                10:Shell.COLORS[2],
                100:Shell.COLORS[0],
                200:Shell.COLORS[0],
                11:Shell.COLORS[0],
                12:Shell.COLORS[0],
                16:Shell.COLORS[1],
                17:Shell.COLORS[2],
                18:Shell.COLORS[1],
                19:Shell.COLORS[1],
                20:Shell.COLORS[1],
            }
        elif map_nb == 2:
            map_shell = {1:Shell.COLORS[2],
                2:Shell.COLORS[0],
                6:Shell.COLORS[2],
                7:Shell.COLORS[2],
                8:Shell.COLORS[2],
                9:Shell.COLORS[0],
                10:Shell.COLORS[2],
                100:Shell.COLORS[0],
                200:Shell.COLORS[0],
                11:Shell.COLORS[1],
                12:Shell.COLORS[0],
                16:Shell.COLORS[1],
                17:Shell.COLORS[1],
                18:Shell.COLORS[1],
                19:Shell.COLORS[0],
                20:Shell.COLORS[1],
            }
        elif map_nb == 3:
            map_shell = {1:Shell.COLORS[2],
                2:Shell.COLORS[0],
                3:Shell.COLORS[2],
                4:Shell.COLORS[0],
                7:Shell.COLORS[2],
                8:Shell.COLORS[2],
                9:Shell.COLORS[0],
                10:Shell.COLORS[2],

                11:Shell.COLORS[1],
                12:Shell.COLORS[0],
                13:Shell.COLORS[1],
                14:Shell.COLORS[0],
                17:Shell.COLORS[1],
                18:Shell.COLORS[1],
                19:Shell.COLORS[0],
                20:Shell.COLORS[1],
            }
        elif map_nb == 4:
            map_shell = {1:Shell.COLORS[2],
                2:Shell.COLORS[2],
                3:Shell.COLORS[2],
                4:Shell.COLORS[0],
                7:Shell.COLORS[0],
                8:Shell.COLORS[1],
                9:Shell.COLORS[0],
                10:Shell.COLORS[1],

                11:Shell.COLORS[1],
                12:Shell.COLORS[1],
                13:Shell.COLORS[1],
                14:Shell.COLORS[0],
                17:Shell.COLORS[0],
                18:Shell.COLORS[1],
                19:Shell.COLORS[0],
                20:Shell.COLORS[1],
            }
        elif map_nb == 5:
            map_shell = {1:Shell.COLORS[2],
                2:Shell.COLORS[2],
                3:Shell.COLORS[2],
                4:Shell.COLORS[1],
                5:Shell.COLORS[0],
                8:Shell.COLORS[2],
                9:Shell.COLORS[0],
                10:Shell.COLORS[0],

                11:Shell.COLORS[1],
                12:Shell.COLORS[1],
                13:Shell.COLORS[1],
                14:Shell.COLORS[2],
                15:Shell.COLORS[0],
                18:Shell.COLORS[1],
                19:Shell.COLORS[0],
                20:Shell.COLORS[0],
            }

        return map_shell