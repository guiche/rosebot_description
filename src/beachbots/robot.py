import rosebotnav
import math


class Robot(rosebotnav.robot.Robot):

    #__attr_reinit__ = 'buts_',

    def __init__(self, *args, **kwargs):

        taille = kwargs.get('taille', 190)
        profondeur_av = kwargs.pop('profondeur_av', 72.5)
        profondeur_ar = kwargs.pop('profondeur_ar', 152.5)
        super(Robot, self).__init__(*args,
                                    taille  = taille,  #demi largeur - largeur physique : 35cm
                                    profondeur_av = profondeur_av,  # demi profondeur axe des roues - avant, hors bras
                                    profondeur_ar = profondeur_ar,  # demi profondeur axe des roues - arriere
                                    **kwargs)

        self.servo_bras = 0

    def dessineContour(self, **kwargs):
        # affiche la boite de deplacement.

        super(Robot, self).dessineContour(**kwargs)

        trait = kwargs.get('trait',0)
        sideRadius = kwargs.get('sideRadius')

        if trait == 0 and sideRadius is not None:

            # dessin des bras

            taille_bras = 100
            orient = kwargs['orient']
            pos = kwargs['pos']

            largeur = orient.nml * sideRadius
            devant = pos + orient * self.profondeur_av

            angle_bras = self.servo_bras * math.pi / 180. - math.pi / 2
            for pt, angle in (devant + largeur, angle_bras), \
                             (devant - largeur, -angle_bras):

                self.racine.dessineLigne(pt, pt + taille_bras * self._orient_.rot(angle), couleur=couleur, trait=7)
