from rosebotnav import mobile
from tricarto.tryangle.sommet import SommetBorne, Vec, Connecte
import rosebotnav
from . import robot

class Coquillage(mobile.EtreMobile):

    def __init__(self, couleur, **kwargs):
        mobile.EtreMobile.__init__(self, **kwargs)

        self.couleur = couleur

        self._visco_ = 5.

        #self.coeff_glisse = .1

    def Afficher(self):
        self.racine.dessineCercle(self.pos, self.rayon, self.couleur, trait=0)


class Partie(rosebotnav.racine.Partie):

    POS_DEPART = 850, 220
    ROBOT_CLASS = robot.Robot

    def __init__(self, *args, **kwargs):
        super(Partie, self).__init__(*args, **kwargs)

        self.configCoquillages = 1
        self.coquillages = []

        self.image_fond = 'beachbots.PNG'

    def PaintBackground(self):

        rosebotnav.Partie.PaintBackground(self)

        self.dessinePositionsCoquillages()

    def peupler(self, vide=True, fond=True):
        from tricarto.tryangle import Reseau, toPointList, sommet
        from rosebotnav.formation import Camp
        self.graphe = Reseau(nom="beachbots", exact=False)

        # Sommets ajoutes pour initialiser le graphe avec un triangle de sorte que les autres puissent etre ajoutes
        # incrementalement
        marge = 1000
        cadreExterieur = [(-marge, -marge), (2000 + marge, -marge), (2000 + marge, 3000 + marge), (-marge, 3000 + marge)]

        parevent = [(750, 900), (750 + 22, 900), (750 + 22, 900 + 576), (1350, 1476), (1350, 1476 + 48), (772, 1524),
                    (772, 2100), (750, 2100)]

        doigtFilet = [(2000, 928), (1978, 928), (1978, 950), (2000, 950)]
        doigtFilet2 = [(x, 3000 - y) for (x, y) in doigtFilet]

        doigt = [(0, 810), (200, 810), (200, 832), (0, 832)]
        doigt2 = [(x, 3000 - y) for (x, y) in doigt]

        # -(1,-1) plutot que (0,0): Robustification contre l'insertion superposee du robot au point 0,0
        coords = [(-1, -1), ] + \
                 [(2000, 0), ] + doigtFilet + doigtFilet2 + \
                 [(2000, 3000), ] + \
                 [(0, 3000), ] + doigt2 + list(reversed(doigt))

        Bornes = self.graphe.AddVertices(toPointList(cadreExterieur + coords + parevent))

        for Borne in Bornes:
            self.graphe.VertexToWertex[Borne] = SommetBorne(Borne, reseau=self.graphe)

        Connecte(Bornes[:4], cloture=True)
        Connecte(Bornes[4:-len(parevent)], cloture=True)
        Connecte(Bornes[-len(parevent):], cloture=True)

        self.graphe.ResetDual()
        # searchDual.resetConvexAreas()

        CampViolet = Camp.getCamp('Violet', couleur=(255, 0, 255))
        CampVert = Camp.getCamp('Vert', couleur=(0, 255, 0))
        CampBlanc = Camp.getCamp(Camp.CAMP_NEUTRE, couleur=(240, 250, 250))

        self.Camps = CampViolet, CampVert, CampBlanc

        # Robot camp jaune
        x0, y0 = self.POS_DEPART
        robot = self.ROBOT_CLASS(Reseau=self.graphe, pos=(x0, y0), Orient0=Vec(0, 1), camp=CampViolet, nom="Rosebot")

        robot.Camps = CampViolet, CampVert, CampBlanc
        self.Selection = set([robot])

        if not vide:
            # Robot adverse
            self.ROBOT_CLASS(Reseau=self.graphe, pos=(x0, 3000 - y0), Orient0=Vec(0, -1), camp=CampVert, nom="RobotAdverse1")

        sommet.Wertex.surrayon = 1

        self.graphe.CheckMesh()

        if self.Affiche:
            self.DimEcran = Vec(550, 700)

            if fond:
                self.image_echelle = 2.10
                self.image_pos[0] = -125
                self.image_pos[1] = -120

            self.VueOrg = Vec(-50, 3100)
            self.VueRatio = .225
            # coordonnees reeles de l'aire visible si elle est limitee
            self.VueLimites = Vec(-50, -100), Vec(2300, 3100)

            # self.insereCoquillages(1)

    def insereCoquillages(self, configIdx):
        from beachbots.shell import Shell
        shells = Shell.get_map_shell(configIdx)

        self.configCoquillages = configIdx

        if self.coquillages:
            for coquille in self.coquillages:
                coquille.Delete()

            self.coquillages = []

        if shells is not None:
            for posIdx in shells:
                pos = Shell.SHELL_COORD[posIdx]
                couleur = shells[posIdx]

                coquillage = Coquillage(couleur,
                                        Reseau=self.graphe,
                                        pos=pos,
                                        nom="Coqui%d" % posIdx,
                                        taille=Shell.RADIUS)

                coquillage.racine = self

                self.coquillages.append(coquillage)


    def dessinePositionsCoquillages(self):
        from beachbots.shell import Shell
        shells = Shell.get_map_shell(self.configCoquillages)

        if shells is not None:

            for posIdx in shells:
                pos = Shell.SHELL_COORD[posIdx]
                couleur = shells[posIdx]

                self.dessineCercle(pos, Shell.RADIUS, couleur=couleur, trait=15)
