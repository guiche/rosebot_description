import rosebotnav
from . import plateau

def main():
    rosebotnav.main(plateau.Partie)

if __name__ == '__main__':
    main()
