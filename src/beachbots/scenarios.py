from strategy import ActionSequence, Strategy, ActuatorItem, PositionItem
import copy
from math import pi

tol_default = 1
tol_o_default = 0.05

sequencePousseChateau = ActionSequence("PousseChateau",
                                       [
                                           ActuatorItem(11, 5), # Open latches
                                            PositionItem(900, 430, pi/4., tol=100.0, tol_o=0.30,avAr=True),
                                            PositionItem(790, 250, pi/4., tol=100.0, tol_o=0.30,avAr=False),
                                           
                                            #PositionItem(1050, 800, pi/2., tol= 5.0*tol_default, tol_o = 4.0*tol_o_default),
                                            PositionItem(1100, 1220, pi/2., tol=45.0, tol_o=0.15),
                                            #ActuatorItem(11, 67), # Open latches
                                           
                                            PositionItem(1100, 475, 0., tol=25.0, tol_o=0.1, ),
                                            #ActuatorItem(11, 20), # Open latches
                                           ActuatorItem(11, 130), # close latches
                                       ],
                                       timeout=40)

sequenceCoquillage = ActionSequence("Coquillages",
                                    [
                                        PositionItem(1500, 1300, 4.71, tol=25.0, tol_o=0.1),
                                        PositionItem(1500,  410, 4.71, tol=25.0, tol_o=0.1, ),
                                        PositionItem(910,  110, 4.71, tol=25.0, tol_o=0.1)
                                    ],
                                    timeout=20)


sequenceCabine1 = ActionSequence("Cabine1",
                                 [
                                     PositionItem(1100, 600, 0., tol=25.0, tol_o=0.1),
                                     PositionItem(150, 600, 0., tol=25.0, tol_o=0.05, avAr=False),
                                     PositionItem(1100, 600, 0., tol=25.0, tol_o=0.1),
                                     PositionItem(1100, 350, 0., tol=25.0, tol_o=0.1)
                                 ],
                                 timeout=20)
                                 
sequenceCabine2 = ActionSequence("Cabine2",
                                 [
                                     PositionItem(1100, 350, 0., tol=25.0, tol_o=0.1),
                                     PositionItem(150, 350, 0., tol=25.0, tol_o=0.05, avAr=False),
                                     PositionItem(1100, 350, 0., tol=25.0, tol_o=0.1),
                                     PositionItem(1100, 350, 0., tol=25.0, tol_o=0.1)
                                 ],
                                 timeout=20)
                                 
sequenceTest = ActionSequence("Test",
                                 [
                                     PositionItem(550, 450, pi/2. ,tol=100.0 ),
                                     PositionItem(1450, 450, pi/2., tol=25.0, tol_o=0.1, avAr=True),
                                     PositionItem(600, 450, pi/2., tol=100.0 ),
                                     PositionItem(910, 450, pi/2., tol=25.0, tol_o=0.1, avAr=False ),
                                     PositionItem(910, 160, pi/2., tol=25.0, tol_o=0.1, avAr=False ),
                                     
                                 ],
                                 timeout=90)                                

SEQUENCES = (
    #sequenceTest,
    sequencePousseChateau,
    sequenceCabine1,
    sequenceCabine2,
    
    # sequenceCoquillage,
    # strategy_to.sequence_TO
    )

__strategie_partie = Strategy(*SEQUENCES)


def initStrategy(send_command_servo_service):
    strategie_partie = copy.deepcopy(__strategie_partie)
    # add send_command_servo_service callback
    strategie_partie.send_command_servo_service = send_command_servo_service
    return strategie_partie
