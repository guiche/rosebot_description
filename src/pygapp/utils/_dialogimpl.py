'''
Created on 21 May 2014

@author: j913965
'''

import bisect
import itertools
import os
import re
import sys
import wx
import wx.lib.buttons
import wx.lib.customtreectrl as customtreectrl
import wx.lib.hyperlink
import wx.lib.mixins.listctrl
import wx.lib.mixins.treemixin
import wx.lib.scrolledpanel
from   wx.lib.stattext import GenStaticText
from .func import partial

# staging
USE_AGW_STYLE = 'agwStyle' in customtreectrl.CustomTreeCtrl.__init__.__code__.co_varnames

def getIcon(icon):
    """
    Creates a wx.Bitmap from one of these constants:
        wx.ICON_INFORMATION
        wx.ICON_ERROR
        wx.ICON_EXCLAMATION
        wx.ICON_QUESTION

    """
    if isinstance(icon, int):
        icon = {
            wx.ICON_INFORMATION: '/app/icons/info48.png',
            wx.ICON_ERROR      : '/app/icons/error48.png',
            wx.ICON_EXCLAMATION: '/app/icons/warning48.png',
            wx.ICON_QUESTION   : '/app/icons/question48.png',
        }.get(icon, icon)

    if isinstance(icon, int):
        art = {
            wx.ICON_ERROR      : wx.ART_ERROR,
            wx.ICON_EXCLAMATION: wx.ART_WARNING,
            wx.ICON_QUESTION   : wx.ART_QUESTION,
        }.get(icon)
        bmp = art is not None and wx.ArtProvider_GetBitmap(art) or None
    elif isinstance(icon, wx.Bitmap):
        bmp = icon
    else:
        bmp = None

    return bmp

class _MyThemedGenBitmapButton(wx.lib.buttons.ThemedGenBitmapButton):
    def DrawBezel(self, dc, x1, y1, x2, y2):
        """ Overiding to improve focus highlight behavior """
        rect = wx.Rect(x1, y1, x2, y2)
        state = not self.up and wx.CONTROL_PRESSED or 0
        if self.GetClientRect().Contains(self.ScreenToClient(wx.GetMousePosition())):
            state = wx.CONTROL_CURRENT
        elif not self.IsEnabled():
            state = wx.CONTROL_DISABLED
        wx.RendererNative.Get().DrawPushButton(self, dc, rect, state)



## Applications should use ScrolledMessage instead.  ScrolledMessageDialog requires
## wx specific code
class _ScrolledMessageDialog(wx.Dialog):

    def __init__(self, parent, title='', pos=wx.DefaultPosition, size=wx.DefaultSize,
                 msg='', font=None, hscroll=False, style=wx.DEFAULT_DIALOG_STYLE,
                 ButtonStyle=wx.OK, IconStyle=None, header='',
                 readOnly=True, headerFont=None ):
        wx.Dialog.__init__(self, parent, -1, title, pos, size, style=style)

        #set icons
        bmp = getIcon(IconStyle)

        if bmp:
            stabmp = wx.StaticBitmap(self, wx.NewId(), bmp, (-1, -1), bmp.GetSize(), wx.NO_BORDER)

        #buttons sizers
        sizer3 = wx.BoxSizer(wx.HORIZONTAL)
        b = 10
        if ButtonStyle & wx.YES_NO == wx.YES_NO:
            butYES = wx.Button(self, wx.ID_YES)
            butNO = wx.Button(self, wx.ID_NO)
            sizer3.Add(butYES, 0, wx.RIGHT, b)
            sizer3.Add(butNO, 0, wx.RIGHT, b)
            butYES.Bind(wx.EVT_BUTTON, self.OnClickYES)
            butNO.Bind(wx.EVT_BUTTON, self.OnClickNO)

        elif ButtonStyle & wx.OK == wx.OK:
            butOK = wx.Button(self, wx.ID_OK)
            sizer3.Add(butOK, 0, wx.RIGHT, b)
        else:
            pass

        if ButtonStyle & wx.CANCEL == wx.CANCEL:
            butCANCEL = wx.Button(self, wx.ID_CANCEL)
            sizer3.Add(butCANCEL, 0, border=b)

        #set a default button
        #some problem with wx.YES_NO, wx.YES_DEFAULT = 0 !
        if ButtonStyle & wx.YES_NO == wx.YES_NO:
            if ButtonStyle & wx.NO_DEFAULT == wx.NO_DEFAULT:
                butNO.SetDefault()
                butNO.SetFocus()
            elif ButtonStyle & wx.YES_DEFAULT == wx.YES_DEFAULT:
                butYES.SetDefault()
                butYES.SetFocus()
            else:
                pass
        if ButtonStyle & wx.YES_NO != wx.YES_NO and ButtonStyle & wx.OK == wx.OK:
            butOK.SetDefault()
            butOK.SetFocus()
        if ButtonStyle & wx.CANCEL == wx.CANCEL:
            butCANCEL.SetDefault()
            butCANCEL.SetFocus()

        #the text control, use rich text so we can display more than 64k
        sty = wx.TE_MULTILINE | wx.TE_LEFT | wx.TE_RICH
        if readOnly:
            sty |= wx.TE_READONLY
        if hscroll:
            sty |= wx.TE_DONTWRAP | wx.HSCROLL
        self.txt = wx.TextCtrl(self, wx.NewId(), msg, (-1, -1), (-1, -1),sty)

        if font:
            self.txt.SetFont(font)


        #layout
        sizer2 = wx.BoxSizer(wx.VERTICAL)

        sizer1 = wx.BoxSizer(wx.HORIZONTAL)

        b = 5
        if bmp:
            sizer1.Add(stabmp, 0, wx.BOTTOM | wx.LEFT | wx.TOP | wx.RIGHT, b)

        if header:
            hdr = wx.StaticText(self, -1, header)
            sizer1.Add( hdr, 0, wx.BOTTOM|wx.LEFT|wx.TOP, 5 )
            if headerFont:
                hdr.SetFont( headerFont )

        sizer2.Add(sizer1, 0, wx.GROW | wx.LEFT | wx.RIGHT | wx.TOP, b)

        sizer2.Add(self.txt, 1, wx.BOTTOM | wx.GROW, b)
        sizer2.Add(sizer3, 0, wx.BOTTOM | wx.ALIGN_CENTER, b)

        self.SetSizer(sizer2)

        #for escape key
        self.Bind(wx.EVT_CHAR_HOOK, self.OnCharHook)

    def OnClickYES(self, event):
        if self.IsModal():
            self.EndModal(wx.ID_YES)
        else:
            self.Destroy()

    def OnClickNO(self, event):
        if self.IsModal():
            self.EndModal(wx.ID_NO)
        else:
            self.Destroy()

    def OnCharHook(self, event):
        if event.KeyCode == wx.WXK_ESCAPE:
            self.EndModal(wx.ID_CANCEL)
        else:
            event.Skip()


class _MultiChoiceTreeCtrl(customtreectrl.CustomTreeCtrl, wx.lib.mixins.treemixin.TreeHelper):
    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, items={}, boldParents=True,
                 initialSelection=None,
                 style=0,
                 size=wx.DefaultSize,
                 searchable=False,
                 agwStyle=0,
                 initialExpansion=()
                 ):

        if USE_AGW_STYLE:
            agwStyle |= customtreectrl.TR_HAS_BUTTONS|customtreectrl.TR_HIDE_ROOT|customtreectrl.TR_AUTO_CHECK_CHILD
            style     = wx.BORDER_THEME
            customtreectrl.CustomTreeCtrl.__init__(self, parent, id, pos, size, style, agwStyle)
        else:
            style |= wx.BORDER_THEME|customtreectrl.TR_HAS_BUTTONS|customtreectrl.TR_HIDE_ROOT
            style |= customtreectrl.TR_AUTO_CHECK_CHILD
            customtreectrl.CustomTreeCtrl.__init__(self, parent, id, pos, size, style)
        wx.lib.mixins.treemixin.TreeHelper.__init__(self)
        self.root = self.AddRoot("The Root Item")
        self.boldParents = boldParents

        initialSelection = initialSelection or []
        if initialSelection == 'ALL':
            self.initialSelection = []
        else:
            self.initialSelection = list(initialSelection)
            for i,item in enumerate(self.initialSelection):
                if isinstance(item, list):
                    self.initialSelection[i] = tuple(item)
            self.initialSelection = set(self.initialSelection)

        self.items = items
        self.addItems( self.root, items, selection=initialSelection, expanded=initialExpansion )

        if initialSelection == 'ALL':
            self.CheckChilds( self.root, True )

        if searchable:
            self.searchtxt = wx.SearchCtrl(parent, size=(200,-1))
            self.searchtxt.Bind(wx.EVT_TEXT, self.OnSearchText)
            self.searchtxt.ShowCancelButton(True)
            self.searchtxt.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.OnSearchCancel)
            #bmp = lib.image.getImage( '/app/icons/16x16/fugue/magnifier.png' )
            #self.searchtxt.SetSearchBitmap( bmp )
            #bmp = lib.image.getImage( '/app/icons/16x16/silk/cross.png' )
            #bmp = bmp.ConvertToImage().ConvertToGreyscale().ConvertToBitmap()
            #self.searchtxt.SetCancelBitmap( bmp )
            txtwin = [x for x in self.searchtxt.GetChildren() if isinstance(x, wx.TextCtrl)][0]
            txtwin.Bind(wx.EVT_KEY_DOWN, self.OnSearchKey)
            parent.dlgsizer.Add(self.searchtxt, 0, wx.EXPAND | wx.TOP, 10)
            
    def OnSearchCancel(self, evt):
        self.searchtxt.Clear()
        evt.Skip()

    def OnSearchKey(self, evt):
        ## TODO: implement
        evt.Skip()

    def OnSearchText(self, evt):
        txt = self.searchtxt.GetValue()
        ## mixed case, use case sensitive compare
        expr = re.compile( txt, flags = ( txt.islower() and re.IGNORECASE or 0 ))

        self.DeleteChildren( self.root )
        self.addItems( self.root, self.items, match=expr )
        ## todo recheck selected
        
        self.searchtxt.Refresh()

        ## todo reset selection

        evt.Skip()

    def addItems_i( self, items, path=(), selection=(), match=None ):
        '''return a nested dictionary of items to add after applying match criteria.
        If a leaf matches, then we will include the parents of the leaf.'''
        itemsToAdd = []
        for key in sorted(items):
            thispath = tuple(path)+(key,)
            isMatched = True if match is None else match.search( key )

            children = None
            if isinstance(items, dict):
                children = self.addItems_i( items[key], thispath, selection, match )

            if children is not None or isMatched:
                itemsToAdd.append( { 'key': key, 'path': thispath, 'children': children } )

        if not itemsToAdd: return None
        res = { 'items': itemsToAdd, 'len': len( items ) }

        return res

    def addItems( self, root, items, path=(), selection=(), match=None, expanded=() ):
        '''Add all the items applying any match/selection/expansion criterea.'''
        def add( root, data ):
            '''returns true if all items are selected under this root'''
            allChecked = True
            for item in data[ 'items' ]:
                child = self.AppendItem( root, item[ 'key' ], ct_type=1 )
                self.SetPyData( child, item[ 'path' ] )

                check = False
                if item[ 'children' ]:
                    if self.boldParents:
                        self.SetItemBold( child )
                    check = add( child, item[ 'children' ] )

                if item[ 'path' ] in selection or item[ 'key' ] in selection or check:
                    self.CheckItem( child, True )
                else:
                    allChecked = False

                if item['path'] in expanded or match is not None:
                    self.Expand( child )

            return allChecked if len( data[ 'items' ] ) else False

        # apply any match criterea to the items to whittle down the list
        allItems = self.addItems_i( items, path, selection, match )
        if allItems:    # if we have results add 'em
            if add( root, allItems ):
                self.CheckItem( root, True )

    def HasCheckedItems(self):
        """
        Returns True if any items are checked
        """
        for item in self.GetItemChildren(self.root, recursively=True):
            if self.IsItemChecked(item):
                return True
        return False


    def GetCheckedItems(self, returnLeaves=False):
        """
        Returns a list of lists [ (parent0, parent1, ..., child), ...]

        returnLeaves: If True, don't return a list of lists, just return a list of leaf values
        """
        sel = []
        for item in self.GetItemChildren(self.root, recursively=True):
            if self.IsItemChecked(item):
                if returnLeaves:
                    if not self.ItemHasChildren(item):
                        sel.append(self.GetItemText(item))
                else:
                    sel.append(self.GetPyData(item))
        return sel


class _VListCtrl(wx.ListCtrl, wx.lib.mixins.listctrl.ListCtrlAutoWidthMixin):
    def __init__(self, parent, id=-1, pos=wx.DefaultPosition, size=wx.DefaultSize, style=0,
                 validator=wx.DefaultValidator, name=wx.ListCtrlNameStr, choices=()):
        wx.ListCtrl.__init__( self, parent, id, pos, size,
                              style | wx.LC_SINGLE_SEL | wx.LC_REPORT | wx.LC_NO_HEADER | wx.LC_VIRTUAL,
                              validator, name )
        wx.lib.mixins.listctrl.ListCtrlAutoWidthMixin.__init__( self )

        self._normalAttr   = wx.ListItemAttr()
        self._selectedAttr = wx.ListItemAttr()
        self._selectedAttr.SetTextColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_HIGHLIGHTTEXT ) )
        self._selectedAttr.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_HIGHLIGHT ) )
        self.Bind( wx.EVT_KILL_FOCUS, lambda evt: self.Select( self.GetFirstSelected(), 0 ) )

        self.InsertColumn( 0, 'Data' )
        self.SetItems( choices )

    def OnGetItemText(self, row, col):
        return self._items[ row ]

    def OnGetItemAttr(self, row):
        return self._selectedAttr if row == self.GetFocusedItem() else self._normalAttr

    def GetItems(self):
        return self._items

    def SetItems(self, items):
        self._items = items
        self.SetItemCount( len(items) )
        self.RefreshItems( 0, len(items)-1 )

    Items = property( GetItems )

    def GetSelection(self):
        return self.GetFocusedItem()

    def SetSelection(self, row):
        self.Select( self.GetFirstSelected(), 0 )
        self.Focus( row )
        self.EnsureVisible( row )

# Compatibility
_VListCtrl.GetCount = _VListCtrl.GetItemCount
_VListCtrl.GetString = _VListCtrl.GetItemText


class _VCheckListCtrl(_VListCtrl):
    def __init__(self, *args, **kwargs):
        _VListCtrl.__init__( self, *args, **kwargs )
        imgSize         = (15, 15)
        self._imagelist = wx.ImageList( *imgSize )
        self._imagelist.Add( self._createBitmap( 0, imgSize ) )
        self._imagelist.Add( self._createBitmap( wx.CONTROL_CHECKED, imgSize ) )
        self.SetImageList( self._imagelist, wx.IMAGE_LIST_SMALL )
        self.Bind( wx.EVT_LEFT_DOWN, self._onLeftDown )

    def _createBitmap(self, flag, size):
        bmp = wx.EmptyBitmap(*size)
        dc  = wx.MemoryDC(bmp)
        dc.Clear()
        wx.RendererNative.Get().DrawCheckBox(self, dc,
                                             (0, 0, size[0], size[1]), flag)
        dc.SelectObject(wx.NullBitmap)
        return bmp

    def _onLeftDown(self, evt):
        index, flags = self.HitTest( evt.GetPosition() )
        if flags == wx.LIST_HITTEST_ONITEMICON:
            self.Toggle( index )
            evt = wx.CommandEvent( wx.wxEVT_COMMAND_CHECKLISTBOX_TOGGLED, self.GetId() )
            evt.SetInt( index )
            evt.SetEventObject( self )
            self.GetEventHandler().ProcessEvent( evt )
        else:
            evt.Skip()

    def OnGetItemImage(self, row):
        return self._checked[ row ]

    def SetItems(self, items):
        self._checked = bitarray( (0,) ) * len(items)
        _VListCtrl.SetItems( self, items )

    def IsChecked(self, row):
        return self._checked[ row ]

    def Check(self, row, flag=True):
        self._checked[ row ] = flag
        self.RefreshItem( row )

    def Toggle(self, row):
        self._checked[ row ] ^= 1
        self.RefreshItem( row )

# Staging: only CORE for now
NEW_CHOICE_DIALOG = int( os.environ.get( 'ATHENA_DEVELOPER', '0' ) )

class _ChoiceDialogI(wx.Dialog):
    '''
    Internal class for a multi choice dialog with a clear button and search field.
    Used by Choice()
    '''
    def __init__(self, parent, msg, title, lst, pos = wx.DefaultPosition,
                 size = (300,380), style = wx.CHOICEDLG_STYLE|wx.RESIZE_BORDER,
                 enableSelectAll=False, searchable=True, multichoice=True,
                 sortByRelevance=False, extraButtons=[], checkBoxes=None,
                 filterCheckBoxes=None, multichoiceItemDClickCallback=None,
                 enableSearchByCaps=False, filterCheckColumns=1, concatchoice=False, strictSearch=False
    ):
        wx.Dialog.__init__(self, parent, -1, title, pos, size, style)

        x, y = pos
        if x == -1 and y == -1:
            self.CenterOnScreen(wx.BOTH)
        self.choices = lst
        self.chosen = False
        self.filteredChoices = lst
        self.multichoice = multichoice
        self.concatchoice = False if multichoice else concatchoice
        self.selected = set()
        self.sortByRelevance = sortByRelevance
        self.statustxt = None
        self.extraButtons = extraButtons
        self.returnVal = None
        self.clearbtn = None
        self.inSelectionTab = True
        self.enableSearchByCaps = enableSearchByCaps
        self.strictSearch = strictSearch

        ## force to the front on display, for some reason needed
        ## in rtmkt
        self.Bind(wx.EVT_SHOW, self.OnShowRaise)

        stat = wx.StaticText(self, -1, msg)

        EVT = wx.EVT_LIST_ITEM_ACTIVATED if NEW_CHOICE_DIALOG else wx.EVT_LISTBOX_DCLICK
        if multichoice:
            cls = _VCheckListCtrl if NEW_CHOICE_DIALOG else wx.CheckListBox
            self.lbox = cls(self, -1, choices=lst)
            self.Bind(wx.EVT_CHECKLISTBOX, self.OnItemToggle, self.lbox)
            if multichoiceItemDClickCallback:
                self.Bind(EVT, multichoiceItemDClickCallback, self.lbox)
        else:
            #cls = _VListCtrl if NEW_CHOICE_DIALOG else partial( wx.ListBox, style=wx.LB_SINGLE )
            self.lbox = cls(self, -1, choices=lst)
            self.Bind(EVT, self.OnItemDClick, self.lbox)

        dlgsizer = wx.BoxSizer(wx.VERTICAL)
        dlgsizer.Add(stat, 0, wx.ALL, 10)

        dlgsizer.Add(self.lbox, 1, wx.EXPAND | wx.LEFT|wx.RIGHT, 10)

        if searchable:
            self.searchtxt = wx.SearchCtrl(self, size=(200,-1), style=wx.TE_PROCESS_ENTER)
            self.searchtxt.Bind(wx.EVT_TEXT, self.OnSearchText)
            self.searchtxt.ShowCancelButton(True)
            self.searchtxt.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.OnSearchCancel)
            #bmp = lib.image.getImage( '/app/icons/16x16/fugue/magnifier.png' )
            #self.searchtxt.SetSearchBitmap( bmp )
            #bmp = lib.image.getImage( '/app/icons/16x16/silk/cross.png' )
            #bmp = bmp.ConvertToImage().ConvertToGreyscale().ConvertToBitmap()
            #self.searchtxt.SetCancelBitmap( bmp )
            txtwin = [x for x in self.searchtxt.GetChildren() if isinstance(x, wx.TextCtrl)][0]
            txtwin.Bind(wx.EVT_KEY_DOWN, self.OnSearchKey)
            dlgsizer.Add(self.searchtxt, 0, wx.EXPAND | wx.LEFT|wx.RIGHT|wx.TOP, 10)
            #ui.task_queue_staging.post(self.searchtxt.SetFocus)

        checksizer = wx.GridSizer( rows=0, cols=filterCheckColumns, vgap=10 )
        if self.concatchoice:
            self.chosen = wx.TextCtrl(self, size=(200,-1),style=wx.TE_PROCESS_ENTER)
            dlgsizer.Add(self.chosen, 0, wx.EXPAND | wx.LEFT|wx.RIGHT|wx.TOP, 10)
            self.chosen.Bind(wx.EVT_KEY_DOWN, self.OnChosenKey)

        self.filterCheckBoxes = {}
        self.replaceContentsLabel = None
        if filterCheckBoxes:
            for label, val in filterCheckBoxes.items():
                val += (False, False, None)[ len( val )-2: ]
                self.filterCheckBoxes[label] = val

                isChecked, cb, _invert, replaceContents, _clickCb = val
                if replaceContents:
                    # One of the filterCheckboxes may have replaceContents enabled, which allows a single filterCheckbox
                    # to toggle between different contents for the entire listbox.
                    if self.replaceContentsLabel is not None:
                        raise ValueError( '_ChoiceDialogI only supports one filterCheckbox with replaceContents=True' )

                    self.replaceContentsLabel = label

                checkBox = wx.CheckBox( self, -1, label )
                checkBox.SetValue( isChecked )
                checksizer.Add( checkBox, 0, wx.ALIGN_LEFT | wx.LEFT, border = 10 )

                checkBox.Bind( wx.EVT_CHECKBOX, partial( self.updateFiterCheckboxModel, label, checkBox, val[1:] ) )

        if self.concatchoice:
            if type(self.concatchoice) == str:
                default = True
                cb = False
            else:
                default = self.concatchoice['default']
                cb = self.concatchoice['cb']
                self.concatchoice = self.concatchoice['glue']
            self.chosen.Show(default)
            checkBox = wx.CheckBox( self, -1, "Select path" )
            checkBox.SetValue( default )
            checksizer.Add( checkBox, 0, wx.ALIGN_LEFT | wx.LEFT, border = 10 )
            def hideshow( checkbox, self, cb, evt ):
                if checkbox.GetValue():
                    self.chosen.Show()
                    self.Layout()
                else:
                    self.chosen.Hide()
                    self.Layout()
                if cb:
                    cb( checkbox.GetValue() )

            checkBox.Bind( wx.EVT_CHECKBOX, partial( hideshow, checkBox, self, cb ) )

        if len(checksizer.GetChildren()):
            dlgsizer.AddSpacer( 10 )
            dlgsizer.Add( checksizer )

        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        if extraButtons:
            for lbl,cb in extraButtons:
                btn = wx.Button(self, -1, lbl)
                btn.Bind(wx.EVT_BUTTON, partial(self.ExtraButton, cb))
                btnsizer.Add(btn, 0, wx.ALIGN_LEFT)

        if multichoice:
            clear = wx.Button(self, -1, "Clear")
            clear.Bind(wx.EVT_BUTTON, self.OnClear)
            clear.Enable(False)
            self.clearbtn = clear
            btnsizer.Add(clear, 0, wx.ALIGN_LEFT)

            if enableSelectAll:
                selectall = wx.Button(self, -1, "Select all")
                selectall.Bind(wx.EVT_BUTTON, self.OnSelectAll)
                btnsizer.Add(selectall, 0, wx.ALIGN_LEFT)

            if searchable:
                self.statustxt = wx.StaticText(self, -1, '')
                self.statustxt.SetForegroundColour('#0000CC')
                dlgsizer.Add(self.statustxt, 0, wx.TOP|wx.LEFT|wx.EXPAND, 10)

        if checkBoxes:
            def updateCheckboxModel(label, isChecked, _):
                checkBoxes[label] = isChecked()

            for label, isChecked in checkBoxes.items():
                checkBox = wx.CheckBox(self, -1, label)
                checkBox.SetValue(isChecked)
                btnsizer.Add(checkBox, 0, wx.ALIGN_LEFT | wx.CENTER)
                checkBox.Bind( wx.EVT_CHECKBOX, partial(updateCheckboxModel, label, checkBox.IsChecked) )

        dlgsizer.Add(wx.StaticLine(self, -1), 0, wx.ALL|wx.EXPAND, 10)

        btnsizer.Add(wx.Panel(self, -1), 10)

        self.okbtn = wx.Button(self, wx.ID_OK, "OK")
        self.okbtn.SetDefault()
        btnsizer.Add(self.okbtn)
        cancel = wx.Button(self, wx.ID_CANCEL, "Cancel")
        btnsizer.Add(cancel)

        dlgsizer.Add(btnsizer, 0, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.BOTTOM | wx.ALIGN_RIGHT, 10)

        self.SetSizer(dlgsizer)

        if filterCheckBoxes:
            self.doFiltering()
        self.UpdateStatus()
        self.Layout()

    def updateFiterCheckboxModel( self, label, checkBox, xxx_todo_changeme, evt ):
        (cb, invert, replaceContents, clickCb) = xxx_todo_changeme
        checked = checkBox.IsChecked()
        self.filterCheckBoxes[label] = (checked, cb, invert,replaceContents, clickCb)
        if clickCb:
            clickCb( checked )
        self.doFiltering()

    def doFiltering( self ):
        hidden = set()

        # If there is a replaceContents filter, apply that first
        if self.replaceContentsLabel:
            isChecked, cb, _, _, _ = self.filterCheckBoxes[ self.replaceContentsLabel ]
            self.choices = cb( bool( isChecked ) ) or []

        # Go through all checked checkboxes first applying any hides, then any shows.
        # i.e. checked state counts and inverted ('show xxx') only has an effect over already hidden items
        # see /examples/editChoiceDialog_FilterCheckboxes.py

        for label, (isChecked, cb, invert, _replaceContents, _clickCb) in sorted( iter(self.filterCheckBoxes.items()), key=lambda x: x[1][2] ):
            if isChecked:
                if label != self.replaceContentsLabel: # Already filtered outside the loop
                    if invert:
                        hidden.difference_update( cb( self.choices or [] ) or [] )
                    else:
                        hidden.update( cb( self.choices or [] ) or [] )

        self.filteredChoices = sorted( list( set( self.choices ).difference( hidden ) ) )
        self.doSearch()

    def ExtraButton(self, cb, evt):
        self.returnVal = cb()
        if self.returnVal:
            self.EndModal(wx.ID_OK)

    def addSelToChosen(self):
        self.chosen.WriteText(self.lbox.GetItems()[self.lbox.GetSelection()] + self.concatchoice)

    def OnItemDClick(self, evt):
        if self.chosen and self.chosen.IsShown():
            return self.addSelToChosen()
        self.EndModal(wx.ID_OK)

    def OnSearchCancel(self, evt):
        self.searchtxt.Clear()
        self.UpdateClearButton()
        evt.Skip()

    def OnChosenKey(self,evt):
        kc = evt.GetKeyCode()
        if kc == wx.WXK_RETURN:
            if self.okbtn.Enabled:
                self.EndModal(wx.ID_OK)
        else:
            evt.Skip()

    def OnSearchKey(self, evt):
        itemsPerPage = self.lbox.GetSize().height/12  ## crude approximation for pgdown/pgup
        if evt.GetKeyCode() == wx.WXK_DOWN:
            self.inSelectionTab = True
            self.lbox.SetSelection(min(self.lbox.GetSelection()+1, self.lbox.GetCount()-1))
        elif evt.GetKeyCode() == wx.WXK_UP:
            self.inSelectionTab = True
            self.lbox.SetSelection(max(-1, self.lbox.GetSelection()-1))
        elif evt.GetKeyCode() == wx.WXK_PAGEDOWN:
            self.lbox.SetSelection(min(self.lbox.GetSelection()+itemsPerPage, self.lbox.GetCount()-1))
        elif evt.GetKeyCode() == wx.WXK_PAGEUP:
            self.lbox.SetSelection(max(self.lbox.GetSelection()-itemsPerPage, 0))
        elif evt.GetKeyCode() == wx.WXK_RETURN:
            if self.chosen and self.chosen.IsShown():
                self.addSelToChosen()
            elif self.okbtn.Enabled:
                self.EndModal(wx.ID_OK)
        elif evt.GetKeyCode() == wx.WXK_TAB:
            self.searchtxt.Navigate()
        elif (self.multichoice and evt.GetKeyCode() == wx.WXK_SPACE and
            (len(self.lbox.GetItems()) == 1 or self.inSelectionTab)):
            if self.lbox.GetSelection() > -1:
                check = not self.lbox.IsChecked(self.lbox.GetSelection())
                self.lbox.Check(self.lbox.GetSelection(), check)
                item = self.lbox.GetString(self.lbox.GetSelection())
                if check:
                    self.selected.add(item)
                else:
                    self.selected -= set([item])
                self.clearbtn.Enable(len(self.selected)>0)
                self.UpdateStatus()
        else:
            self.inSelectionTab = False
            evt.Skip()

    def OnSearchText( self, evt ):
        self.doSearch()
        evt.Skip()

    def doSearch( self ):
        if self.lbox.GetSelection() != -1:
            sel = self.lbox.GetString(self.lbox.GetSelection())
        else:
            sel = None

        txt   = self.searchtxt.GetValue()
        flags = re.IGNORECASE if txt.islower() else 0 ## mixed case, use case sensitive compare
        try:
            expr = re.compile( txt, flags = flags )
        except re.error:
            expr = re.compile( re.escape( txt ), flags = flags )

        items = []
        for x in self.filteredChoices:
            m = expr.search( x )
            if m:
                items.append( ( len( x ), -( m.end() - m.start() ), x ) )
            elif self.enableSearchByCaps:
                upperChars = ''.join( re.findall( '[A-Z]', x ) )
                if upperChars.startswith( txt.upper() ):
                    items.append( ( len( x ), -len( txt ), x ) )
                elif upperChars.find( txt.upper() ) >= 0:
                    items.append( ( len( x ), len( txt ), x ) )

        if self.sortByRelevance:
            items = [ x[2] for x in sorted( items ) ]
        else:
            items = [ x[2] for x in items ]

        self.lbox.SetItems(items)
        [self.lbox.Check(i, True) for i, item in enumerate(items) if item in self.selected]

        if txt.strip() and not items:
            bg = ui.colors.INVALID_TEXTBG
            self.okbtn.Disable()
        else:
            bg = ui.colors.SYS_COLOR_WINDOW
            self.okbtn.Enable()

        for c in self.searchtxt.GetChildren():
            c.SetBackgroundColour( bg )
            c.Refresh()
        self.searchtxt.SetBackgroundColour( bg )
        self.searchtxt.Refresh()

        if sel in items:
            self.lbox.SetSelection(items.index(sel))
        elif items:
            self.lbox.SetSelection(0)

        self.UpdateClearButton()

    def OnItemToggle(self, evt):
        idx   = evt.GetSelection()
        multi = False

        if wx.GetKeyState( wx.WXK_SHIFT ):
            selected = [ self.lbox.IsChecked( i ) for i in range( self.lbox.GetCount() ) ]
            first = last = None
            indicies = []
            for i, s in enumerate( selected ):
                if s and i != idx:
                    indicies.append( i )

            if indicies:
                idx0 = bisect.bisect_right( indicies, idx ) - 1

                if idx0 > -1:
                    first, last = indicies[idx0], idx
                else:
                    first, last = idx, indicies[0]

            if first is not None and last is not None:
                multi = True
                for ii in range( first, last+1 ):
                    self.selected.add( self.lbox.GetString( ii ) )
                    self.lbox.Check( ii, True )

        if not multi:
            item = self.lbox.GetString( idx )
            if self.lbox.IsChecked( idx ):
                self.selected.add(item)
            else:
                if item in self.selected:
                    self.selected.remove(item)
        self.UpdateStatus()
        self.UpdateClearButton()
        evt.Skip()

    def UpdateStatus(self):
        if self.statustxt:
            if self.selected:
                txt = '%i/%i selected' % (len(self.selected), len(self.choices))
            else:
                txt = ''
            self.statustxt.SetLabel(txt)

    def GetVisibleItems( self ):
        return [ self.lbox.GetString( i ) for i in range(self.lbox.GetCount()) if self.lbox.IsChecked( i ) ]

    def UpdateClearButton( self ):
        if self.clearbtn:
            canClear = bool( self.selected & set( self.GetVisibleItems() ) )
            self.clearbtn.Enable( canClear )

    def OnClear(self, evt):
        [self.lbox.Check(i, False) for i in range(self.lbox.GetCount())]
        self.selected = set()
        self.clearbtn.Enable(False)
        self.UpdateStatus()

    def OnSelectAll(self, evt):
        [self.lbox.Check(i, True) for i in range(self.lbox.GetCount())]
        self.selected |= set( self.GetVisibleItems() )
        self.clearbtn.Enable(True)
        self.UpdateStatus()

    def OnShowRaise(self, evt):
        ui.task_queue_staging.post(self.Raise)

    def SetSelections(self, indices):
        if self.multichoice:
            list(map(self.lbox.Check, indices))
            [self.selected.add(self.choices[i]) for i in indices]
            self.clearbtn.Enable(True)
        else:
            self.lbox.SetSelection(indices[-1])
        self.UpdateStatus()

    def GetStringSelection(self):
        """ used for single choice """
        if self.chosen and self.chosen.IsShown():
            if not self.chosen.GetValue():
                self.addSelToChosen()
            return self.chosen.GetValue()
        if self.returnVal:
            return self.returnVal
        else:
            if self.lbox.GetSelection() != -1:
                return str(self.lbox.GetString(self.lbox.GetSelection()))

    def GetStringSelections(self):
        if self.returnVal:
            return self.returnVal
        elif self.strictSearch and self.searchtxt.GetValue():
            return sorted(s for s in self.selected if s in self.lbox.GetItems())
        else:
            return sorted(self.selected)


class _TextExtI(wx.Dialog):
    def __init__(self,parent=None,labels=('',),title='',defaults=('',), size=None, choices=[], scroll=False, fit = True, expand = True,
        icons = [],
        textStyles = []
    ):
        wx.Dialog.__init__( self, parent, title=title, style = wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER | ( wx.EXPAND if expand else 0 ) )

        choices = choices or len( labels )*[None]
        defaults = defaults or len( labels ) * ['']
        icons = icons or [ None ]
        textStyles = textStyles or [ 0 ]

        self.texts = []
        if scroll:
            panel = wx.lib.scrolledpanel.ScrolledPanel( self )
        else:
            panel = wx.Panel( self )

        sizer = wx.BoxSizer( wx.VERTICAL )
        for i, [ label, default, iconID, controlStyle ] in enumerate(zip( labels, itertools.cycle(defaults), itertools.cycle(icons), itertools.cycle(textStyles) )):

            if iconID is not None:
                labelSizer = wx.BoxSizer( wx.HORIZONTAL )
                bmp  = wx.ArtProvider.GetBitmap( iconID, wx.ART_CMN_DIALOG )
                icon = wx.StaticBitmap( self, -1, bmp, bmp.Size.Get() )
                labelSizer.Add( icon, flag = wx.ALL | wx.ALIGN_CENTER_VERTICAL, border = bmp.Height / 5 )
                labelFlags = wx.ALIGN_CENTER_VERTICAL
                sizer.Add( labelSizer, 0, wx.TOP )
            else:
                labelSizer = sizer
                labelFlags = wx.TOP

            labelSizer.Add( wx.StaticText( panel, -1, label ), 0, labelFlags, i and 5 or 0 )

            if choices[i]:
                default = default or choices[i][0]
                style = wx.CB_READONLY
                if choices[i] == sorted( choices[i] ):
                    ## If it is sorted, use CB_SORT style for typeahead find
                    style |= wx.CB_SORT
                text = wx.ComboBox( panel, -1, default, choices=choices[i], style=style )
            else:
                text = wx.TextCtrl( panel, -1, default, style = controlStyle )

            self.texts.append( text )
            sizer.Add( text, 0, wx.EXPAND|wx.TOP, 2 )
        sizer.Add( wx.StaticLine( panel ), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5 )

        okay   = wx.Button( panel, wx.ID_OK )
        okay.SetDefault()
        cancel = wx.Button( panel, wx.ID_CANCEL )

        btns   = wx.StdDialogButtonSizer()
        btns.AddButton( okay )
        btns.AddButton( cancel )
        btns.Realize()


        topsizer = wx.BoxSizer( wx.VERTICAL )

        topsizer.Add( sizer, 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
        topsizer.Add( btns, 0, wx.BOTTOM|wx.LEFT|wx.RIGHT, 10 )


        panel.SetSizer(topsizer)

        frame_sizer = wx.BoxSizer( wx.VERTICAL )
        frame_sizer.Add(panel, 1, wx.EXPAND )
        self.SetSizer(frame_sizer)

        if scroll:
            panel.SetupScrolling()
            self.SetMinSize( (-1, min( topsizer.CalcMin().y + 50,wx.GetDisplaySize().y)) )

        if fit:
            self.Fit()

        if size:
            if expand:
                self.SetMinSize( size )
            else:
                self.SetSize( size )

class _MessageDialogI(wx.Dialog):
    def __init__(self, parent, ID, title, text, icon,
                size=wx.DefaultSize, pos=wx.DefaultPosition,
                style=0, okDefault=False ):

        style |= wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, parent, ID, title, pos, size, style)


        sizer = wx.BoxSizer(wx.VERTICAL)
        box = wx.BoxSizer(wx.HORIZONTAL)

        if icon:
            bmp = getIcon( icon )
            if bmp:
                box.Add(wx.StaticBitmap(self, -1, bmp), 0, wx.ALL, 5)

        box.Add(wx.StaticText(self, -1, text), 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        BTNMASK = wx.OK|wx.CANCEL|wx.YES|wx.NO|wx.HELP|wx.NO_DEFAULT
        btnsizer = self.CreateStdDialogButtonSizer(style&BTNMASK)

        if style & wx.NO:
            ## why does this not work automatically?
            self.Bind(wx.EVT_BUTTON,   lambda evt: self.EndModal(wx.ID_NO), id=wx.ID_NO)
            self.Bind(wx.EVT_CHAR_HOOK, self.OnCharHook)

        if style & wx.CANCEL and not okDefault:
            btn = self.FindWindowById( wx.ID_CANCEL )
            btn.SetFocus()


        sizer.Add(btnsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER|wx.ALL, 10)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.CenterOnParent()

    def OnCharHook(self, event):
        if event.KeyCode == wx.WXK_ESCAPE:
            self.EndModal(wx.ID_CANCEL)
        else:
            event.Skip()


class _TimedMessageDialogI(wx.Dialog):
    ''' A message dialog with a with a set lifespan, in seconds, before automatically closing.
        When the timer expires, the dialog will return either:
            wx.ID_CANCEL    : default
            wx.ID_OK        : if okDefault=True passed to __init__
            wx.ID_NO        : if style passed to __init__ contains wx.ID_NO
    '''
    def __init__(self, parent, ID, title, text, icon,
                size=wx.DefaultSize, pos=wx.DefaultPosition,
                style=0, okDefault=False,
                ttl=60,
                timerColor=wx.RED,
                timerFontSize=8,
                timerSecondsBold=True ):
        ''' Create the dialog

            ttl< int >                      : seconds until this dialog will close
            timerColor< wx.Colour >         : the color of the label that counts down time remaining
            timerFontSize< int >            : font size of the timer label
            timerSecondsBold< bool >        : should we make the countdown number bold in the label?
        '''

        style |= wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, parent, ID, title, pos, size, style)

        self.okDefault = okDefault
        self.timerColor    = timerColor
        self.timerFontSize = timerFontSize
        self.boldSeconds   = timerSecondsBold

        self.timeToLive = ttl

        sizer = wx.BoxSizer(wx.VERTICAL)
        box = wx.BoxSizer(wx.HORIZONTAL)

        if icon:
            bmp = getIcon( icon )
            if bmp:
                box.Add(wx.StaticBitmap(self, -1, bmp), 0, wx.ALL, 5)

        box.Add(wx.StaticText(self, -1, text), 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        sizer.Add( self.createTimerPnl(), 1, wx.GROW|wx.ALIGN_CENTER|wx.BOTTOM, 5 )

        BTNMASK = wx.OK|wx.CANCEL|wx.YES|wx.NO|wx.HELP|wx.NO_DEFAULT
        btnsizer = self.CreateStdDialogButtonSizer(style&BTNMASK)

        if style & wx.NO:
            ## why does this not work automatically?
            self.Bind(wx.EVT_BUTTON,   lambda evt: self.EndModal(wx.ID_NO), id=wx.ID_NO)
            self.Bind(wx.EVT_CHAR_HOOK, self.OnCharHook)

        if style & wx.CANCEL and not okDefault:
            btn = self.FindWindowById( wx.ID_CANCEL )
            btn.SetFocus()

        sizer.Add(btnsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER|wx.BOTTOM, 10)
        self.SetSizer(sizer)

        self.timer = wx.Timer( self )
        self.Bind( wx.EVT_TIMER, self.OnTimer, self.timer )
        self.Bind( wx.EVT_CLOSE, self.OnClose )

        sizer.Fit(self)
        self.CenterOnParent()

    def stopTimer( self ):
        ''' Ensure that our timer has stopped '''
        try:
            self.timer.Stop()
        except:
            pass

    def OnClose( self, event ):
        ''' Callback to stop the timer when the dialog was closed using OS window controls instead of Ok/Cancel '''
        self.stopTimer()
        event.Skip()

    def ShowModal( self ):
        ''' Start out timer and show the dialog '''
        self.timer.Start( 1000 )
        return super( _TimedMessageDialogI, self ).ShowModal()

    def EndModal( self, retCode ):
        ''' Be sure our timer is stopped before closing dialog '''
        self.stopTimer()
        return super( _TimedMessageDialogI, self ).EndModal( retCode )

    def OnTimer( self, event ):
        ''' A second has passed. Update the timer label. If ttl has expired, close dialog '''
        self.timeToLive -= 1
        self.ttlLbl.SetLabel( str( self.timeToLive ) )
        if self.timeToLive <= 0:
            self.EndModal( self.okDefault and wx.ID_OK or wx.ID_CANCEL )

    def OnCharHook(self, event):
        ''' Shortcut keys for closing dialog '''
        if event.KeyCode == wx.WXK_ESCAPE:
            self.EndModal(wx.ID_CANCEL)
        else:
            event.Skip()

    def createTimerPnl( self ):
        ''' Build & return the panel that contains the countdown timer label '''
        timerPnl = wx.Panel( self, -1 )
        sizer = wx.BoxSizer( wx.HORIZONTAL )

        sizer.Add(wx.Panel(timerPnl, -1), 1, wx.EXPAND|wx.LEFT|wx.RIGHT, 1)  ##Growable spacer

        textLbl = wx.StaticText( timerPnl, -1, 'Time remaining: '.format( self.timeToLive ), style=wx.ALIGN_CENTER|wx.ST_NO_AUTORESIZE)
        textLbl.SetForegroundColour(self.timerColor)
        textLbl.SetFont( wx.Font( self.timerFontSize, wx.FONTFAMILY_DEFAULT,  wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_LIGHT ) )

        self.ttlLbl = wx.StaticText( timerPnl, -1, str( self.timeToLive ), style=wx.ALIGN_CENTER|wx.ST_NO_AUTORESIZE)
        self.ttlLbl.SetForegroundColour(self.timerColor)
        self.ttlLbl.SetFont( wx.Font( self.timerFontSize, wx.FONTFAMILY_DEFAULT,  wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD if self.boldSeconds else wx.FONTWEIGHT_NORMAL ) )

        secLbl = wx.StaticText( timerPnl, -1, ' sec', style=wx.ALIGN_CENTER|wx.ST_NO_AUTORESIZE)
        secLbl.SetForegroundColour(self.timerColor)
        secLbl.SetFont( wx.Font( self.timerFontSize, wx.FONTFAMILY_DEFAULT,  wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_LIGHT ) )

        sizer.Add( textLbl, 0, wx.ALIGN_CENTER|wx.RIGHT, 0 )
        sizer.Add( self.ttlLbl, 0, wx.ALIGN_CENTER, 0 )
        sizer.Add( secLbl, 0, wx.ALIGN_CENTER|wx.LEFT, 0 )

        sizer.Add(wx.Panel(timerPnl, -1), 1, wx.EXPAND|wx.LEFT|wx.RIGHT, 1)  ##Growable spacer
        timerPnl.SetSizer( sizer )

        return timerPnl


class _NumberDialogI(wx.NumberEntryDialog):
    def __init__(self, parent, ID, message, prompt, title, value, min, max, pos=wx.DefaultPosition):
        wx.NumberEntryDialog.__init__(  self,
                                        parent,
                                        message = message,
                                        prompt  = prompt,
                                        caption = title,
                                        value   = value,
                                        min     = min,
                                        max     = max,
                                        pos     = pos )

    def OnCharHook(self, event):
        if event.KeyCode == wx.WXK_ESCAPE:
            self.EndModal(wx.ID_CANCEL)
        else:
            event.Skip()

class _ChoiceTreeDialogI(wx.Dialog):
    def __init__(self,parent=None,items={},text='', title='',defaults={}, initialSelection=None,
                 expand=False, size=wx.DefaultSize, showLines=True, boldParents=True, checkBoxes = None, searchable=False, initialExpansion=() ):
        ''' if expand=False, can use initialExpansion tuple to specify which individual children should be expanded
        '''
        wx.Dialog.__init__( self, parent, title=title, size=size, style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )

        self.dlgsizer = sizer = wx.BoxSizer( wx.VERTICAL )
        if text:
            sizer.Add( wx.StaticText(self, -1, text), 0, wx.EXPAND|wx.TOP|wx.LEFT|wx.RIGHT, 5)

        style = 0
        if USE_AGW_STYLE:
            agwStyle = customtreectrl.TR_NO_LINES if not showLines else 0
            self.tree = _MultiChoiceTreeCtrl(   self,
                                                -1,
                                                items=items,
                                                initialSelection=initialSelection,
                                                style=style,
                                                boldParents=boldParents,
                                                searchable=searchable,
                                                agwStyle=agwStyle,
                                                initialExpansion=initialExpansion )
        else:
            if not showLines:
                style |= customtreectrl.TR_NO_LINES
            self.tree = _MultiChoiceTreeCtrl(   self,
                                                -1,
                                                items=items,
                                                initialSelection=initialSelection,
                                                style=style,
                                                boldParents=boldParents,
                                                searchable=searchable,
                                                initialExpansion=initialExpansion )
        self.tree.Bind(customtreectrl.EVT_TREE_ITEM_CHECKED, self.OnItemChecked)
        if expand:
            self.tree.ExpandAll()

        sizer.Add( self.tree, 1, wx.TOP|wx.EXPAND, 5 )

        sizer.Add( wx.StaticLine( self ), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5 )

        self.checkBoxes = []
        if checkBoxes:

            optionsizer = wx.BoxSizer( wx.VERTICAL )

            for key, data in checkBoxes.items():
                cbox    = wx.CheckBox( self, id=-1, label=data[ 0 ], style=wx.CHK_2STATE, name=key )
                cbox.SetValue( data[ 1 ] )
                optionsizer.Add( cbox )
                self.checkBoxes.append( cbox )

        okay   = wx.Button( self, wx.ID_OK )
        okay.SetDefault()
        cancel = wx.Button( self, wx.ID_CANCEL )

        btns   = wx.BoxSizer(wx.HORIZONTAL)

        clear = wx.Button(self, -1, "Clear")
        clear.Bind(wx.EVT_BUTTON, self.OnClear)
        if not initialSelection:
            clear.Enable(False)
        self.clearbtn = clear

        selectall = wx.Button(self, -1, "Select All")
        selectall.Bind(wx.EVT_BUTTON, self.OnSelectAll)

        btns.Add(clear, 0, wx.ALIGN_RIGHT)
        btns.Add(selectall, 0, wx.ALIGN_RIGHT)

        btns.Add(wx.Panel(self, -1), 10)

        btns.Add( okay )
        btns.Add( cancel )

        topsizer = wx.BoxSizer( wx.VERTICAL )

        topsizer.Add(sizer, 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
        if checkBoxes:
            topsizer.Add(optionsizer, 0, wx.RIGHT|wx.LEFT|wx.TOP|wx.BOTTOM, 10 )
        topsizer.Add(btns, 0, wx.BOTTOM|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)

        self.SetSizer( topsizer )
        self.SetSize(size)

    def OnItemChecked(self, evt):
        self.clearbtn.Enable(self.tree.HasCheckedItems())
        evt.Skip()

    def OnClear(self, evt):
        self.clearbtn.Enable(False)
        self.tree.CheckChilds(self.tree.root, False)

    def OnSelectAll(self, evt):
        self.clearbtn.Enable(True)
        self.tree.CheckChilds(self.tree.root, True)

    def GetSelection(self, returnLeaves=False):
        return self.tree.GetCheckedItems(returnLeaves)

    def GetCheckboxValues( self ):
        return dict( ( cbox.GetName(), cbox.GetValue() ) for cbox in self.checkBoxes )



class _SingleChoiceDialogI(wx.Dialog):
    def __init__(self,parent=None,text='',title='',initialVal=None,choices=[],editable=False,
        sort = True,
        size = (-1,-1),
        icon = None
    ):
        wx.Dialog.__init__( self, parent, title=title, style=wx.CHOICEDLG_STYLE, size=size )
        style = wx.TE_PROCESS_ENTER
        if not editable:
            style |= wx.CB_READONLY
        if sort:
            style |= wx.CB_SORT

        self.combo = wx.ComboBox( self, -1, choices=choices, style=style )
        self.combo.Bind( wx.EVT_CHAR, self.OnChar )


        sizer = wx.BoxSizer( wx.VERTICAL )
        topLineFlags = wx.LEFT|wx.TOP|wx.RIGHT

        if icon is not None:
            labelSizer = wx.BoxSizer( wx.HORIZONTAL )
            bmp  = wx.ArtProvider.GetBitmap( icon, wx.ART_CMN_DIALOG )
            iconCtrl = wx.StaticBitmap( self, -1, bmp, size= bmp.Size.Get() )
            labelSizer.Add( iconCtrl, flag = wx.ALL | wx.ALIGN_CENTER_VERTICAL, border = bmp.Height / 5 )
            labelFlags = wx.ALIGN_CENTER_VERTICAL
            sizer.Add( labelSizer, 0, topLineFlags )
        else:
            labelSizer = sizer
            labelFlags = topLineFlags

        labelSizer.Add( wx.StaticText( self, -1, text ), 0, labelFlags, 10 )

        sizer.Add( self.combo, 0, wx.EXPAND|wx.ALL, 10 )
        sizer.Add( wx.StaticLine( self ), 0, wx.EXPAND|wx.ALL, 5 )

        okay   = wx.Button( self, wx.ID_OK )
        okay.SetDefault()
        cancel = wx.Button( self, wx.ID_CANCEL )

        btns   = wx.StdDialogButtonSizer()
        btns.AddButton( okay )
        btns.AddButton( cancel )
        btns.Realize()
        sizer.Add( btns, 0, wx.EXPAND|wx.ALL, 5 )

        if initialVal is not None:
            self.combo.SetValue( initialVal )
            self.combo.SetStringSelection( initialVal )

        self.combo.SetFocus()
        self.SetSizerAndFit( sizer )

    def OnChar(self,event):
        kc = event.GetKeyCode()
        if kc == wx.WXK_RETURN:
            self.EndModal( wx.ID_OK )
            return
        elif kc == ord( '?' ):
            lib.wxutil.ComboDropDown( self.combo )
        else:
            event.Skip()


class KeyValueDialogI(wx.Dialog):

    """ simple data entry of a key and a ( string ) value """

    def __init__(self, labels, defaultValues=None, parent=None , title=''):
        wx.Dialog.__init__( self, parent, title=title, style=wx.CHOICEDLG_STYLE )

        self.labels = [ wx.StaticText( self, label=l ) for l in labels ]
        self.defaultValues = defaultValues or [ '' for _ in labels ]

        self.fields = [ wx.TextCtrl( self, -1, str( v ) ) for v in self.defaultValues ]

        sizer = wx.BoxSizer( wx.VERTICAL )

        for label,field in zip( self.labels, self.fields ):
            hsizer = wx.BoxSizer( wx.HORIZONTAL )
            hsizer.Add( label, 1, wx.EXPAND|wx.ALL, 5 )
            hsizer.Add( field, 1, wx.EXPAND|wx.ALL, 5 )
            sizer.Add( hsizer, 0, wx.EXPAND|wx.ALL, 1 )

        okay   = wx.Button( self, wx.ID_OK )
        okay.SetDefault()
        cancel = wx.Button( self, wx.ID_CANCEL )

        btns   = wx.StdDialogButtonSizer()
        btns.AddButton( okay )
        btns.AddButton( cancel )
        btns.Realize()
        sizer.Add( btns, 1, wx.ALIGN_CENTER|wx.ALL, 5 )

        self.SetSizerAndFit( sizer )


class ListSelectorOrderDialog( wx.Dialog, wx.lib.mixins.listctrl.ColumnSorterMixin ):

    ''' simple dialog whcih has two lists sources/destinations and buttons to move items between two and a order button on the destinations list for ordering '''

    def __init__( self,
                parent,
                ID=wx.ID_ANY,
                title=None,
                size=(50,100),
                style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER|wx.MAXIMIZE_BOX,
                pos= wx.DefaultPosition,
                db = None,
                actionButtons=(),
                onActivate=wx.ID_OK,
                sources = None,
                destinations = [],
                sourceTitle = '',
                destTitle = '',
                removeSource=False,
                ):

        self.db = db

        title=title or 'Edit Bookmarks'
        sourceTitle = sourceTitle or 'Source'
        destTitle = destTitle or 'Saved'
        self.removeSource=removeSource

        self.onActivateAction = onActivate
        self.labelstyle = wx.TRANSPARENT_WINDOW | wx.FULL_REPAINT_ON_RESIZE | wx.CLIP_CHILDREN
        self.currSpin = 0

        c = wx.SystemSettings_GetColour(wx.SYS_COLOUR_3DFACE).Get()
        self.bg = [ max( x - 7, 0 ) for x in c]
        self.fg = [ min( x + 17, 255 ) for x in c ]

        pre = wx.PreDialog()
        pre.Create(parent, ID, title, pos, size, style)
        self.PostCreate(pre)

        panel = wx.Panel( self )

        # Main sizer
        sizer = wx.BoxSizer(wx.VERTICAL)


        hsizer = wx.BoxSizer(wx.HORIZONTAL)

        srcsizer = wx.BoxSizer(wx.VERTICAL)
        self.srclist = wx.ListBox(panel, choices=sources , style=wx.LB_SORT|wx.LB_EXTENDED)
        self.srclist.Bind(wx.EVT_LISTBOX, self.OnSourceSelect)
        self.srclist.Bind(wx.EVT_LISTBOX_DCLICK, self.OnAddSourceToDestinations)


        srcsizer.Add(GenStaticText(panel, -1, sourceTitle, style=self.labelstyle))
        srcsizer.Add(self.srclist, 1, wx.ALL, 0)
        hsizer.Add(srcsizer, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        # Destination buttons
        destnbtnsizer = wx.BoxSizer(wx.VERTICAL)
        img = lib.image.getImage('/app/icons/16x16/silk/play_blue.png', size=None)
        img.SetMaskColour(wx.Colour(alpha=wx.ALPHA_TRANSPARENT))
        self.addbtn = _MyThemedGenBitmapButton(panel, -1, img, size=(24,24))
        self.addbtn.Enable(False)
        self.addbtn.Bind(wx.EVT_BUTTON, self.OnAddSourceToDestinations)
        destnbtnsizer.Add(self.addbtn)
        img = lib.image.getImage('/app/icons/16x16/silk/reverse_blue.png', size=None)
        img.SetMaskColour(wx.Colour(alpha=wx.ALPHA_TRANSPARENT))
        self.delbtn = _MyThemedGenBitmapButton(panel, -1, img, size=(24,24))
        self.delbtn.Enable(False)
        self.delbtn.Bind(wx.EVT_BUTTON, self.OnRemoveDestination)
        destnbtnsizer.Add(self.delbtn)
        hsizer.Add(destnbtnsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 0)

        # Dstination list
        self.destList = wx.ListBox(panel, choices=destinations, style=wx.LB_EXTENDED)
        self.destList.Bind(wx.EVT_LISTBOX, self.OnDestinationsSelect)
        self.destList.Bind(wx.EVT_LISTBOX_DCLICK, self.OnRemoveDestination)


        destnsizer = wx.BoxSizer(wx.VERTICAL)
        destnsizer.Add(GenStaticText(panel, -1, destTitle, style=self.labelstyle))
        destnsizer.Add(self.destList, 1, wx.ALL, 0)
        hsizer.Add(destnsizer, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)


        #Spin ctrl
        spinsizer =  wx.BoxSizer(wx.VERTICAL)
        self.spin = wx.SpinButton( panel, style=wx.SP_VERTICAL, size=(25,50) )
        self.spin.SetRange(1,100)
        self.spin.SetValue(1)
        self.Bind(wx.EVT_SPIN,self.OnSpin, self.spin)
        spinsizer.Add( self.spin, 0, wx.CENTER )
        hsizer.Add(spinsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 0)

        sizer.Add(hsizer, 1, wx.GROW|wx.ALL, 5)

        btnsizer = wx.BoxSizer(wx.HORIZONTAL)

        self.okbtn = wx.Button(panel, -1, 'Ok')
        self.actionButtons = [ self.okbtn ]
        self.okbtn.Enable(True)
        self.okbtn.Bind(wx.EVT_BUTTON, self.OnOk)
        btnsizer.Add(self.okbtn, 0, wx.ALL, 5)

        for i, ( b, f, ) in enumerate( actionButtons ):
            btn = wx.Button( panel, -1, b )
            btn.Bind( wx.EVT_BUTTON, partial( f, self ) )
            btnsizer.Add( btn, 0, wx.ALL, 5 )
            if i < len( actionButtons ):
                btn.Enable( False )
                self.actionButtons.append( btn )
            else:
                btn.Enable( True )

        btn = wx.Button( panel, wx.ID_CANCEL )
        btnsizer.Add( btn, 0, wx.ALL, 5 )

        sizer.Add( btnsizer, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )


        panel.SetSizer( sizer )

        sizer.Fit( self )


    def OnSpin( self, evt ):
        ''' On move/up down in destination list '''

        idxs = self.destList.GetSelections()
        posn = evt.GetPosition()

        # no postion defined and posn is 1 it must be down

        #lib.logging.info('%s and %s'  %(self.currSpin,posn))
        if self.currSpin == 0 and posn == 1:
            offset = +1
            idxs = [i for i in reversed(idxs)]
        elif posn > self.currSpin:
            offset = -1
        else:
            offset = +1
            idxs = [i for i in reversed(idxs)]

        elems = self.destList.Items

        for idx in idxs:
            if idx != -1 and idx+offset != -1 and idx+offset < len(elems):
                #Then get the element at idx and idx+offsite and swap
                if (idx + offset) < len(elems) and (idx+offset)>=0:
                    elems[idx], elems[idx+offset] = elems[idx+offset], elems[idx]
        self.destList.SetItems( elems )

        self.destList.DeselectAll()
        [ self.destList.Select(item) for item in ( i+offset for i in idxs if i+offset != -1 and i+offset < len(elems)) ]
        self.currSpin = posn


    def OnAddSourceToDestinations( self, evt ):
        ''' Add entry from sources '''
        selItems = [self.srclist.Items[i] for i in self.srclist.GetSelections()]
        for sel in selItems:
            if sel not in self.destList.Items:
                self.destList.Append( sel )

            if self.removeSource:
                if sel in self.srclist.Items:
                    self.srclist.Delete( self.srclist.Items.index(sel) )

    def OnRemoveDestination( self, evt ):
        ''' Delete entry from destinations '''
        idx = self.destList.GetSelections()
        selection = [self.destList.Items[i] for i in idx]
        if idx != -1:
            _ = [ self.destList.Delete(i) for i in reversed(idx) ]
            self.destList.DeselectAll()
            self.delbtn.Enable(False)
        if self.removeSource:
            _ = [ self.srclist.Append( sel ) for sel in selection ]

    def OnDestinationsSelect( self, evt ):
        ''' On select of destinations '''
        if evt.IsSelection():
            self.delbtn.Enable()
            self.addbtn.Disable()
            sel = evt.GetString()
            self.delbtn.SetToolTipString( 'Remove %s from destination'%( sel ) )
            self.selection = sel



    def OnDestinationsDClick( self, evt ):
        ''' On double click of destinations '''
        self.selection = [evt.GetString()]
        self.EndModal( wx.ID_OK )


    def OnSourceSelect( self, evt ):
        ''' On selection of a source item '''
        if evt.IsSelection():
            self.addbtn.Enable()
            self.delbtn.Disable()
            sel = evt.GetString()
            self.addbtn.SetToolTipString( 'Add %s from source'%( sel ) )
            self.selection = sel


    def OnSourceDClick( self, evt ):
        ''' Double click the source list'''
        self.selection = [evt.GetString()]
        self.EndModal( wx.ID_OK )


    def SourceItems( self ):
        ''' Return displayed source items '''
        return self.srclist.Items


    def DestinationItems( self ):
        ''' Return displayed destination items '''
        return self.destList.Items


    def OnOk(self, evt):
        self.EndModal( wx.ID_OK )
