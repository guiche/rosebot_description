'''
Created on 21 May 2014

@author: j913965
'''

import datetime
import os
import sys
import time
from . import _dialogimpl
import wx
import wx._controls
import wx.lib.dialogs

# TODO: use this as a public function
from ._dialogimpl import getIcon as getIcon

# Note: some scripts use this as ui.dialog._getIcon
_getIcon = getIcon

## Flags for dialog icons
ICON_INFORMATION = wx.ICON_INFORMATION
ICON_ERROR = wx.ICON_ERROR
ICON_EXCLAMATION = wx.ICON_EXCLAMATION
ICON_QUESTION = wx.ICON_QUESTION

ELLIPSIS = '...'

def __getparent(parent=None):
    """
    Helper function to find an appropriate parent window for a dialog
    """
    if not parent and wx.GetApp():
        ## None specified, use top level app window
        win = wx.GetActiveWindow() or wx.GetApp().GetTopWindow()
        win = win and win.GetTopLevelParent() or win
        if win and not isinstance( win, wx.SplashScreen ) and not isinstance( win, wx.ProgressDialog ) and not 'ProgressWindow' in win.GetName():
            return win
    elif hasattr(parent, 'widget'):
        ## this is a ui element, return its wx window
        return parent.widget.GetTopLevelParent()
    else:
        ## assume it is a wx window
        return parent

_getparent = __getparent


def NumberDialog( message='', prompt='', title= '', value = 50, min = 1, max = 100, parent = None):
    parent = __getparent(parent)
    dlg = _dialogimpl._NumberDialogI(parent, -1, message, prompt, title, value, min, max)
    if dlg.ShowModal() != wx.ID_OK:
        v = None
    else:
        v = dlg.GetValue()

    if dlg:
        dlg.Destroy()
    
    return v


def ScrolledMessage( text='', title='', size=(400, 300), font=None, parent=None, hscroll=False,
                     cancelButton=False, yesNo=False, header='', icon=None, modal=True, readOnly=True,
                     headerFont=None ):
    """
    Display a message dialog with a scrolling text area.

    text: The body text to scroll
    title: The dialog title
    header: An optional header to display outside of the body text
    cancelButton: If true show a cancel button along with the default Ok button
    yesNo: If true, display Yes/No buttons instead of the default Ok button / optional cancelButton
    hscroll: If true, don't wrap text.
    icon: Optional icon.  One of the ICON_INFORMATION, ICON_ERROR, ICON_EXCLAMATION, ICON_QUESTION flags from ui.dialog.

    Returns: True if Ok or Yes button is pressed.
    If readOnly is false it returns the new text if Ok/Yes is pressed, None otherwise
    """
    
    if yesNo:
        btns = wx.YES_NO
    else:
        btns = wx.OK
        if cancelButton:
            btns |= wx.CANCEL

    style = wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE | wx.MAXIMIZE_BOX
    font = font and font.value or None
    headerFont = headerFont.value if headerFont else None
    dlg = _dialogimpl._ScrolledMessageDialog( __getparent(parent), msg=text, title=title, size=size,
                                  ButtonStyle=btns, font=font, hscroll=hscroll, style=style,
                                  header=header, IconStyle=icon, readOnly=readOnly,
                                  headerFont=headerFont )

    dlg.CenterOnParent()

    if modal:
        try:
            res = dlg.ShowModal() in [wx.ID_OK, wx.ID_YES]
            if not readOnly:
                if res:
                    return dlg.txt.GetValue()
                return None
            return res
        finally:
            dlg.Destroy()
    else:
        dlg.Show()


lastTime = None

def MessageOccasional( *args ):
    global lastTime
    if lastTime is None or (time.time() - lastTime) > 2:
        print(args[0] % args[1:])
        lastTime = time.time()

def Choice( choices, text='', title='', parent=None, multiChoice=False, initialSelection=None,
            enableSelectAll=False, searchable=True, sortByRelevance=False,
            extraButtons=[], checkBoxes=None, filterCheckBoxes=None, size=(330,380),
            multiChoiceItemDClickCallback=None, enableSearchByCaps=False, noParent=False,
            filterCheckColumns=1, concatChoice=False, strictSearch=False, **kwargs ):
    """
    A dialog to select an item.

    OPTIONS:

    choices:          A list of choices, or the name of an object in /Choices/
    multiChoice:      Allow multiple selections
    initialSelection: A list of items that should be initially selected.
    searchable:       True/False to show search field.  Only works for multiChoice at the moment.
    sortByRelevance:  Sort results by relevance to search term.
    extraButtons:     A list of (label,callback) tuples to add as additional buttons in the dialog

    checkBoxes:       A dictionary of label -> isChecked key-value pairs. Checkboxes with these labels
        will be added to this dialog box; the values in the checkBoxes dictionary will reflect
        the state of those checkboxes.

    filterCheckBoxes: A dict of label to (defaultCheckState, callbackFn, invert=False).
        callbackFn: Takes and returns a list of strings; those returned are those to hide.
        Invert: A checked box means show rather than hide the results of the callback. Only has
                meaning for choices which may be hidden by other checkboxes
                - see: examples.editChoiceDialog_FilterCheckboxes

    multiChoiceItemDClickCallback: callback for when a multiChoice list's item is double clicked
    enableSearchByCaps: True/False to match based on capitalisation within each choice
        (e.g. 'cf' matches 'Commodity Future', and 'Foo Commodity Future')
        Note that this option extends the regular search

    noParent:           Specify that the dialog is really to have no parent
    concatChoice:       Alternative to multiChoice, returns a string concatenation selected values in the order selected.
        joined by this arg.  Alternatively, this can be a dict of with keys ( 'default', 'cb', 'glue' )

    strictSearch:       When True returns only subsets of the searched items
    """
    style = wx.CHOICEDLG_STYLE|wx.RESIZE_BORDER #& ~wx.CENTRE
    searchable = searchable is None and len(choices)>30 or searchable

    # __getparent can return bad windows under certain conditions so we provide a choice to not use it here
    actual_parent = None
    if not noParent:
        actual_parent = __getparent(parent)

    dlg = _dialogimpl._ChoiceDialogI( actual_parent, text, title, choices, style=style,
                                          enableSelectAll=enableSelectAll, searchable=searchable,
                                          enableSearchByCaps=enableSearchByCaps,
                                          multichoice=multiChoice, concatchoice=concatChoice, sortByRelevance=sortByRelevance,
                                          extraButtons=extraButtons, checkBoxes=checkBoxes,
                                          filterCheckBoxes=filterCheckBoxes, size=size,
                                          multichoiceItemDClickCallback=multiChoiceItemDClickCallback,
                                          filterCheckColumns=filterCheckColumns, strictSearch=strictSearch )
    if initialSelection:
        if not isinstance(initialSelection, (list, tuple, set)):
            initialSelection = [initialSelection]

        sel = [i for i,v in enumerate(dlg.lbox.Items) if v in initialSelection]
        if sel:
            dlg.SetSelections(sel)

    try:
        if dlg.ShowModal() == wx.ID_OK:
            if multiChoice:
                return dlg.GetStringSelections()
            else:
                return dlg.GetStringSelection()
        return None
    finally:
        dlg.Destroy()



def ChoiceTree(choices={}, text='', title='', initialSelection=None, returnLeaves=True,
               expand=False, parent=None, size=(400,400), showLines=True, boldParents=True,
               checkBoxes = None, searchable=False, initialExpansion=() ):
    """
    A choice dialog with selections organized in a tree.

    Returns a list of lists [ (parent0, parent1, ..., child), ...]
    OR if returnLeaves, only a simple list of children that have been selected.

    choices: dict like { 'G12': ['USD', 'EUR', 'GBP'], 'Latam': ['MXN', 'BRL'] }
    returnLeaves: If True, don't return a list of lists, just return a list of leaf values
    initialSelection: Specify as either a simple list of items to select or a list of path lists.
                      Can also be the string 'ALL' to select all items (this is faster for large trees)
    expand: If true, expand all nodes by default
    showLines: Show lines in the tree
    boldParents: If True, make expandable items bold
    checkBoxes: Additional check boxes to add to the panel
    searchable: If we should add a search bar
    initialExpansion: if expand=False, can use initialExpansion tuple to specify which individual children should be expanded
    """
    dlg = _dialogimpl._ChoiceTreeDialogI(parent=__getparent(parent), items=choices, title=title, text=text,
                             initialSelection=initialSelection, size=size, expand=expand,
                             showLines=showLines, boldParents=boldParents, checkBoxes = checkBoxes, searchable=searchable, initialExpansion=initialExpansion )
    try:
        dlg.CenterOnParent()
        if dlg.ShowModal() == wx.ID_OK:
            selection = dlg.GetSelection(returnLeaves=returnLeaves)
            checkBoxValues = checkBoxes and dlg.GetCheckboxValues() or None
            return checkBoxValues and ( selection, checkBoxValues, ) or selection

        if checkBoxes:
            return None, None
        return None

    finally:
        dlg.Destroy()


def Text( default='', text='', title='', parent=None, password=False, multiline=False,
    size=(-1,-1),
    tooltip = None,
    icon = None
):
    """
    A Simple text entry dialog.

    Returns the entered text, or None if Cancel was pressed.

    multiline: If True, allow multiple lines of text to be entered.
    password: If True, display '*' instead of input characters.
    tooltip: If set it will add Icon with tooltip info - tooltip can be static text or pixie func
    icon: An icon to dsplay next to the one of wx.ART_* or None
    """

    style = wx.TextEntryDialogStyle
    if password:
        style |= wx.TE_PASSWORD
    if multiline:
        style |= wx.TE_MULTILINE

    dlg = wx.TextEntryDialog( __getparent(parent), text, title, defaultValue=default, style=style)
    dlg.SetSize(size)
    toolTip = None

    if icon is not None:
        bmp = wx.ArtProvider.GetBitmap( icon, wx.ART_CMN_DIALOG )
    else:
        bmp = None

    if toolTip:
        if bmp is None:
            bmp  = lib.image.getImage( '/app/icons/info32.png' )

        def onMouseOver( event ):
            if toolTip:
                icon.SetToolTipString( '%s' % str(toolTip()) )
            event.Skip()

        dlg.Bind( wx.EVT_MOTION, onMouseOver )

    if bmp is not None:
        icon = wx.StaticBitmap( dlg, -1, bmp )
        dlg.Sizer.Children[0].Sizer.SetOrientation(wx.HORIZONTAL)
        dlg.Sizer.Children[0].Sizer.Add( icon, 0, wx.RIGHT|wx.ALIGN_BOTTOM, 10 ) # add icon

        if text:
            # get static text controls
            # wx creates multiple static text controls when the text string has multiple lines
            st   = [ t for t in dlg.Children if isinstance( t, wx._controls.StaticText ) ]

            for t in st:
                dlg.Sizer.Children[0].Sizer.Remove( t ) # remove static text

            textSizer = wx.BoxSizer( wx.VERTICAL )

            for t in st:
                textSizer.Add( t, 0, wx.LEFT|wx.ALIGN_CENTER_VERTICAL ) # add static

            dlg.Sizer.Children[0].Sizer.Add( textSizer, 0, wx.LEFT|wx.ALIGN_CENTER, 10 )

    try:
        dlg.Fit()
        if dlg.ShowModal() == wx.ID_OK:
            return dlg.GetValue()
        return None
    finally:
        dlg.Destroy()


def YesNo( text='', title='', parent=None, warning=False, yesDefault=True, error=False):
    """
    yesDefault: makes [yes] button the default
    warning: Display a warning icon instead of a question icon
    """
    icon = (error and ICON_ERROR or warning and ICON_EXCLAMATION or ICON_QUESTION)
    flags = wx.YES_NO
    ## note that wx.YES_DEFAULT is 0, so we have to put NO_DEFAULT ahead of it in this expression
    flags |= (not yesDefault and wx.NO_DEFAULT or wx.YES_DEFAULT)
    dlg = _dialogimpl._MessageDialogI( __getparent(parent), -1, title, text, icon, style=flags)
    try:
        return dlg.ShowModal() == wx.ID_YES
    finally:
        dlg.Destroy()

def OkCancel( text='', title='', parent=None, warning=False, okDefault=True ):
    flags = wx.OK | wx.CANCEL
    icon = (warning and ICON_EXCLAMATION or ICON_QUESTION)
    flags |= (not okDefault and wx.NO_DEFAULT or wx.YES_DEFAULT)
    dlg = _dialogimpl._MessageDialogI( __getparent(parent), -1, title, text, icon,
                                          style=flags, okDefault=okDefault )
    try:
        return dlg.ShowModal() == wx.ID_OK
    finally:
        dlg.Destroy()

def YesNoCancel( text='', title='', parent=None, warning=False ):
    flags = wx.YES_NO | wx.CANCEL
    icon = (warning and ICON_EXCLAMATION or ICON_QUESTION)
    dlg = _dialogimpl._MessageDialogI( __getparent(parent), -1, title, text, icon, style=flags)
    try:
        return {wx.ID_YES:'Yes',wx.ID_NO:'No',wx.ID_CANCEL:'Cancel'}.get( dlg.ShowModal(), None )
    finally:
        dlg.Destroy()



def TimedOkCancel( text='', title='', parent=None, warning=False, okDefault=True, ttl=60, ):
    ''' An OkCancel dialog with a set lifespan, in seconds, before automatically closing.
        The action taken when the timer runs out depends on the okDefault arg

        Args:
            text< str>          : The main text to be displayed in the dialog
            title< str >        : The window header text
            parent< wx.Window > : The parent that this dialog will float over. Can be left as None
            warning< bool >     : Is this a Warning dialog? If True, displays an Exclamation icon
                                   Otherwise, shows a Question icon
            okDefault< bool >   : Is the OK button the default action? If False, Cancel is default
            ttl< int >          : Time To Live - number of seconds that this dialog will remain visible, after which
                                  it will activate either the Ok or Cancel (depending on okDefault arg)

        Returns:
            < bool >            : True if user/timer clicked OK  | False if Cancel
    '''

    flags = wx.OK | wx.CANCEL
    icon = (warning and ICON_EXCLAMATION or ICON_QUESTION)
    flags |= (not okDefault and wx.NO_DEFAULT or wx.YES_DEFAULT)
    dlg = _dialogimpl._TimedMessageDialogI( __getparent(parent), -1, title, text, icon,
                                          style=flags, okDefault=okDefault, ttl=ttl, )
    try:
        return dlg.ShowModal() == wx.ID_OK
    finally:
        dlg.Destroy()

def TimedYesNoCancel( text='', title='', parent=None, warning=False, ttl=60 ):
    ''' A YesNoCancel dialog with a set lifespan, in seconds, before automatically closing.
        If the timer runs out, it automatically activates the 'Cancel' action

        Args:
            text< str>          : The main text to be displayed in the dialog
            title< str >        : The window header text
            parent< wx.Window > : The parent that this dialog will float over. Can be left as None
            warning< bool >     : Is this a Warning dialog? If True, displays an Exclamation icon
                                   Otherwise, shows a Question icon
            ttl< int >          : Time To Live - number of seconds that this dialog will remain visible, after which
                                  it will activate the Cancel action

        Returns:
            < str >         : 'Yes', 'No', or 'Cancel' -- matches button they clicked (or 'Cancel' if timer ran out)
    '''
    flags = wx.YES_NO | wx.CANCEL
    icon = (warning and ICON_EXCLAMATION or ICON_QUESTION)
    dlg = _dialogimpl._TimedMessageDialogI( __getparent(parent), -1, title, text, icon, style=flags, ttl=ttl, )
    try:
        return {wx.ID_YES:'Yes',wx.ID_NO:'No',wx.ID_CANCEL:'Cancel'}.get( dlg.ShowModal(), None )
    finally:
        dlg.Destroy()


def ErrorWarning( text='', title='', warning=False, parent=None, scrolled=False, stayOnTop=False ):
    
    flags = (warning and wx.ICON_EXCLAMATION or wx.ICON_ERROR) | wx.OK | wx.RESIZE_BORDER | wx.CAPTION | wx.DEFAULT_DIALOG_STYLE
    if stayOnTop:
        flags   |= wx.STAY_ON_TOP
    if not scrolled:
        if warning:
            icon = ICON_EXCLAMATION
        else:
            icon = ICON_ERROR
        Message(text=text, title=title, parent=__getparent(parent), icon=icon)
    else:
        dlg = _dialogimpl._ScrolledMessageDialog( __getparent(parent), text, title, style=flags )
        try:
            return dlg.ShowModal() == wx.ID_OK
        finally:
            dlg.Destroy()


def traceOnError(text=None, title='Error', log=False):
    """
    Decorator to display an ErrorTrace on exception

    Use like:

    @traceOnError
    def myFunction():
        ...

    XXX: Doesn't really work for pixie functions.
    """
    def wrap(func):
        def wraperror(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except:
                extra = 'func=%s\nargs=%r\nkwargs=%r' % (func.__name__, args, kwargs)
                ErrorTrace(text=text, title=title, log=log, extra=extra)
        return wraperror

    return wrap



def File( start_dir=None, text="", title='Open'+ELLIPSIS, parent=None, filter='*.*'):
    """
    Open a file chooser dialog.

    'filter' specifies allowable file types and descriptions with a |-separated string, e.g.
         filter='Excel files (*.xls)|*.xls|Tab-separated text files (*.txt)|*.txt'

    Returns a dialog object, where dialog.paths is a list of selected file paths.
    """
    if not start_dir:
        start_dir = os.getcwd()
    ret = wx.lib.dialogs.fileDialog( parent=__getparent(parent), title=title, directory=start_dir,
            filename='', wildcard=filter)
    return ret

def FileSave( start_dir=None, text="", title='Save'+ELLIPSIS, parent=None, filter='*.*', defaultFilename="" ):
    """
    Open a file save dialog.

    'filter' specifies allowable file types and descriptions with a |-separated string, e.g.
         filter='Excel files (*.xls)|*.xls|Tab-separated text files (*.txt)|*.txt'

    Returns a dialog object, where dialog.paths is a list of selected file paths.
    """
    if not start_dir:
        start_dir = os.getcwd()

    ret = wx.lib.dialogs.saveFileDialog( parent=__getparent(parent), title=title, directory=start_dir,
            filename=defaultFilename, wildcard=filter)
    return ret

def Directory(start_dir="", message='Choose a directory', parent=None):
    """
    Returns the selected directory, or None if no selection is made.

    start_dir: defaults to current directory if not provided.
    message: Text to display
    parent: parent window (no surprise there)
    """
    if not start_dir:
        start_dir = os.getcwd()
    return wx.lib.dialogs.directoryDialog(parent=__getparent(parent), message=message, path=start_dir).path

def ColorChooser( color=None ):
    ret = wx.lib.dialogs.colorDialog(color=color)
    return ret

def Splash( img, timeout=3, parent=None ):
    f = wx.SplashScreen(img, wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT, timeout * 1000, __getparent(parent))
    f.Show()

    return f

def SingleChoice( choices, text='', title='', parent=None, initialSelection=None,
    editable=False,
    size=(-1,-1),
    icon = None
):
    """
    Display a dialog with a drop down list to select from.
    Different than Choice(), which shows a multi element list.
    In most cases you will want to use Choice() instead.
    """
    sort = editable
    d = _dialogimpl._SingleChoiceDialogI(
        parent=parent, text=text, title=title, initialVal=initialSelection,
        choices=choices, editable=editable, sort=sort, size=size,
        icon = icon
    )

    try:
        res = d.ShowModal()
        if res == wx.ID_OK:
            if editable:
                return d.combo.GetValue()
            else:
                return d.combo.GetStringSelection()
        return None
    finally:
        d.Destroy()

SingleChoiceDialog = SingleChoice ## old name




def TextExt( title='', labels=('',), defaults=('',), parent=None, size=(320,-1), choices=(),
    scroll=False, fit=True, expand=True, icons = None
):
    """
    A text entry dialog that has multiple fields.

    Returns a list of the results.

    choices: Optional list of choice lists for the fields
    icons:   Icon labels for fields, an optional list of wx.ART_* or None
    """
    d = _dialogimpl._TextExtI( __getparent(parent), labels, title, defaults, size=size,
        choices=choices, scroll=scroll, fit=fit, expand=expand, icons = icons
    )

    try:
        d.CenterOnParent()
        res = d.ShowModal()
        if res == wx.ID_OK:
            return [ text.GetValue() for text in d.texts ]
        return None
    finally:
        d.Destroy()


def KeyValueDialog( labels, defaultValues=None, title='', parent=None ):
    """ basic key value item editor """
    d = _dialogimpl.KeyValueDialogI( labels, defaultValues, parent=parent, title=title )
    try:
        res = d.ShowModal()
        if res == wx.ID_OK:
            return [ f.GetValue() for f in d.fields ]
        return None
    finally:
        d.Destroy()

def SimpleDialog( text='', title='', warning=True, showCheckBox=True, parent=None, info=False, button=None ):
    """ Shows a dialog with an optional "Don't show this again" checkbox.
    Returns True if the checkbox was checked, otherwise returns False.
    button: Optional button to show e.g., Help button, the arg should be a ('Button Text', callback) tuple.
    """
    dlg = wx.Dialog( parent, -1, title=title, style=wx.DEFAULT_DIALOG_STYLE )
    try:
        # wx.ART_ERROR, wx.ART_QUESTION, wx.ART_WARNING, wx.ART_INFORMATION
        image = ( warning and wx.ART_WARNING or wx.ART_ERROR )
        image = ( info and wx.ART_INFORMATION or image )

        sizer  = wx.BoxSizer(wx.VERTICAL)
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        bmp  = wx.ArtProvider.GetBitmap( image, wx.ART_CMN_DIALOG, (32,32) )
        icon = wx.StaticBitmap( dlg, -1, bmp, size=(bmp.GetWidth(), bmp.GetHeight()) )
        hsizer.Add(icon, 0)
        text = text and text+'\n\n' or ''

        label = wx.StaticText(dlg, -1, text, size=(240, -1), style=wx.FIXED_MINSIZE)
        label.Wrap(240)
        hsizer.Add(label, 1, wx.ALIGN_CENTRE|wx.LEFT, 10)
        sizer.Add(hsizer, 0, wx.EXPAND|wx.ALL, 10)

        btnsizer = wx.BoxSizer(wx.HORIZONTAL)
        btn = wx.Button(dlg, wx.ID_OK)
        btn.SetDefault()
        btnsizer.Add(btn, 0, wx.ALIGN_RIGHT|wx.RIGHT, 5)
        if button:
            if len( button ) != 2: raise ValueError("The optional button arg must specify a ('Button Text', callback) tuple.")
            helpBtn = wx.Button(dlg, -1, button[0] )
            helpBtn.Bind( wx.EVT_BUTTON, lambda x: button[1]() )
            btnsizer.Add( helpBtn, 0, wx.ALIGN_RIGHT|wx.RIGHT, 5 )
        sizer.Add( btnsizer, 0, wx.BOTTOM|wx.ALIGN_CENTER|wx.CENTER|wx.TOP, 8 )

        cbox = None
        if showCheckBox:
            cboxsizer = wx.BoxSizer(wx.HORIZONTAL)
            cbox = wx.CheckBox( dlg, -1, "Don't show this again." )
            cboxsizer.Add( cbox, 0, wx.ALIGN_LEFT )
            sizer.Add(cboxsizer, 0, wx.BOTTOM|wx.ALIGN_LEFT|wx.LEFT|wx.TOP, 8)

        dlg.SetSizer(sizer)
        sizer.Fit(dlg)
        dlg.ShowModal()
        return cbox and cbox.GetValue() or False
    finally:
        dlg.Destroy()


def About( title=None, text=None, icon=None ):
    """
    Standard "About" item in help menu
    """

    txt = 'About Version'
    
    try:
        import winreg
        try:
            hkey    = winreg.CreateKey( winreg.HKEY_LOCAL_MACHINE,
                            'System\CurrentControlSet\Control\Session Manager\SubSystems' )
            txt += '  Session Settings:\n       ' + str(winreg.QueryValueEx( hkey, 'Windows' )[0])
        finally:
            winreg.CloseKey( hkey )
    except:
        import traceback
        traceback.print_exc()

    title = title
    text = text
    icon = icon or lib.image.getImageFromIcon( lib.image.getAppIcon() )
    font = ui.elems.Font( size=11, family=ui.elems.FontFamily.SWISS,
                            style=ui.elems.FontStyle.NORMAL,
                            weight=ui.elems.FontWeight.NORMAL,
                            forceClearType=True )
    return ScrolledMessage( txt, title=title, header=text, icon=icon, size=(600,500),
                            hscroll=True, headerFont=font )



class SimpleSearchDialog(wx.Dialog):
    """
    A dialog that shows a text field and a checkbox to add an __init__.py file within the directory.
    """
    def __init__(self, parent, id, start, searchType):
        title = 'Search for ...'
        wx.Dialog.__init__(self, parent, id, title)
        sizer = wx.BoxSizer(wx.VERTICAL)

        label = wx.StaticText(self, -1, 'Search for ...')
        sizer.Add(label, 0, wx.ALL, 5)

        box = wx.FlexGridSizer(2, 2, 5, 5)  # rows, cols, hgap, vgap
        box.AddGrowableCol(1)

        # Start location
        label = wx.StaticText(self, -1, "Start:")
        box.Add(label, 0, wx.ALIGN_RIGHT|wx.LEFT, 5)
        self.start = wx.TextCtrl(self, -1, start, size=(240,-1))
        box.Add(self.start, 0, wx.GROW)

        # What to search for
        label = wx.StaticText(self, -1, "Name:")
        box.Add(label, 0, wx.ALIGN_RIGHT|wx.LEFT, 5)
        self.objname = wx.TextCtrl(self, -1, "", size=(240,-1))
        box.Add(self.objname, 0, wx.GROW)

        # What type to search for
        label = wx.StaticText(self, -1, "Type:")
        box.Add(label, 0, wx.ALIGN_RIGHT|wx.LEFT, 5)
        self.objtype = wx.TextCtrl(self, -1, searchType, size=(240,-1))
        box.Add(self.objtype, 0, wx.GROW)

        ##Hide them if the searchType is specified as it would be in AST
        if searchType:
            label.Hide()
            self.objtype.Hide()

        #Case sensitive flag
        self.caseCheckBox = wx.CheckBox(self,-1,"Case Sensitive:", style=wx.ALIGN_RIGHT)
        box.Add(self.caseCheckBox,0,wx.GROW, )

        sizer.Add(box, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)

        line = wx.StaticLine(self, -1, size=(20,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP|wx.LEFT, 5)

        btnsizer = wx.StdDialogButtonSizer()

        btn = wx.Button(self, wx.ID_OK)
        btn.SetDefault()
        btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_CANCEL)
        btnsizer.AddButton(btn)
        btnsizer.Realize()

        sizer.Add(btnsizer, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER|wx.ALL, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.CenterOnParent()

    @staticmethod
    def display(parent, ppanel, start='/', searchType=None):
        """
        Displays the SearchDisplay dialog and applies the needed
        operations on the given dbhandler
        """
        dlg = SimpleSearchDialog(parent=parent, id=wx.NewId(), start=start, searchType=searchType)
        if dlg.ShowModal() == wx.ID_OK:
            try:
                print('Searching for %s of %s' %(dlg.objname.GetValue(),dlg.objtype.GetValue()))
            except:
                ErrorTrace('Error searching for %s' %dlg.objname)
                ## anything bad - return None so dialog don't do something worse
                return None
        else:

            return None

        return (dlg.start.GetValue(),dlg.objname.GetValue(),dlg.objtype.GetValue(),dlg.caseCheckBox.GetValue())


class FontDialog( object ):
    """
        Simplified wrapper of wx.FontDialog.
    """
    def __init__( self, faceName='', size=9, italic=False, bold=False, underline=False ):
        fd = wx.FontData()
        fd.InitialFont = wx.Font(
            size,
            wx.FONTFAMILY_DEFAULT,
            wx.FONTSTYLE_ITALIC if italic else wx.FONTSTYLE_NORMAL,
            wx.FONTWEIGHT_BOLD  if bold   else wx.FONTWEIGHT_NORMAL,
            underline = underline,
            face      = faceName
        )
        self._fontData = fd
        self._font     = None   # selected font

    def show( self, parent = None ):
        """
            Displays modal font dialog box.
            Returns True if user made selection, False otherwise
        """
        fdlg = wx.FontDialog( parent if parent else _getparent(), self._fontData )
        if fdlg.ShowModal() != wx.ID_OK:
            return False
        self._fontData = fdlg.FontData
        self._font     = fdlg.FontData.ChosenFont
        return True

    def _checkFont( self ):
        ''' checks that the font object is not None '''
        if self._font is None:
            raise RuntimeError( "No font has been selected" )

    def font( self ):
        ''' returns wx.Font object representing selected font or None if not yet selected '''
        return self._font

    def faceName( self ):
        """ face name of the selected font (after show() was called) """
        self._checkFont()
        return self._font.FaceName

    def size( self ):
        ''' point size of the selected font '''
        self._checkFont()
        return self._font.PointSize

    def isBold( self ):
        ''' is the selected font Bold? '''
        self._checkFont()
        return self._font.Weight == wx.FONTWEIGHT_BOLD

    def isItalic( self ):
        ''' is the selected font Italic? '''
        self._checkFont()
        return self._font.Style == wx.FONTSTYLE_ITALIC

    def isUnderlined( self ):
        ''' is the selected font Underlined? '''
        self._checkFont()
        return self._font.GetUnderlined()


def selectFont( parent = None, faceName='', size=9, italic=False, bold=False, underline=False ):
    ''' returns wx.Font object selected from FontDialog or None '''
    fd = FontDialog( faceName, size=size, italic = italic, bold = bold, underline = underline )
    if fd.show( parent ):
        return fd.font()

    return None

def selectFontProfile( fontProfile = None ):
    """ returns font profile string of the selected font or None,
        fontProfile is the current font profile string in format:
            faceName,pointSize[,style]
            where style is any combination of: 'bold', 'italic' or 'underlined' concatenated with '.'
    """
    fname = 'Arial'
    psize = 9
    style = ''
    if fontProfile:
        fp = fontProfile.split( ',' )
        if fp:
            fname = fp[0]         # font face name
        if len(fp) > 1:
            psize = int( fp[1] )  # font point size
        if len(fp) > 2:
            style = fp[2]

    fd = FontDialog(
        faceName  = fname,
        size      = psize,
        bold      = 'bold'       in style,
        italic    = 'italic'     in style,
        underline = 'underlined' in style
    )

    if fd.show():
        res = '%s,%d'% ( fd.faceName(), fd.size() )
        s = []
        if fd.isBold():
            s.append( 'bold' )
        if fd.isItalic():
            s.append( 'italic' )
        if fd.isUnderlined():
            s.append( 'underlined' )
        if s:
            res += ',' + '.'.join( s )
        return res

    return None
