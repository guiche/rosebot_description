'''
Created on 20 Jun 2014

@author: j913965
'''

import io as StringIO
import math
import types
import unicodedata
import collections

#LINT_DISABLE Override Builtin

LOCALE_INFO = {
    'THOUSANDS' :   ',',
    'DECIMAL'   :   '.'
}

SIMPLE_TYPES = [ int,float,str ]
COMMON_CONTAINERS = [ set,list,tuple ]
INFINITIES = dict( (float(n),n) for n in ('-inf','inf') )


def remove_trail_zeros_ifneeded( value, svalue, remove_trailing_zeros ):
    if not remove_trailing_zeros: return svalue
    if type(value) == float and value != round(value):
        svalue = svalue.rstrip('0')
        # lib.logging.info('STRIPPING ZEROS --- after %s', svalue)
    return svalue

def number( Val, Decimals, remove_trailing_zeros = False, convert_pseudo_num = False, minPrecision=None ):
    '''Converts a number to a reasonable-looking string in a platform independent way.'''
    # We roll our own implementation here, which isn't ideal - I tried using locale
    # for a while, but it behaves different on different platforms and involves having
    # to set global flags, which is a bit ugly. I'm sure somehow could come up with
    # something more pretty than what I've got below...

    # optionally allow for +/- infinity as well as nan (format func returns 'ERROR' otherwise)
    if convert_pseudo_num:
        if( Val in INFINITIES ):
            return INFINITIES[ Val ]
        if( isinstance(Val,float) and math.isnan(Val) ):
            return 'nan'

    # don't lose accuracy on > 32bit integers by inadvertently force-casting to float via '%.*f' format string...
    if isinstance( Val,int ):
        fmt_num = format( '%d', Val, grouping=1, remove_trailing_zeros=remove_trailing_zeros, minPrecision=minPrecision )
        if( Decimals > 0 ):
            fmt_num = '%s.%s' % ( fmt_num,('0'*Decimals) )
        return fmt_num

    # could speed this up 25% by inlining format here...
    return Format( '%.*f', (Decimals, Val), grouping=1, remove_trailing_zeros = remove_trailing_zeros, minPrecision = minPrecision )

def _removeUnicodeFromVal(v):
    ''' Convert any unicode values in v to strings '''
    if isinstance(v, dict):
        return removeUnicodeFromDict(v)
    elif isinstance(v, str):
        return ascii(v)
    elif isinstance(v, list):
        return [ _removeUnicodeFromVal(i) for i in v ]
    elif isinstance(v, tuple):
        return tuple( _removeUnicodeFromVal(i) for i in v )
    else:
        return v

def removeUnicodeFromDict(d):
    ''' Convert any keys or values in a dict to strings if they are unicode '''
    return dict( (_removeUnicodeFromVal(k), _removeUnicodeFromVal(v)) for k,v in d.items() )

def str2number( Str ):
    '''Convert a formatted number-string to a float number, the reverse function of number()'''
    return float( ''.join( Str.split( ',' )))

def Format( f, Val, grouping=0, remove_trailing_zeros = False, minPrecision = None ):
    num = Val
    if type(Val) is tuple:
        num = Val[-1]

    if num is None: return ''
    try:
        if math.isinf( num ) or math.isnan( num ):
            return 'ERROR'

        Str = f % Val
        if remove_trailing_zeros and '.' in Str:
            Str = Str.rstrip('0')
            if Str[-1] == '.':
                # we never want to return a string representation of a number that ends in a period. so we strip the '.'
                Str = Str.rstrip('.')

    except TypeError:
        ## someone passed in a string maybe
        return 'ERROR'

    pos     = Str.find('.')
    intPart = Str if pos == -1 else Str[:pos]
    decPart = Str[pos+1:] if pos != -1 else ''

    if minPrecision:
        assert minPrecision > 0
        zerosToAdd = max(0, minPrecision-len(decPart))
        if zerosToAdd > 0:
            decPart = decPart + '0'*zerosToAdd

    decPart = LOCALE_INFO['DECIMAL'] + decPart if decPart else decPart

    if grouping:
        sep     = LOCALE_INFO['THOUSANDS']
        res     = []
        for i in range( len(intPart), 0, -3 ):
            res.insert(0, intPart[max(0,i-3):i] )

        res = sep.join( res ) + decPart
    else:
        res = intPart + decPart
    return res.replace( '-,', '-' ) # in case the minus sent it one over

def _terse_str_i( o, s, limit, memo ):
    n    = max(limit - s.tell(),10)
    if type(o) == str:     # special case strings
        s.write( o[:n] )
        return s
    elif type( o ) == str:
        s.write( o.encode('ascii', 'replace') )
        return s
    elif type(o) in SIMPLE_TYPES:
        s.write( str(o) )
        return s
    elif hasattr( o, '__terse_str__' ) and isinstance( o.__terse_str__, collections.Callable):
        o.__terse_str__( s, limit-s.tell() )
        return s

    it = None
    enum = False

    if type( o ) is not type:
        # try getting item and ignore errors if this is not a container
        # check for iteritems, items being callable, in case types ignore
        # the iteration convention and reuse the same names
        if hasattr( o, 'iteritems' ) and isinstance( o.iteritems, collections.Callable):
            try:
                it = iter(o.items())
            except Exception:
                # We tried to get data for display without knowing anything about a type
                # The failure is likely a result of us calling methods without knowlede of a specific type
                pass

        if it is None and hasattr( o, 'items' ) and isinstance( o.items, collections.Callable):
            try:
                it = list(o.items())
            except Exception:
                # We tried to get data for display without knowing anything about a type
                # The failure is likely a result of us calling methods without knowlede of a specific type
                pass

        if it is None and type(o) in COMMON_CONTAINERS:
            it = enumerate( iter(o) )
            enum = True

    if it is None:
        # hmm punt to string is probably not good since it could be slow..
        s1 = str( o )
        s.write( s1[:n] )
        if len(s1) > n:
            s.write( '...' )

        return s

    oid = id(o)
    if oid in memo:
        s.write( '<R:%8x>' % (oid,) )
        return s
    try:
        memo.add( oid )

        sep = ''
        s.write( enum and '[' or '{' )
        for k,v in it:
            s.write( sep )
            if s.tell() > limit:
                s.write( '...' )
                break

            if enum:
                _terse_str_i( v, s, limit, memo )
            else:
                _terse_str_i( k, s, limit, memo )
                s.write( ':' )
                _terse_str_i( v, s, limit, memo )
            sep = ', '
        s.write( enum and ']' or '}' )
    finally:
        memo.remove( oid )

    return s

def terse_str( obj, limit=128 ):
    '''Attempt to efficiently generate a terse string representation.'''
    return _terse_str_i( obj, StringIO.StringIO(), limit, set() ).getvalue()

def camelKiller(camel):
    """
    Split up words in a camel case string.

    E.g. turns CamelCase into Camel Case, and FXOption into FX Option.
    """
    s = ''
    for i,c in enumerate(camel):
        if i and c.isupper() and i<len(camel)-1 and camel[i+1].islower():
            s += ' '
        s += c
    return s

def strip_accents(s):
    ''' Remove accented vowels '''
    return ''.join( c for c in unicodedata.normalize('NFD',s) if unicodedata.category(c) != 'Mn' )

def ascii( s, sevenBit=False ):
    """
    Make sure string is ascii.
    Unicode will be converted.
    """
    if isinstance(s, str):
        # downcast to unaccented chars
        s = strip_accents(s)
        # flatten common unicode quotes/dashes
        s = s.replace( '\u2011', '-' )  # non-breaking hyphen
        s = s.replace( '\u2012', '-' )  # figure-dash
        s = s.replace( '\u2013', '-' )  # en-dash
        s = s.replace( '\u2014', '-' )  # em-dash
        s = s.replace( '\u2015', '-' )  # horizontal bar
        s = s.replace( '\u00a0', ' ' )  # non-breaking space
        s = s.replace( '\u2018', "'" )  # single-quote (left)
        s = s.replace( '\u2019', "'" )  # single-quote (right)
        s = s.replace( '\u201c', '"' )  # double-quote (left)
        s = s.replace( '\u201d', '"' )  # double-quote (right)
        return s.encode( 'ascii', 'replace' )
    elif sevenBit:
        s = s.replace( '\xe9', 'e' )
        return s
    else:
        return s

def levenshteinDistance( x, y, verbose=False ):
    '''Calculates the Levenshtein distance between strings x and y'''
    q={}

    for ix in range(-1,len(x)):
        q[ix]={}

        for iy in range(-1,len(y)):
            q[ix][iy]=0.0

    for ix,cx in [(-1,None)]+[qq for qq in enumerate(x)]:
        for iy,cy in [(-1,None)]+[qq for qq in enumerate(y)]:
            if ix==-1:
                if verbose:
                    print("{0},{1} b -----------".format(ix,iy), end=' ')
                q[ix][iy]= float(iy)+1.0
            elif iy==-1:
                if verbose:
                    print("{0},{1} c -----------".format(ix,iy), end=' ')
                q[ix][iy]= float(ix)+1.0
            elif cx==cy:
                evalme = [(q[ix-1][iy]+1.0),(q[ix][iy-1]+1.0),(q[ix-1][iy-1])]
                if verbose:
                    print("{1},{2} d:{0}".format("|".join(str(x) for x in evalme),ix,iy), end=' ')
                q[ix][iy]=min(evalme)
            else:
                evalme = [q[ix-1][iy]+1.0,q[ix][iy-1]+1.0,q[ix-1][iy-1]+1.0]
                if verbose:
                    print("{1},{2} e:{0}".format("|".join(str(x) for x in evalme),ix,iy), end=' ')
                q[ix][iy]=min(evalme)

            if verbose:
                print("({0},{1},{2})".format(cx,cy,q[ix][iy]), end=' ')

        if verbose:
            print()

    return q[len(x)-1][len(y)-1]
