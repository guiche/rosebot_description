'''
Created on 24 Sep 2012

@author: Zarastro

     Ajout de methodes de 'property'es pour faciliter l'acces a un vecteur de booleens.
'''

def mode_getter( nomAttr, i ):
    def fget( obj ):
        return getattr( obj, nomAttr ) & i > 0
    return fget
  
def mode_setter( nomAttr, i ):
    
    def fset( obj, val ):
        if val ^ ( getattr( obj, nomAttr ) & i > 0 ) :
            setattr( obj, nomAttr, getattr( obj, nomAttr ) ^ i )
            
    return fset

def AjouteProprietes( Class, attrSousJacent, Modes, Noms, suffixe='_', prefixe='' ):
    '''attrSousJacent : nom du vecteur de booleens sous jacent
    '''
    for Nom in Noms:
        i = getattr( Modes, Nom )
        setattr( Class, '%s%s%s'%(prefixe,Nom,suffixe), property(fget=mode_getter(attrSousJacent,i), fset=mode_setter(attrSousJacent,i) ) )
  
