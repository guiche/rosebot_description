'''
Created on 20 Jun 2014

@author: j913965
'''

STDOUT=1
STDERR=2
import io
import sys

class RedirectStderr(object):
    def __init__(self,f):
        self.f = f

    def __enter__(self):
        self.oldf  = sys.stderr
        sys.stderr = self.f

    def __exit__(self,*exc_info):
        sys.stderr = self.oldf

class RedirectStdout(object):
    def __init__(self,f):
        self.f = f

    def __enter__(self):
        self.oldf  = sys.stdout
        sys.stdout = self.f

    def __exit__(self,*exc_info):
        sys.stdout = self.oldf

class StringOut(object):
    """Redirect print output to a string. Usage is:

    with StringOut() as s:
        print 'foo'
        myfunction()

    All the prints will be captured to s.buf. You can convert it to a string by doing s.getString()"""

    def __init__(self,flags=STDOUT):
        self.buf = io.StringIO()
        self.flags = flags

    def __enter__(self):
        if self.flags & STDOUT:
            self.oldStdout  = sys.stdout
            sys.stdout      = self.buf
        if self.flags & STDERR:
            self.oldStderr  = sys.stderr
            sys.stderr      = self.buf
        return self

    def __exit__(self,*exc):
        if self.flags & STDOUT:
            sys.stdout      = self.oldStdout
        if self.flags & STDERR:
            sys.stderr      = self.oldStderr

    def getString(self):
        """Return the contents of the buffer as a string"""
        return self.buf.getvalue()
