'''
Created on 20 Jun 2014

@author: j913965
'''
import gc
import sys

def memUsed( pid=None, garbageCollect=False, infofield='PagefileUsage' ):
    '''returns memory used by python in Mbytes
    Optionally, use paramger pid to find memory usage of a particular process'''

    if garbageCollect:
        gc.collect()

    if sys.platform == 'win32':
        import win32process, win32api, win32con # from pywin32
        handle=-1 # INVALID_HANDLE_VALUE
        if pid is not None:
            handle = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION, 0, pid)
        ret = float(win32process.GetProcessMemoryInfo(handle)[ infofield ])/(1024.0*1024.0)
        if handle != -1:
            win32api.CloseHandle(handle)
        return ret
    else:
        status_file = pid and '/proc/%d/statm' % pid or '/proc/self/statm'
        stats = file( status_file, 'rb' ).read( 256 )
        stats = stats[ : stats.index(' ') ]
        return float( stats ) / 256.0 # 4KB page / 1MB
    
    raise ValueError('Unable to find memory usage')