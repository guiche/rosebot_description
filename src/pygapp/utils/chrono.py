

import sys
import time
from utils.stringformat import number
from   utils.memusage import memUsed


if sys.platform.startswith('win'):
    from ctypes import windll, c_ulonglong, byref
    class _ITimerWin:
        def __init__(self):
            self.k32  = windll.kernel32
            self.freq = c_ulonglong(0)
            windll.p = self.k32.QueryPerformanceFrequency( byref( self.freq ))

        def start(self):
            self.now = c_ulonglong(0)
            self.k32.QueryPerformanceCounter( byref( self.now ))

        def stop(self):
            now = c_ulonglong(0)
            self.k32.QueryPerformanceCounter( byref( now ))
            self.elapsed = float(now.value-self.now.value)/self.freq.value
            return self.elapsed
    ITimer = _ITimerWin
else:
    class _ITimer:
        def start(self):
            self.now = time.time()
        def stop(self):
            self.elapsed = time.time()-self.now
            return self.elapsed
    ITimer = _ITimer

class Chrono(object):
    """Chronometre, the usage is:

    with Timer( 'Pricing %s', book_name ):
        # do something

    By default we print out the time taken. first arg is a fmt string, rest is args.
    You can quiet it by doing:

    with Timer( quiet=true ) as t:
        # do someting
    print t.elapsed()

    Optional keyword arguments:
        
        setElapsed:   Pixie func which will be updated when the timer stops
        logSlow:      Only log messages if the elapsed time exceeds logThreshold
        
        quiet:        Disables automatic logging
        args:         Arguments for format string
        timer:        ITimer instance, can be used to provide a pre-existing timer
        precision:    The number of digits to the right of the decimal point which
                      will be written in log file
        logThreshold: number of seconds after which logSlow will log messages, defaults to 5 seconds.
        useMillis:    should elapsed() and log message represent time in
                      milliseconds
        profile:      run the block under profiler. dump profiler output if
                      logSlow is specified and logThreshold is exceeded.
                      (warning: profiling overhead will be incurred)
        name:         optional name.
        logMemUsage:  flag to indicate if memusage from the enter to exit should be included in the log message
        logMemThreshold: Amount of memory in MB after which logMemUsage will log messages, defaults to None.
        logMemUsageStrict: forces garbage collection before start/stop memory samples are collected.
    """
    def __init__(self, *args, **kwargs ):
        """Timer( fmt_string, fmt_args.. ) or Timer( quiet=True )"""
        self.name         = kwargs.get( 'name' )
        
        self.setElapsed   = kwargs.get( 'setElapsed' )
        self.logSlow      = kwargs.get( 'logSlow', False )
        self.logMemThreshold = kwargs.get( 'logMemThreshold', None )
        self.quiet        = kwargs.get( 'quiet' ) or self.setElapsed or self.logSlow or self.logMemThreshold
        
        self.args         = args or ('Timer',)
        self.timer        = ITimer()
        self.precision    = kwargs.get('precision',4)
        self.logThreshold = kwargs.get('logThreshold',5)
        self.useMillis    = kwargs.get('useMillis', False)
        self.logMemUsage  = kwargs.get('logMemUsage', False) or self.logMemThreshold is not None
        self.logMemUsageStrict = kwargs.get('logMemUsageStrict', False)

        if kwargs.get('profile'):
            import utils.profileur
            self.profiler = utils.profileur.ProfileCtx( printOnExit=False )
        else:
            self.profiler = None

    def __enter__(self):
        if self.profiler:
            self.profiler.__enter__()
        if self.logMemUsage:
            self._memStart = memUsed( garbageCollect = self.logMemUsageStrict )
        self.timer.start()


        return self

    def getLogTimerName(self):
        if self.name:
            return 'Timer:' + self.name + ' '
        return ''

    def __exit__(self,*args):
        self.timer.stop()
        if self.profiler:
            self.profiler.__exit__(*args)
        if self.logMemUsage:
            self._memStop = memUsed( garbageCollect = self.logMemUsageStrict )
            memDelta = self._memStop - self._memStart
            memLog = 'memUsage = %s MB (%s - %s)' % ( memDelta, self._memStop, self._memStart)
        else:
            memLog = ''
        elapsed = self.elapsed()
        if not self.quiet:
            print('%s%s took %s %s %s'%(
                          self.getLogTimerName(),
                          self.args[0] % tuple(self.args[1:]),
                          number( elapsed, self.precision ),
                          self.units(),
                          memLog))

        if self.setElapsed:
            self.setElapsed.setvalue( elapsed )

        doLog = self.logSlow and elapsed > self.logThreshold
        doLog = doLog or self.logMemThreshold and memDelta > self.logMemThreshold
        if doLog:
            self.logfunc( '%s%s took %s %s %s',
                self.getLogTimerName(),
                self.args[0] % tuple( self.args[1:] ),
                number( elapsed, self.precision ),
                self.units(),
                memLog)
            
            if self.profiler:
                import utils.io
                with utils.io.StringOut() as profileRes:
                    self.profiler.print_stats()

                self.logfunc( '%s profile: %s',
                    self.args[0],
                    profileRes.getString() )


    @classmethod
    def profileDir( cls ):
        import tempfile
        return tempfile.tempdir


    def elapsed(self):
        if self.useMillis:
            return self.timer.elapsed * 1000
        return self.timer.elapsed

    def getMemUsage(self):
        """
        If logMemUsage was passed on __init__, this can be called after context
        exit to obtain the memory stats.
        """
        memDelta = self._memStop - self._memStart
        memLog = 'memUsage = %s MB (%s - %s)' % ( memDelta, self._memStop, self._memStart)
        return (memDelta, self._memStart, self._memStop)

    def units(self):
        if self.useMillis:
            return "ms"
        return "seconds"


class _DisjointTimer(object):
    """Primitive timer with singleton for timing performance in disjoint
       areas of code, particularly in async and GUI code. To use it in 2
       methods, in different modules, for example (note the singleton is used):

       module_a.py
       -----------
       def method_a():
           DisjointTimer.update()

        module_b.py
        -----------
        def method_b():
            print "Time since method_a", DisjointTimer.update()
    """

    def __init__(self):
        self._t = time.time()

    def peek(self):
        return time.time() - self._t

    def update(self):
        delta = time.time() - self._t
        self._t = time.time()
        return delta

# Singleton
DisjointTimer = _DisjointTimer()
