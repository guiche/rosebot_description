'''
Created on 28 Nov 2012

@author: J913965
'''

from pygapp import sauvegarde
from . import media
import os
import logging

try:
    import pygame
    from pygame import mouse as souris
    TOUCHES_NUM = set((pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5,
                       pygame.K_6, pygame.K_7, pygame.K_8, pygame.K_9, pygame.K_0))
except:
    TOUCHES_NUM = None

class clic(object):
    
    gauche = 1
    milieu = 2
    droit  = 3
    
    mol_bas  = 4
    mol_haut = 5
 
    @classmethod
    def droite_enfonce(cls):
        return souris.get_pressed()[2]
    
    @classmethod
    def gauche_enfonce(cls):
        return souris.get_pressed()[0]

class EtatSouris(object):
    
    def __init__(self):
        self.jalons   = []

        
def SelecteAjoutRetrait(Elem, Selection, Empile):
    """ Ajout / Retrait d'un Element a une liste 
        :type Selection: set
        :type Empile: bool
    """
    
    if Empile:
        if Elem in Selection:
            Selection.remove(Elem)
        else:
            Selection.add(Elem)
    else:
        Selection.clear()
        Selection.add(Elem)

def ListeDesParties():
    return sauvegarde.ListeRepertoire(media.SAUVE_REP, sauvegarde.SUFF_PARTIE)
    
def SelectePartie(defaut=None, choixNouveau=True, valideExistant=True, Legende='Choisir la partie:'):
    
    Rep = media.SAUVE_REP
    
    if defaut:
        Rep = os.path.join(Rep, os.path.dirname(defaut))
                  
    return sauvegarde.SelectDansRepertoire(Rep, sauvegarde.SUFF_PARTIE, Legende=Legende, choixNouveau=choixNouveau, valideExistant=valideExistant)

def Ouvrir(nom='', Legende=None):
    
    if not nom:
        nom = SelectePartie(choixNouveau=False, valideExistant=False, Legende=Legende)
        
    return sauvegarde.Ouvrir(nom,dossier=media.SAUVE_REP,suffixe=sauvegarde.SUFF_PARTIE)

class PasAPas(object):
    
    Non         = ''
    Tour        = 'Tour'
    SelecSeule  = 'SelectionSeule'
    Selection   = 'Selection'
    Unite       = 'Unite'
    

def ModesPasAPas():
    return PasAPas.Non, PasAPas.Tour, PasAPas.SelecSeule, PasAPas.Selection, PasAPas.Unite

PasAPas.EnsembleValeurs = ModesPasAPas()

class PasAPasSelecSeuleException(Exception):
    pass

class SousEnsembleException( Exception ):
    
    def __init__(self, Coupables, *args):
        Exception.__init__( self, *args )
        self.coupables = Coupables
        
    def __repr__(self):
        return Exception.__repr__(self) + " Coupables : " + " ".join(str(coupable) for coupable in self.coupables)

    __str__ = __repr__


class NouvellePartieException(Exception):
    
    def __init__(self, partie=None, tour_num=None, **argsPartie):
        self.partie     = partie
        self.tour_num   = tour_num
        self.argsPartie = argsPartie

    def Ouvrir(self):
        
        if isinstance(self.partie, str):
            nomPartie = self.partie
        else:
            nomPartie = self.partie.nom
            if self.partie.nom not in ListeDesParties():
                return self.partie
    
        InstanceJeu = Ouvrir( nomPartie )
        
        if not isinstance(self.partie, str):
            
            for param in 'VueOrg', 'VueRatio', 'SimTimeFactor', 'VueAngle':
                setattr(InstanceJeu,param,getattr(self.partie,param))
                            
        if self.tour_num:
            saute_tours = self.tour_num - InstanceJeu.NumeroDuTour
            if saute_tours > 0:
                InstanceJeu.pasapas_saute_tours = saute_tours
                if not InstanceJeu.Const_IPS:
                    logging.warning("Remontage : correction d'IPS variable dans la partie d'origine")
                    InstanceJeu.Const_IPS = True
            else:   
                logging.warning('Ne peux pas revenir au tour %d'%self.tour_num)
        
        for nom_attr, val in self.argsPartie.items():
            setattr(InstanceJeu, nom_attr, val)
            
        return InstanceJeu        
