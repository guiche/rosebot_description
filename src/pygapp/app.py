from . import sauvegarde, menus, console
try:
    from .utils import profileur
except:
    profileur = None

from .partieutils import SelectePartie
from .partieutils import PasAPas, ModesPasAPas, PasAPasSelecSeuleException, SousEnsembleException, NouvellePartieException
from .partieutils import EtatSouris, clic
from . import media
import os
import traceback
import collections
from . import vect2d
import math
import logging

CouleurContourSelec = 0, 63, 0
CouleurNumerosVert  = 170, 200, 255


class Application(sauvegarde.Phenixable):
    
    couleur_meta = 225, 150, 205
    couleur_select = 0, 255, 0
    
    Vec = vect2d.Vec
    
    Surf = None
    etatSouris = EtatSouris()
    
    def __init__(self, DimEcran=(400, 600),nom=''):
        
        self.nom = nom

        self.ImgParSec  = 50 # Images Par Seconde (souhaite)
        self.AfficheIPS = True # Images par seconde
        self.Const_IPS  = False # force le meme intervalle de temps entre chaque image.
        self.SimTimeFactor = 1 # Temps Simulation = SimTimeFactor * Temps Reel
        self.TempsT = 0 # Temps de simulation ecoule depuis le debut de la simulation
        
        self.pasapas = PasAPas.Non
        self.pasapas_saute_tours = 0


        self.Boucle   = True
        self.Courir   = True
        self.NumeroDuTour = 0
        self.Interruption = None        
        
        self.SauvegardeAutoActivee = False

        self.Rediffusion = 0
        self.Enregistre  = True
        self.Enregistrement  = collections.OrderedDict() # [ (NumTour, [Evenement, ] ), ... ]

        self.VueOrg = self.Vec(0, DimEcran[1])  # Origine du referentiel d'affichage dans le referentiel absolu
        self.VueRatio = 1.  # Facteur de grossissement du referentiel d'affichage
        self.VueAngle   = None #self.Vec(1/math.sqrt(2),1/math.sqrt(2)) # angle de rotation de la carte. [1,1] pour la rotation identite. 
        self.VueLimites = None #Vec(xmin, ymin), Vec(xmax, ymax) # coordonnees reeles de l'aire visible si elle est limitee
         
        self.DimEcran       = self.Vec(*DimEcran)
        self.DimEcranPetit  = self.Vec(*DimEcran)   
        self._PleinEcran    = DimEcran is None

        #InfoObject = pygame.display.Info()
        #self.ResEcran =

        self.Affiche        = True
        self.Afficher       = True
        self.AfficheNumeros = False
        self.AfficheAlpha   = 0
        self.AfficheConsole = False
        self.ClicSourisPrec = [None] * 10  # 10 boutons souris ?

        self.image_fond     = ''#'robomovies.jpg'
        self.image_pos      = self.Vec(0,0)#self.Vec(-25,25) # position du coin basgauche de l'image de fond
        self.image_echelle  = 1#2
        
        self.ActiveSons = True
        
        self.Selection       = set()
        self.SurvolSelection = True  # Affiche la selection potentielle sous le curseur de la souris
        self.SuitSelection   = None
        self.VueSubjective   = False

        self.ModeInfra       = False

        self._validation = 0

        self._Gommer_prop = False
        self.TailleGomme  = 15

    
    def __str__(self):
        return '%s instance : %s' % (type(self),self.nom)

    __exc__state__ = '_Surf', 'SauvegardeAuto', 'font', 'console', 'horloge',# champs non persistes
    
    @property
    def SauvegardeAutoActivee_(self):
        return self.SauvegardeAutoActivee
   
    @SauvegardeAutoActivee_.setter
    def SauvegardeAutoActivee_(self, val):
        if val:
            self.AutoSauveExc()
        self.SauvegardeAutoActivee = val

    def AutoSauveExc(self):
        self.SauvegardeAuto = sauvegarde.SauvegardeAutoExc( self )

        if hasattr(self, 'horloge'):
            self.horloge.tick()

    def InitPreBoucle(self):

        if self.Affiche:
            import pygame
            if not pygame.display.get_init():
                try:
                                
                    #Pygame init
                    pygame.init()
                    pygame.mixer.init()
                    pygame.mouse.set_visible(1)
                    
                except:
                    traceback.print_exc()
                    input('Appuyez sur Entree pour fermer')
            
                # format loggin
                logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%H:%M:%S')
        
            if not pygame.font:  print('Warning, fonts disabled')
            if not pygame.mixer: print('Warning, sound disabled')

            self.horloge = pygame.time.Clock()

            self.ConfigEcran(self.DimEcran)

            self.font = pygame.font.Font(media.cheminFichier(media.FONTE_DEFAUT, subdir='fonts'), 15)
            self._Surf = pygame.display.get_surface()

    def get_event(self):
        import pygame
        if self.Rediffusion:
            # rejoue les evenements enregistres
            for (ev_type, ev_info) in self.Enregistrement.get(self.NumeroDuTour, []):
                even = pygame.event.Event(ev_type, ev_info)
                if ev_type == pygame.MOUSEMOTION:
                    pygame.mouse.set_pos(even.pos)
                yield even
            
            #for event in pygame.event.get():
            #    yield event
            
        else:
            for even in pygame.event.get():

                if self.Enregistre:
                    # enregistre les evenements
                    self.Enregistrement.setdefault(self.NumeroDuTour, []).append((even.type, even.dict))
                yield even
                
    def Finalisation(self):
        pass
    
    @property
    def PleinEcran(self):
        """ Mode plein ecran """
        return self._PleinEcran

    @PleinEcran.setter
    def PleinEcran(self, val):
        if val != self._PleinEcran:
            if val:
                NouvDim = None
            else:
                NouvDim = self.DimEcranPetit
                
            self._PleinEcran = val
              
            self.ConfigEcran(NouvDim)
            
            if self.VueLimites:
                # force le respect des limites
                self.changeVue(org=self.VueOrg, ratio=self.VueRatio)
            
    def ConfigEcran(self, DimEcran=None):
        """ Mode d'affichage plein Ecran / fenetre """
        import pygame
        display = pygame.display.get_surface()
        
        if not display or DimEcran is None or display.get_size() != tuple(DimEcran):
            
            if DimEcran is None or self.PleinEcran:
                # sur le nouveau portable Asus, la resolution 1920x1080 n'est pas bien prise en compte
                # et l'image est rognee. Limitons donc a 1600x900, plus classique.
                DimEcran = self.Vec(1600, 900)
                #DimEcran = self.Vec(0,0)
                Flags = pygame.FULLSCREEN
            else:
                Flags = 0  # pygame.NOFRAME
                
            pygame.display.quit()
            self._Surf = pygame.display.set_mode((int(DimEcran[0]), int(DimEcran[1])), Flags)
                        
            SurfSize = self.Vec(*self._Surf.get_size())
            
            # Recallage pour que le coin inferieur gauche reste inchange
            self.changeVue(org=self.VueOrg+(0,SurfSize[1] - self.DimEcran[1]))
            
            self.DimEcran = SurfSize

        pygame.display.set_caption(self.nom)

    @property
    def VueCentre(self):
        """ Coordonnees reeles du centre de l'ecran d'affichage """
        milieuEcran = self.DimEcran / (2*self.VueRatio)
        milieuEcran[1] *= -1  # passage en referentiel direct
        milieuEcran += self.VueOrg
        return milieuEcran 
    
    @VueCentre.setter
    def VueCentre(self, CoordsReeles):
        """ Coordonnees reeles du centre de l'ecran d'affichage """
        milEcran = self.DimEcran / (2*self.VueRatio)
        milEcran[1] *= -1  # passage en referentiel direct
        
        self.changeVue(org=CoordsReeles - milEcran)
        
    def Cadrer(self, Elems, marge=1.5):
        
        p1, p2 = Elems[0].cadrage
           
        for Elem in Elems[1:]:
            
            p1_, p2_ = Elem.cadrage
            
            for i in 0, 1:
                p1[i] = min(p1[i], p1_[i])
                p2[i] = max(p2[i], p2_[i])
            
        p1 = self.Vec(*p1)           
        p2 = self.Vec(*p2)           
        Cadre = p2 - p1
         
        if Cadre[1] == 0:
            self.VueRatio = self.DimEcran[0] / Cadre[0] 
        elif Cadre[0] == 0:
            self.VueRatio = self.DimEcran[1] / Cadre[1]
        elif Cadre[0] / Cadre[1] > self.DimEcran[0] / self.DimEcran[1]:
            self.VueRatio = self.DimEcran[0] / Cadre[0]
        else: 
            self.VueRatio = self.DimEcran[1] / Cadre[1]
        
        self.VueRatio /= marge
        self.VueCentre = (p1 + p2) / 2
        
        
    def CoordsVues(self, CoordsReeles, Entier=False):
        """ Coordonnees dans le referentiel d'affichage a l'ecran """
        return self.CoordsVues_ep(self.Vec(*CoordsReeles), Entier=Entier)
        
    def CoordsVues_ep(self, CoordsReeles, Entier=False):
        """ Coordonnees dans le referentiel d'affichage a l'ecran """
        
        if self.VueAngle is not None:
            CoordsReeles.irot_vc(self.VueAngle, self.VueCentre)
        
        #CoordsReeles -= self.VueOrg
        #CoordsReeles *= self.VueRatio
        #CoordsReeles[1] *= -1  # passage en referentiel direct depuis referentiel a y inverse
        #optimisation :
        CoordsReeles[0] = (CoordsReeles[0]-self.VueOrg[0])*self.VueRatio
        CoordsReeles[1] = (self.VueOrg[1]-CoordsReeles[1])*self.VueRatio
        
        if Entier:
            CoordsReeles[0] = int(CoordsReeles[0])
            CoordsReeles[1] = int(CoordsReeles[1])
            
        return CoordsReeles

    def VecReel(self, v):
        """ conversion Vue->Reel pour un vecteur """
        return self.CoordsReeles(v)-self.VueOrg
        
    def CoordsReeles(self, CoordVues):
        """ Coordonnees dans le referentiel absolu de la carte """
        return self.CoordsReeles_ep(self.Vec(*CoordVues))
    
    def CoordsReeles_ep(self, CoordVues):
        """ Coordonnees dans le referentiel absolu de la carte """
        
        #CoordVues[1] *= -1  # passage en referentiel direct depuis referentiel d'affichage a y inverse
        #CoordVues /= self.VueRatio
        #CoordVues += self.VueOrg
        
        invRatio = 1/self.VueRatio
        CoordVues[0] =  CoordVues[0]*invRatio + self.VueOrg[0]
        CoordVues[1] = -CoordVues[1]*invRatio + self.VueOrg[1]
        
        if self.VueAngle is not None:
            CoordVues.iarot_vc(self.VueAngle, self.VueCentre) 
        
        return CoordVues
    
    def dessineCercle(self, centre, rayon, couleur=None, trait=1, coordsReeles=True):
        import pygame
        if couleur is None:
            coul = self.couleur_select
        else:
            coul = couleur
            
        t = trait

        if coordsReeles:
            p1 = self.CoordsVues(centre, Entier=True)
            r = max(1, int(rayon * self.VueRatio))
            
            if t > 0 and self.VueRatio < 1:
                t = max(1, int(trait * self.VueRatio))
                
            t = min(t, r)
            
        else:
            p1 = [int(a) for a in centre]
            r = rayon
        
        try:
            pygame.draw.circle(self._Surf, coul, p1, r, t)
        except:
            traceback.print_exc()
            print(coul, p1, r, t)
            
    def dessineLigne(self, *points, **kwargs):
        """ kwargs : couleur=None, coordsReeles=True, lisse=True, trait=1)"""
        import pygame
        couleur = kwargs.get('couleur')
        
        if couleur is None:
            coul = self.couleur_select
        else:
            coul = couleur
            
        trait = kwargs.get('trait',1)
        
        if kwargs.get('coordsReeles',True):
            pts = [ self.CoordsVues(p) for p in points ]
            t = trait
        else:
            pts = points
                        
            if self.VueRatio < 1:
                t = max(1, int(trait * self.VueRatio))
            else:
                t = trait
                    
        lisse = trait == 1 and kwargs.get('lisse',True)
        
        if len(points) == 2:
            if lisse:
                drawFunc = pygame.draw.aaline
            else:
                drawFunc = pygame.draw.line
            
            drawFunc(self._Surf, coul, pts[0], pts[1], t)
            
        else:
            if trait == 0:
                pygame.draw.polygon(self._Surf, coul, pts)
                
            else:
                if lisse:
                    drawFunc = pygame.draw.aalines
                else:
                    drawFunc = pygame.draw.lines
                
                drawFunc(self._Surf, coul, False, pts, t)

    def afficheParatexte(self, texte, pos, couleur=(150, 150, 150), coordsReeles=False, centrage=None):
        """
            centrage : ajustement de position du texte pour qu'il n'empiete pas sur le segment pos+centrage
        """
        texteSurf = self.font.render(texte, 1, couleur)
                
        if coordsReeles:
            # position absolue
            
            coinHautGauche = self.CoordsVues(pos, Entier=True)
            
        else:
            coinHautGauche = list(pos)
            # position en nombre reel est interpretee comme une fraction d'ecran
            # position entiere negative intepretee comme une distance au bord oppose.
            for i in 0, 1:
                if coinHautGauche[i] < 0:
                    coinHautGauche[i] += self.DimEcran[i]
                elif isinstance(coinHautGauche[i], float) and not coinHautGauche[i].is_integer():                
                    assert coinHautGauche[i] <= 1., (coinHautGauche[i], i)
                    coinHautGauche[i] = int(coinHautGauche[i] * self.DimEcran[i])

        if centrage:
            coinHautGauche = list(coinHautGauche)
            if centrage[0] < 0:
                coinHautGauche[0] -= texteSurf.get_width()
                
            if ( centrage[1] < 0 ) ^ coordsReeles:
                coinHautGauche[1] -= texteSurf.get_height()
                            
        self._Surf.blit(texteSurf, coinHautGauche)
        
    def PaintBackground(self):
        
        if self.image_fond:
            try:
                img = media.charge_image(self.image_fond, self.image_echelle*self.VueRatio)
                x,y = self.CoordsVues(self.image_pos)
                
                self._Surf.blit(img, (x,y-img.get_height()))
            except:
                if not hasattr(self, 'image_manquante'):
                    self.image_manquante = True
                    traceback.print_exc()
                       
    def PaintEraser(self):
        
        if self.Gommer:
            from pygame import mouse as souris
            import pygame
            # Dessin du curseur de gomme
            trait = 0
            Taille = self.TailleGomme - trait
            x, y = souris.get_pos()
            x -= Taille
            y -= Taille
            Contour = x, y, 2 * Taille, 2 * Taille
            pygame.draw.rect(self._Surf, (255, 255, 255), Contour, trait)

        
    def PaintMeta(self):
        import pygame
        from pygame import mouse as souris
        
        if self.AfficheConsole:
            self.console.affiche_texte()
        
        if self.Rediffusion:
            pygame.draw.polygon(self._Surf, (200,0,200), [(0,0),(0,self.DimEcran[1]),
                                                      (self.DimEcran[0],self.DimEcran[1]),(self.DimEcran[0],0)], 3)
        
            
        if self.Gommer:
            self.PaintEraser()
            
        elif self.etatSouris.jalons: 
            if not clic.droite_enfonce():
                # Affiche le contour de la Selection en cours    
                            
                pointlist = [ self.CoordsVues(Coors) for Coors in self.etatSouris.jalons ]
                
                posSouris = self.Vec(*souris.get_pos())
                if posSouris != pointlist[-1]:
                    pointlist.append(posSouris)
                    
                nombPoints = len(pointlist)
                if nombPoints > 1:
                    if self.SurvolSelection and nombPoints >= 2:
                        # Affiche les unites potentiellement selectionnes dans ce contour
                        coordsReeles = [ self.CoordsReeles(Coords) for Coords in pointlist ]
                        
                        self.SelectionneDansContour(contour=coordsReeles, affiche=True)
        
                    if nombPoints == 2:
                        # Affiche la boite de selection
                        x0, y0 = pointlist[0]
                        x1, y1 = pointlist[1]
                        Contour = x0, y0, x1 - x0, y1 - y0
                        pygame.draw.rect(self._Surf, CouleurContourSelec, Contour, 1)
                        
                    else:
                        # Affiche le contour polygonal de selection
                        pygame.draw.aalines(self._Surf, CouleurContourSelec, False, pointlist, 1)
                        
            elif not self.Selection:
                # Reglet pour mesurer la distance entre deux clics souris
                point1 = self.etatSouris.jalons[0]
                point2 = self.CoordsReeles(souris.get_pos())
                self.dessineLigne( point1, point2, couleur=(0,0,255), trait=1, lisse=True, coordsReeles=True )

                vectDist = point2 - point1

                distance = vectDist.nor

                if distance:
                    pos = (point1 + point2)/2 # vectDist.uni_nml * 20 / self.VueRatio
                    texteDist = '    %.1f' % distance
                    self.afficheParatexte(texteDist, pos, coordsReeles=True)
    
    def SelectionneDansContour(self, contour, affiche=False):
        pass
    
    def PeintUneToile(self,deltaT):
        pass
    
    def SelecteElemAPos(self, pos):
        return None
        
    def Validation(self,**kwargs):
        """ returns False if no problem """
        return False
    
    def ImprimeEcran(self,versPressePapier=True):
        '''
        NomModule = 'win32clipboard'
        try:
            win32clipboard = __import__(NomModule)
        except ImportError:
            print "Erreur: Imprime ecran necessite l'installation de %s"%NomModule
            return
         
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        win32clipboard.SetClipboardData(win32clipboard.CF_BITMAP, data)
        win32clipboard.CloseClipboard()
        '''
        import pygame
        if versPressePapier:
            pygame.scrap.init()
            pygame.scrap.set_mode (pygame.SCRAP_CLIPBOARD)
            image_data = pygame.image.tostring(self._Surf,"RGBA")
            pygame.scrap.put(pygame.SCRAP_BMP, image_data)
            print("sauvegarde d'ecran dans presse-papier")
            
        else:
            # vers fichier
            Surface = self._Surf.copy()
            Repertoire = media.IMAGE_REP
            NomFichier = sauvegarde.SelectDansRepertoire(Repertoire, Suffixe='', Legende="Sauver l'image d'ecran sous :", choixNouveau=True,valideExistant=True,Effacable=True)
            if NomFichier:
                print("sauvegarde d'ecran : %s"%NomFichier) 
                pygame.image.save( Surface, os.path.join(Repertoire,NomFichier+'.png') )
            
            
    def AfficheOmbre(self, Alpha):    
                    
        if Alpha:
            import pygame
            OmbreSurf = pygame.Surface(self.DimEcran)
            OmbreSurf.fill((0, 0, 0))
            OmbreSurf.set_alpha(Alpha)
            self._Surf.blit(OmbreSurf, (0, 0))
            del OmbreSurf

    def changeVue(self, org=None, ratio=None, angle=None):
        
        if ratio is not None:
            if self.VueLimites and ratio < self.VueRatio:
                dimLimites = self.VueLimites[1] - self.VueLimites[0]
                min_ratio = min( self.DimEcran[i]/dimLimites[i] for i in [0,1] )
                ratio = max(min_ratio, ratio)
                
            self.VueRatio = ratio
        
        if org is not None:
            if self.VueLimites:
                DimVue = self.DimEcran / self.VueRatio
                self.VueOrg[0] = min( max(org[0], self.VueLimites[0][0]), self.VueLimites[1][0]-DimVue[0] )
                self.VueOrg[1] = min( max(org[1], self.VueLimites[0][1]+DimVue[1]), self.VueLimites[1][1] )
            else:
                self.VueOrg[0] = org[0]
                self.VueOrg[1] = org[1]
            
        if angle is not None:
            self.VueAngle = angle
        
    def Recadre(self):
        ''' Reinit la vue '''
        self.changeVue(org=(0,self.DimEcran[1]), ratio=1, angle=None)
        self.VueAngle = None
            
    def Reinit(self):
        ''' Reinit '''
        self.VidangeSelect()
        #self.AfficheIPS = False
        #self.Const_IPS = False
        # self.Recadre()
        #self.PleinEcran = True
        self.AfficheConsole = False
        self.SurvolSelection = False
        self.pasapas = PasAPas.Non
        

        if self.NumeroDuTour:
            for numTour in list(self.Enregistrement.keys()):
                val = self.Enregistrement.pop(numTour)
                if numTour > self.NumeroDuTour:
                    self.Enregistrement[numTour-self.NumeroDuTour] = val

        self.NumeroDuTour = 0
        self.TempsT = 0            
    
    def GrandBoucle(self, MaxNombTours=None):
        """ Boucle principale """
        
        self.InitPreBoucle()
        
        self.console = console.Console(self)
         
        with self.console:
            try:
                boucleIter = self.boucleIter(exo_controle=MaxNombTours is not None)
                numTours = 0
                for _temps_tour in boucleIter:
                    if MaxNombTours is not None and numTours >= MaxNombTours:
                        break
                    numTours += 1
                    
            finally:
                self.Finalisation()


    def boucleIter(self, exo_controle=False):
                    
        DebutDeTour = True
        IPS           = 0
        JalonTemps_ms = 0
                
        Couleur_Noir = 0,0,0
        
        temps_tour = 1./self.ImgParSec # temps reel d'execution du tour
        deltaT = temps_tour # temps vu par le deroule du jeu
        
        # Statistique de temps par tour
        NombTours        = 1
        tot_temps_tour   = 0
        tot_temps_tour_2 = 0

        """
        import socket
        VoileDOmbre = socket.gethostname() == 'WC1133QPK'
        if VoileDOmbre:
            pygame.display.set_caption("cmd.exe")
        """
        VoileDOmbre = False
                        
        while self.Boucle:
            
            if self.Interruption:
                continue
                                                                 
            if self.SuitSelection:
                # Vue subjective : camera suit unite selectionnee
                #self.VueCentre = self.SuitSelection.pos
                diffPos = self.SuitSelection.pos - self.VueCentre
                
                distance = diffPos.nor * self.VueRatio
                if distance:
                
                    if self.VueSubjective:
                        max_dist = 10
                    else:
                        max_dist = 100
                        
                    if distance >= max_dist:
                        self.VueCentre += (distance-max_dist)/self.VueRatio * diffPos.uni
                    else:
                        #v_camera = 15
                        #self.VueCentre += diffPos.uni * min( deltaT * v_camera, distance)
                        pass
                     
                if self.VueSubjective:
                    # de telle sorte que l'orientation de l'unite pointe vers le haut.
                    if hasattr(self,'orient_'):
                        self.VueAngle  = self.Vec(self.SuitSelection.orient_[1], self.SuitSelection.orient_[0])
                
                
            # Peinture de la scene
            if self.Affiche:
                import pygame
                # Affiche l'arriere plan
                self._Surf.fill( Couleur_Noir )
                if self.Afficher:
                    self.PaintBackground()

                epitexte = ''
            
                if not self.Courir or self.pasapas:
                    AfficheIPS = True
                    if not self.Courir:
                        # pour ne pas prendre en compte le temps passe en pause
                        self.horloge.tick()
                        
                    epitexte = '%s     t=%.2fs; Tour %d'%( self.nom, self.TempsT, self.NumeroDuTour)
                    
                    if self.pasapas:
                        # enchainement du ralenti lorsque la touche espace reste enfoncee
                        
                        epitexte += ' - pas a pas : ' + self.pasapas
                        temps = pygame.time.get_ticks()
                        if temps - JalonTemps_ms > 220:
                            JalonTemps_ms   = temps
                            touches         = pygame.key.get_pressed()
                            self.Courir     = bool( touches[pygame.K_SPACE] and not touches[pygame.K_LCTRL] ) or touches[pygame.K_F10] or touches[pygame.K_F11]  
                    else:
                        epitexte += ' - pause'
                        
                        
                else:
                    AfficheIPS = self.AfficheIPS
                    
                if AfficheIPS:
                    # Affichage du nombre d'image par seconde
                    IPS = int( 1./temps_tour + .5 )
                    #IPS = int( ( 9*ImgParSec + 1./temps_tour ) / 10 + .5 )# lissage pour l'affichage
                    IPSmessage = 'IPS : %d'%IPS + '/%d' % self.ImgParSec 
                    if self.Const_IPS:
                        IPSmessage += 'c'
                    
                    self.afficheParatexte(IPSmessage,(-85,0))
                
                if self.SimTimeFactor != 1.:# affichage du temps de simulation 
                    self.afficheParatexte('x %.2f'%self.SimTimeFactor,(-85,15))
                    self.afficheParatexte('t %.2f'%self.TempsT,(-85,30))
                    
                self.PeintUneToile(deltaT * self.SimTimeFactor)
                
                self.PaintMeta()

                TouchesAppuyees = pygame.key.get_pressed()
                
                if epitexte or self.SurvolSelection or TouchesAppuyees[pygame.K_TAB]:
                    # Selection d'elements sous le pointeur souris.
                    from pygame import mouse as souris
        
                    posSouris = souris.get_pos()
                    LocElem = self.SelecteElemAPos(posSouris)
        
                if epitexte or self.SurvolSelection:
                    
                    self.afficheParatexte( epitexte, (10,0) )
                    
                    textePosSouris = str(self.CoordsReeles(posSouris))

                    # Affiche le nom d'un Element trouve sous le pointeur souris.
                    
                    if LocElem:
                        pos = self.Vec(*posSouris)+self.Vec(10,-10)
                        self.afficheParatexte(str(LocElem), pos, coordsReeles=False, couleur=CouleurNumerosVert)
                        
                        if (not self.Courir or self.pasapas) and LocElem is not None and LocElem not in self.Selection:
                            if hasattr(LocElem, 'AfficherInfo'):
                                LocElem.AfficherInfo(deltaT * self.SimTimeFactor, DansSelection=True)
                            
                    self.afficheParatexte(textePosSouris, (10,30))
            
                
            try:
                if self.Affiche:
                
                    if VoileDOmbre:                    
                        if not pygame.key.get_mods() & pygame.KMOD_CAPS and not pygame.key.get_pressed()[pygame.K_RCTRL]: 
                            self.AfficheOmbre(Alpha=175)

                    # Gestion de l'etat des peripheriques
                    self.TraiteEtat(deltaT=deltaT)
                    
                    if TouchesAppuyees[pygame.K_TAB]:
                        # Affichage des details d'un Sommet ou Segment trouve sous le pointeur.
                        if LocElem:
                            observation = [ LocElem ]
                        else:
                            observation = self.Selection
                            
                        if observation:
                            self.EditElems(observation, imgFond=False, lectureSeule=True, bloque=False)
                    
                    # Gestion des evenements des peripheriques.
                    if self.TraiteEvenements(deltaT):
                        break # sortie de cette partie
                
                    # Affichage de la toile a l'ecran
                    pygame.display.flip()
            
            except sauvegarde.SauvegardeAutoExc:
                raise
            
            except NouvellePartieException:
                raise
            
            except PasAPasSelecSeuleException:
                DebutDeTour = True

            except:
                traceback.print_exc()
                            
            if self.Courir or self.pasapas_saute_tours > 0:
                    
                if DebutDeTour:
                    
                    if self.pasapas or self.pasapas_saute_tours:
                        self.horloge.tick() # ne pas prendre en compte le temps passe en pause
                    
                    self.TempsT += deltaT * self.SimTimeFactor
    
                    majFuncIter = self.MajTourFuncIter(deltaT * self.SimTimeFactor, self.TempsT)
                    
                    DebutDeTour = False
                    
                    if self.pasapas == PasAPas.Tour and pygame.key.get_mods() & pygame.KMOD_CTRL:
                        self.ProfilePerf()

                while True:
                    
                    try:
                        
                        majFunc = next(majFuncIter)
                        
                    except StopIteration:                    
                        # iteration terminee : fin du tour
                                                
                        DebutDeTour = True
                        
                        if exo_controle:
                            temps_tour = exo_controle #"1./self.ImgParSec
                        else:    
                            min_temps = 0 if (not self.Affiche and self.Const_IPS) else self.ImgParSec
                            temps_tour = self.horloge.tick(min_temps) / 1000. # temps ecoule depuis le tour precedent (en secondes)
                            
                        deltaT = 1./self.ImgParSec
    
                        if self.pasapas_saute_tours > 0:
                            self.pasapas_saute_tours -= 1
                            self.Courir = False
                        
                        if self.pasapas:
                            self.Courir = False
                            JalonTemps_ms = pygame.time.get_ticks()
                            
                        elif not self.Const_IPS:
                            deltaT = max( deltaT, temps_tour ) 

                        tot_temps_tour_2 += temps_tour ** 2
                        tot_temps_tour   += temps_tour
                        temps_tour_moyen  = tot_temps_tour / NombTours
                        
                        tour_var = tot_temps_tour_2 / NombTours - temps_tour_moyen**2
                        if tour_var < 0:
                            tour_var = 0
                        tour_ecart_type = math.sqrt(tour_var)
                        
                        if tour_ecart_type:
                            frac_ecart_type = ( temps_tour - temps_tour_moyen ) / tour_ecart_type
                        else: 
                            frac_ecart_type = 0
                            
                        alerte_temps = frac_ecart_type > 2 and temps_tour / temps_tour_moyen > 1.2

                        if self.pasapas or alerte_temps:
                            if self.pasapas:
                                self.Validation()
                                                                                            
                            Msg = 'Fin Tour %d  [%3d ms]'%( self.NumeroDuTour, temps_tour*1000 )
                            if self.Const_IPS:
                                Msg += ' %dc'%(deltaT*1000)
                            if alerte_temps:
                                Msg += '  %.1f ecart type temps moyen (%d ms) ALERTE'%(frac_ecart_type, temps_tour_moyen*1000)
                                
                            #print Msg
                                                       
                        if self.Rediffusion:
                            self.Rediffusion -= 1

                        self.NumeroDuTour += 1
                        NombTours += 1
                        
                        if self.pasapas and profileur and profileur.isEnabled():
                            self.ProfilePerf()
                        
                        break
                        # fin du bloc except: StopIteration 
                     
                    try:
                        
                        agent = majFunc()
                        
                        if self._validation and self.Validation():
                            break
                                            
                    except SousEnsembleException as exc:

                        self.MaJSelection(exc.coupables) 
                        
                        traceback.print_exc()
                        #self.Courir = False
                        #break
                    
                    except:
                        
                        traceback.print_exc()
                        
                        #self.Courir = False
                        #break
                    
                    if self.pasapas == PasAPas.Unite or (self.pasapas == PasAPas.Selection and agent in self.Selection):
                        
                        if self.pasapas == PasAPas.Unite:
                            self.Selection = set([agent])
                        
                        self.Courir = False
                        JalonTemps_ms = pygame.time.get_ticks()
                        self.Validation()
                        print('            *** Pas a pas %s ***'% agent)
                                                    
                        break
            
            if exo_controle:
                yield temps_tour
            
        if NombTours > 1:
            print()
            print()
            print('temps moyen par tour %d ms'%(1000.*tot_temps_tour/(NombTours-1)))

    def MajTourFuncIter(self,deltaT,tempsT):
        return iter([])
                
    def AddJalonSouris(self, coordsVues, premier=False):
        
        if premier or coordsVues is None:
            self.etatSouris.jalons = []
            
        if coordsVues is not None:
            self.etatSouris.jalons.append(self.CoordsReeles(coordsVues))


    def VidangeSelect(self, Verts=True, Tris=True, Segs=True, Perims=True):
        """ Vide toutes les selections """
        pass
                                    
    def ProfilePerf(self, tick=False):
        

        if profileur.isEnabled():
            print('Profileur Desactive')
            profileur.disable()
            profileur.Show()
            
        else:
            print('Profileur Active')
            profileur.enable()
    
        if tick:
            self.horloge.tick()
   
    
    def SelectionForEdit(self):
        if self.Selection:
            elems = self.Selection
        else:
            elems = [self] 
        
        return elems
    
    def EditElems(self,Elems, sauteTemps=True, bloque=True, **kwargs):
        
        editeurElems = menus.EditeurElem(elems=Elems, fonteH=12, **kwargs) 
        if bloque:
            editeurElems.boucle()
            if sauteTemps:
                self.horloge.tick() # rebase le temps du tour apres la pause liee au menu
        else:
            editeurElems.affiche(self._Surf)

    def Defilement(self, dV):    
        dV *= 300 / self.VueRatio
        if self.VueAngle:
            dV.iarot_v(self.VueAngle)
            
        self.changeVue(org=self.VueOrg+dV)
                                  
    def Grossissement(self, grossit, increment=1.1):
        
        if grossit ^ (increment > 1.):
            increment = 1. / increment
        from pygame import mouse as souris
        
        posSouris = self.Vec(*souris.get_pos())                
        posReele = self.CoordsReeles(posSouris)

        self.changeVue(ratio=self.VueRatio*increment)
        
        # Ajuste la position de la camera de telle sorte que
        # le curseur de la souris occupe la meme position reele.

        # Rapproche le curseur du centre de l'ecran,
        # tout en decallant le repere d'affichage pour que le curseur corresponde
        # toujours a la meme position reele.
        posCentre = self.DimEcran / 2.
        dPosVue = (posSouris - posCentre) / 4
        novPosSouris = [int(i) for i in posSouris - dPosVue]

        souris.set_pos(novPosSouris)
        
        novPosReele = self.CoordsReeles(novPosSouris)
        
        dPos = novPosReele - posReele
        
        self.changeVue(org=self.VueOrg-dPos)
    
    def BoiteJalons(self, Point1, Point2):
        x0, y0 = Point1
        x1, y1 = Point2
        x0, x1 = sorted([x0, x1])
        y0, y1 = sorted([y0, y1])
        return (x0, y0), (x1, y1)
         
    def BasculeEnregistrement(self):
        import pygame
        self.Enregistre = not self.Enregistre
        if self.Enregistre:
            print('Enregistrement depuis le tour', self.NumeroDuTour)
            if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                print('effacement enregistrement precedent')
                self.Enregistrement.clear()
        else:
            print('Arret enregistrement au tour %d - %d evenements' % ( self.NumeroDuTour, len(self.Enregistrement) ))
            
            
    def BasculeRediffusion(self):
        if not self.Rediffusion and not self.Enregistrement:
            print('Rien a rediffuser')
            self.Rediffusion = 0
        
        elif not self.Rediffusion:
            self.Rediffusion = max(max(self.Enregistrement) - self.NumeroDuTour,0)

        if self.Rediffusion:
            self.Enregistre = False
                    
                    
    def Sauvegarde(self, dialogue=False, nom='', copie=False):
        
        FoncDialogue = None
        
        if dialogue or not nom:
            FoncDialogue = SelectePartie
        
        # exfiltrage des evenements survenus avant le tour sauvegarde 
        EvenementsOrig = self.Enregistrement    
        EvenementPostSauve = {}
        for numTour in EvenementsOrig:
            if numTour >= self.NumeroDuTour:
                EvenementPostSauve[numTour] = self.Enregistrement[numTour]
        self.Enregistrement = EvenementPostSauve
        
        sauvegarde.Sauvegarde(self, nom=nom if nom else self.nom, FoncDialogue=FoncDialogue,
                                      suff=sauvegarde.SUFF_PARTIE, sauve_dir=media.SAUVE_REP, copie=copie)

        self.Enregistrement = EvenementsOrig
        
    @property
    def Gommer(self):
        return self._Gommer_prop
    
    @Gommer.setter
    def Gommer(self, val):
        self._Gommer_prop = val
        import pygame
        pygame.mouse.set_visible(not val)

    def Gomme(self):
        from pygame import mouse as souris
        
        Vec = self.Vec
        x, y = self.CoordsReeles(souris.get_pos())
        t = self.TailleGomme / self.VueRatio
        _Contour = Vec(x - t, y - t), Vec(x + t, y - t), Vec(x + t, y + t), Vec(x - t, y + t)
        
        print('Gomme a ', x,y)
    

    def joue_son(self, nomFichier, volume=.5):
        if self.ActiveSons and False:
            media.charge_son(nomFichier, volume=volume).play()


    def TraiteEtat(self, deltaT):
        """
        """
        import pygame     
        Vec = self.Vec
        KeyPressed = pygame.key.get_pressed()
    
        # Mode Gomme
        if self.Gommer and clic.gauche_enfonce():
            self.Gomme()
            
        mods = pygame.key.get_mods()
            
        FlecheHau = KeyPressed[pygame.K_UP]
        FlecheBas = KeyPressed[pygame.K_DOWN]
        FlecheGau = KeyPressed[pygame.K_LEFT]
        FlecheDro = KeyPressed[pygame.K_RIGHT]
        
        # 0 / Defilement
        Grossissement = (FlecheHau and FlecheBas) or (FlecheGau and FlecheDro)
           
        if Grossissement:
            increment = 1 + deltaT * 20 * (.05 if mods & pygame.KMOD_SHIFT else .03)
            self.Grossissement(grossit=FlecheHau, increment=increment)
            
        elif FlecheHau or FlecheBas or FlecheGau or FlecheDro:
            dV = Vec(0, 0)
    
            if FlecheHau:
                dV[1] = 1
            elif FlecheBas:
                dV[1] = -1
            if FlecheGau:
                dV[0] = -1        
            elif FlecheDro:
                dV[0] = 1
    
            if self.VueSubjective:
                # Controle de l'unite selectionnee par le joueur
                if dV:
                    pass#self.Courir = True
                self.SuitSelection.ControleDirection(dV[1], dV[0]*deltaT)
                         
    
            elif dV[0] or dV[1]:
                 
                dV *= deltaT
                from pygame import mouse as souris
        
                if KeyPressed[pygame.K_s]:
                    # boite de selection a la souris
                    curseurCoordsVues = souris.get_pos()
                    if not self.etatSouris.jalons:
                        self.AddJalonSouris(curseurCoordsVues, premier=True)
                        self.VidangeSelect()
                    dV[1] *= -1 # axe y inverse
                    novPos = 20*dV + curseurCoordsVues 
                    pygame.mouse.set_pos(novPos)
                            
                else:
                    # Defilement/Rotation de la carte avec les fleches du clavier
    
                    self.SuitSelection = None                
                    
                    if mods & pygame.KMOD_SHIFT:
                        # Defilement accelere
                        dV *= 3
                    
                    if mods & (pygame.KMOD_ALT | pygame.KMOD_CTRL) and self.ModeInfra:
                        # Deplacement fin de la selection
                        dV *= 50 
                        self.DeplaceSelection(dV)
                        
                    elif mods & pygame.KMOD_CTRL:
                        # Rotation de la carte
                        if self.VueAngle is None:
                            self.VueAngle = Vec(1,0)  
                        
                        w = -dV[0] * math.pi / 4
                        self.VueAngle.irot(w)
                        
                    else:
                        # Defilement de la carte
                        self.Defilement(dV)
                    
                
    def TraiteEvenements(self,deltaT):
        ''' Traitement des commandes du joueur
        '''
        # pygame.key.set_repeat(10, 10)
        #mods = pygame.key.get_mods()
        
        for event in self.get_event():
            
            if self.TraiteEvenementAnte(event,deltaT):
                return True
    
            self.TraiteEvenementMid(event)
        
            self.TraiteEvenementPost(event)
            import pygame
            if event.type == pygame.MOUSEBUTTONUP:
    
                if event.button in (clic.gauche, clic.droit):
                    # Vide la liste des jalons        
                    self.AddJalonSouris(None, premier=True)
    

    def TraiteEvenementAnte(self, event, deltaT):
        '''
        '''
        import pygame
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            self.Boucle = False
            return True
                
        if event.type in (pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP) and \
            event.button in (clic.mol_haut, clic.mol_bas) and \
            not (clic.droite_enfonce() or clic.gauche_enfonce()):
            # Evenement de la molette souris
            
            if event.type == pygame.MOUSEBUTTONDOWN:
                
                mods = pygame.key.get_mods()
                accel = 5
                if mods & pygame.KMOD_SHIFT:
                    accel *= 3
                    
                signe = 1 if event.button == clic.mol_bas else -1
                
                if mods & pygame.KMOD_CTRL:
                    # defilement horizontal
                    self.Defilement(self.Vec(deltaT*accel*signe,0))
                elif mods & pygame.KMOD_ALT:
                    # defilement vertical    
                    self.Defilement(self.Vec(0,deltaT*accel*signe))
                else:
                    # (De-)Grossissement
                    self.Grossissement(grossit=signe,increment=1+.1*accel/5.)
                                        
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # Clic souris (d'un bouton qui n'est pas la molette)
            
            if not (event.button == clic.gauche and clic.droite_enfonce()):
                # si le bouton droit n'est pas enfonce.
                
                self.ClicSourisPrec[event.button - 1] = event.pos
                
                if event.button == clic.milieu:  # Clic milieu
                    Elems = self.Selection if self.Selection else [self]
                    self.EditElems(Elems)
                    
                elif event.button == clic.gauche:
                    
                    mods = pygame.key.get_mods()
                    
                    if mods & pygame.KMOD_SHIFT and mods & pygame.KMOD_CTRL:
                        
                        # Copie/Colle la selection
                        self.SelectionNouvPos(pos=event.pos, Colle=True)
                            
                    else:
                        pass
                    
                if event.button == clic.droit and self.Gommer:
                    
                    self.Gommer = False                  
    
                elif event.button in (clic.gauche, clic.droit):
                                        
                    BoiteSelection = True
                                        
                    if BoiteSelection:
                        # Clic gauche ou droit
                        # Premier jalon d'une selection    
                        self.AddJalonSouris(event.pos, premier=True)
                
                
        elif event.type == pygame.MOUSEMOTION:
            
            mods = pygame.key.get_mods()
            
            if self.Selection and self.ModeInfra and \
                mods & pygame.KMOD_SHIFT and not mods & pygame.KMOD_CTRL and not mods & pygame.KMOD_ALT \
                and not any(event.buttons):
                
                # Deplacement force des elements selectionnees
                dpos = self.VecReel(event.rel)
                if dpos:
                    self.DeplaceSelection(dpos)
                            
            elif event.buttons[0]:  # bouton gauche enfonce
                
                if mods & pygame.KMOD_ALT:
                    # Domaine de selection arbitraire suivant le contour dessine a la souris
                    self.AddJalonSouris(event.pos)
                                    
        elif event.type == pygame.KEYDOWN:
            from pygapp.partieutils import TOUCHES_NUM
            touche = event.key
                
            if touche == pygame.K_l:
                if event.mod & pygame.KMOD_CTRL or not self.AfficheConsole:
                    self.AfficheConsole = not self.AfficheConsole
                elif self.AfficheConsole:
                    self.console.vide()
                
            elif touche == pygame.K_o:
                
                dialogue = event.mod & pygame.KMOD_SHIFT
                
                if event.mod & pygame.KMOD_ALT and False:
                    pass
                else:
                    # Ouverture d'une partie
                    print()
                    print()
                    if dialogue:
                        NovPartie = SelectePartie(defaut=self.nom, choixNouveau=False, valideExistant=False)
                    elif event.mod & pygame.KMOD_CTRL:
                        NovPartie = self
                    else:
                        NovPartie = self.nom 
                                                        
                    ArgsPartie = {}
                    if self.Enregistrement:
                        print('passage de %d tour/evenements'%len(self.Enregistrement))
                        if self.Enregistre and not self.Rediffusion:# ne pas conserver l'evenement d'ouverture
                            del self.Enregistrement[self.NumeroDuTour][-1]
                        ArgsPartie['Enregistrement'] = self.Enregistrement
                                   
                    if pygame.key.get_pressed()[pygame.K_SPACE]:
                        ArgsPartie['Courir'] = not self.Courir
    
                    if not NovPartie:
                        NovPartie = type(self)()
                        
                    raise NouvellePartieException(partie=NovPartie, **ArgsPartie) 
                                              
            elif touche == pygame.K_RETURN:
                # Edition des elements
                if event.mod & pygame.KMOD_CTRL:
                    Elems = [self]
                else:
                    Elems = self.SelectionForEdit()
                self.EditElems(Elems)
                
            elif touche == pygame.K_TAB:
                if event.mod & pygame.KMOD_CTRL:
                    pass
                else:
                    # Affiche des informations sur l'element sous le curseur. 
                    self.SurvolSelection = not self.SurvolSelection
                
            elif touche == pygame.K_g:
                
                if event.mod & pygame.KMOD_CTRL:
                    pass
                elif event.mod & pygame.KMOD_SHIFT:
                    self.Gommer = not self.Gommer
                    
                  
            elif touche == pygame.K_i:
                if event.mod & pygame.KMOD_CTRL:
                    self.AfficheIPS = not self.AfficheIPS
                else:
                    self.ModeInfra = not self.ModeInfra
                        
                    print('ModeInfra', self.ModeInfra)
                
            elif touche == pygame.K_a:
                self.Afficher = not self.Afficher
                    
            elif event.key == pygame.K_r:
                if event.mod & pygame.KMOD_CTRL:
                    if not event.mod & pygame.KMOD_ALT:
                        self.Reinit()
                else:
                    self.Recadre()
            
            elif touche == pygame.K_x:
                if not self.VueSubjective:
                    # Remise d'aplomb du 
                    self.VueAngle = None
                        
            elif touche == pygame.K_s:
                
                if event.mod & pygame.KMOD_CTRL:
                  
                    if event.mod & pygame.KMOD_ALT:
                        # Sauvegarde de graphe                      
                        self.graphe.Sauvegarde(dialogue=event.mod & pygame.KMOD_LSHIFT, nom=self.graphe.nom)
                    else:
                        # Sauvegarde de partie
                        self.Sauvegarde(dialogue=event.mod & pygame.KMOD_LSHIFT, nom=self.nom)
                elif event.mod & pygame.KMOD_LSHIFT:
                    self.Sauvegarde(nom=sauvegarde.DERNIERE_LANCEE)
                    
                elif event.mod & pygame.KMOD_ALT:
                    
                    valeurPourSelection = menus.BoiteTexte(["Selectionne : "]).boucle()
                    if valeurPourSelection:
                        
                        Elems = []
                        if valeurPourSelection.strip():
                            
                            Elems = []
        
                            for valeur in valeurPourSelection.split():
                                                             
                                Elem = self.SelectByIndex(valeur)
                                if Elem is not None:
                                    Elems.append(Elem)
                                
                        if Elems:
                            self.VidangeSelect()
                            self.MaJSelection(Elems)
                            self.Recadre()
                                    
            elif touche == pygame.K_v:
                # Validation
                self.Validation(selectionne=True, verbose=True)
                                        
            elif touche == pygame.K_c:
                if event.mod & pygame.KMOD_CTRL and event.mod & pygame.KMOD_ALT:
                    pass
                    
                else:
                    # Centre la camera sur la selection
                    if self.SuitSelection:
                        self.SuitSelection = None
                        self.VueSubjective = False
                        
                    elif self.Selection:
                        self.SuitSelection = next(iter(self.Selection))
                        if event.mod & pygame.KMOD_CTRL:
                            print('VueSubjective de', self.SuitSelection) 
                            self.VueSubjective = True
                            self.SuitSelection.AssBut()
                        
    
            elif touche == pygame.K_n:
                
                if event.mod & pygame.KMOD_SHIFT:
                    pass    
                elif event.mod & pygame.KMOD_CTRL:
                    pass                            
                else:
                    self.AfficheNumeros = not self.AfficheNumeros
            
            elif touche == pygame.K_PRINT:
                
                self.ImprimeEcran(versPressePapier=not(event.mod & pygame.KMOD_CTRL))
                
            elif touche == pygame.K_DELETE:
    
                for etre in self.Selection:
                    if hasattr(etre,'Eliminer'):
                        etre.Eliminer()         

                self.EffaceSelections()
                
            elif touche in TOUCHES_NUM:
                
                intKey = int(pygame.key.name(touche)[-1])
                
                if event.mod & pygame.KMOD_CTRL:
    
                    if self.Selection:
                        # fait de la selection un groupe
                        # Attribue une numero a ce groupe
                    
                        if intKey in self.SelectionGroupes:
                            # Disperse le groupe existant sous ce numero
                            self.SelectionGroupes[intKey].Disperse()
                            self.SelectionGroupe = None
                        
                        self.SelectionGroupe = self._ClasseGroupe_(nom='Groupe %d' % intKey)
                        self.SelectionGroupes[intKey] = self.SelectionGroupe
                        self.SelectionGroupe.Enrole(self.Selection)
                        print('creation de Formation %s, %d personnels' % (self.SelectionGroupe, len(self.SelectionGroupe.Unites)))    
                        
                else:
                    # Selection d'un groupe par son numero
                    if intKey in self.SelectionGroupes:
                        
                        Groupe = self.SelectionGroupes[intKey]
                        self.SelectionneGroupe(Groupe)
                        
                    else:
                        self.SelectionGroupe = None
                        
    
            elif touche == pygame.K_p:
            
                if event.mod & pygame.KMOD_CTRL:
                    self.ProfilePerf(tick=True)
            else:
                pass
    
    def EffaceSelections(self):
        ''' Efface la selection'''
        self.Selection.clear()

    
    def SelectionNouvPos(self, pos, Colle=False):
        ''' Affiche (ou Copie/Colle) la selection a l'emplacement pos'''
        
        if self.Selection:
            
            pos = self.CoordsReeles(pos)
            
            Distance = self.DistBarySelection(pos)
            
            NovVerts = {}
            
            for Elem in self.Selection:
                
                NovPos = Elem.pos + Distance
                
                if Colle:
                    NovObj = Elem.copie(NovPos)
                    NovVerts[Elem] = NovObj
                    
                else:
                    self.dessineCercle(NovPos, 2, Elem.couleur)
            
            return NovVerts

    def SelectByIndex(self,valeur):
                
        try:
            if valeur.isdigit():
                index = int(valeur)
            else:
                index = int(valeur[1:])
        except:
            return None

        Elem = None

        if valeur.startswith('t') or (self.AfficheTriangles and valeur.isdigit()):
            
            if self.AfficheConnexe:
                graphe = self.graphe.searchDual
            else:
                graphe = self.graphe
                self.AfficheTriangles = True
            
            Elem = graphe.Triangles[index]
                
        elif valeur.startswith('v') or valeur.isdigit():
            Vert = self.graphe.Vertices[index]
            Elem = self.graphe.VertexToWertex[Vert]
            
        elif valeur.startswith('s'):
            Elem = self.graphe.Segments[index]
                                        
        elif valeur.startswith('c'):
            self.AfficheConnexe = True
            
            Elem = self.graphe.searchDual.ConvexAreas[index]
            numConvAreas = len(self.graphe.searchDual.ConvexAreas)
            if index >= numConvAreas:
                print('index invalide ; il y a %d convexAreas'%numConvAreas)
            print(type(Elem))

        return Elem
                
                
    def TraiteEvenementMid(self, event):
        import pygame
        from pygapp.partieutils import TOUCHES_NUM
        
        mods = pygame.key.get_mods()
        
        if event.type == pygame.MOUSEBUTTONDOWN:
        
            if event.button == clic.droit:
                pass
            elif event.button == clic.milieu:
                pass
            elif event.button == clic.gauche:
                if clic.droite_enfonce():
                    pass
                    
        elif (event.type == pygame.MOUSEMOTION and (event.buttons[2] or (len(self.etatSouris.jalons)==0 and event.buttons[0])) ):
            
            # deplacement souris avec la boutton droit enfonce ou le bouton gauche enfonce.
            pass
        
        elif event.type == pygame.MOUSEBUTTONUP:
        
            if event.button == clic.droit:
                pass
            elif event.button == clic.gauche:
                pass

        elif event.type == pygame.KEYUP:

            pass
                    
        elif event.type == pygame.KEYDOWN:
            
            touche = event.key
            
            if touche in (pygame.K_PLUS, pygame.K_KP_PLUS, pygame.K_MINUS, pygame.K_KP_MINUS):
                
                touchePlus = touche in (pygame.K_PLUS, pygame.K_KP_PLUS)
                    
                if event.mod & pygame.KMOD_CTRL:
                    SimTimeIncr = 1.2

                    if self.SimTimeFactor != 1 and event.mod & pygame.KMOD_SHIFT and touchePlus ^ (self.SimTimeFactor > 1):
                        # remise a 1
                        self.SimTimeFactor = 1.
                        
                    else:
                        if touchePlus:
                            self.SimTimeFactor *= SimTimeIncr
                        else:
                            self.SimTimeFactor /= SimTimeIncr
                        
                    if abs(self.SimTimeFactor - 1) < .001:
                        self.SimTimeFactor = 1. 
                     
                elif self.pasapas or not self.Courir:
                    
                    if touchePlus:    
                        self.pasapas_saute_tours = menus.BoiteTexte( defaut=0, legende=['Nombre de Tours a sauter :'], ).boucle()
                                
                    else:    
                        saute_tours = menus.BoiteTexte( defaut=0, legende=['Nombre de Tours a rembobiner :'], ).boucle()
                        if saute_tours is not None:
                            num_tour = self.NumeroDuTour - saute_tours
                            raise NouvellePartieException(partie=self, tour_num=num_tour)                          
                    
            elif touche in TOUCHES_NUM:
                
                if not event.mod & pygame.KMOD_CTRL and not self.SelectionGroupe:
                    # Aggregation et Selection des unites par camp
                    pass                
                                
            elif touche == pygame.K_BACKSPACE:
                
                if event.mod & pygame.KMOD_CTRL and self.SauvegardeAuto:
                    print('Rembobinage a la derniere sauvegarde')
                    raise self.SauvegardeAuto

            elif touche == pygame.K_SPACE:
                
                if event.mod & pygame.KMOD_CTRL:
                    # Mode pas a pas
                    Modes = ModesPasAPas()
                    
                    Decal = 1
                    if mods & pygame.KMOD_SHIFT:
                        Decal = -1
                    
                    # Temporaire:
                    if self.pasapas == 'Non':
                        self.pasapas = ''
                    Index = Modes.index(self.pasapas)
                    
                    self.pasapas = Modes[(Index+Decal)%len(Modes)]
                    
                    #print 'Pas a Pas <-', self.pasapas
                    self.Courir = False
                    
                    if self.pasapas == PasAPas.SelecSeule:
                        raise PasAPasSelecSeuleException

                                            
                else:
                    # Mise en Pause
                                            
                    self.Courir = not self.Courir
            
            elif touche == pygame.K_F11:
                # Mode plein ecran
                if not event.mod & pygame.KMOD_CTRL:
                    self.PleinEcran = not self.PleinEcran

            elif touche in (pygame.K_F9, pygame.K_F5):
                
                self.ModeInfra = False
                Const_IPS = touche == pygame.K_F9
                if not (self.Const_IPS ^ Const_IPS):

                    self.Courir = not self.Courir
                    
                    if self.Courir:
                        self.pasapas = PasAPas.Non
                        
                self.Const_IPS = Const_IPS
                
            elif touche == pygame.K_F10:
                self.ModeInfra = False
                self.Courir = True
                self.pasapas = PasAPas.Tour
                if event.mod & pygame.KMOD_CTRL:
                    self.Sauvegarde(nom=sauvegarde.DERNIERE_LANCEE)

            elif touche == pygame.K_F12:
                self.ModeInfra = False
                self.Courir = True
                self.pasapas = PasAPas.SelecSeule
                raise PasAPasSelecSeuleException
                    
            elif touche == pygame.K_F6:
                self.BasculeRediffusion()
                    
                #if self.Courir:
                #    self.pasapas = PasAPas.Non
            
            elif touche == pygame.K_F7:
                self.BasculeEnregistrement()
            
            elif touche == pygame.K_F2:
                # enregistrement de partie comparee
                from .utils import diff_partie
                
                nomJalon = diff_partie.PartieComparee.JALON_INIT
                etatInit = getattr(self, nomJalon, None)
                if etatInit is None:
                    self.Enregistre = True
                    self.Enregistrement.clear()
                    self.Courir    = True
                    self.Const_IPS = True
                                        
                    import copy                    
                    EtatInit = copy.deepcopy(self)
                    
                    setattr(self, nomJalon, EtatInit)
                else:
                    if not self.Const_IPS:
                        logging.log(logging.ERROR, "Partie comparee en deltaT variable")
                    elif not self.Enregistre:
                        logging.log(logging.ERROR, "Partie comparee sans enregistrement des evenements")
                    else:
                        delattr(self, nomJalon)
                        comparaison = diff_partie.PartieComparee(etatInit, self)
                        #if partieInit.NumeroDuTour >= self.NumeroDuTour:
                        #    logging.log(logging.WARNING, "Partie comparee sans tour a derouler")
                        print('Sauvegarde Partie Comparee %s %d tours'%( self.nom, self.NumeroDuTour - etatInit.NumeroDuTour))
                        sauvegarde.Sauvegarde(comparaison, self.nom, suff=diff_partie.PartieComparee.SUFFIX,
                                              sauve_dir=media.TEST_REP)
    
    def TraiteEvenementPost(self, event):
        ''' Traitement des commandes du joueur
        '''
        pass
    