'''
Created on 16 Aug 2012

@author: Zarastro
'''

import pickle

import traceback
import os
from . import menus
from . import media

SUFF_PARTIE = '.par'
SUFF_RESEAU = '.res'

DERNIERE_LANCEE = '__derniere_lancee__'
NOUVELLE_ENTREE = '<Nouveau>'
SUFFIXE_DOSSIER = '/>'

SERIALIZATION_INFO = None
import logging


def ListeRepertoire(Repertoire, Suffixe=''):
    """ Liste de fichiers dans repertoire Repertoire.
        Les noms de dossiers sont formates avec SUFFIXE_DOSSIER
    """
    liste = []
    # TODO use os.walk
    for fileName in os.listdir(Repertoire):
        
        if os.path.isdir(os.path.join(Repertoire,fileName)) and fileName != '.svn':
            liste.append(fileName + SUFFIXE_DOSSIER)
            
        elif not Suffixe or fileName.endswith(Suffixe):
            liste.append(fileName.split('.')[0])
            
    return liste


def SelectDansRepertoire(Repertoire, Suffixe='', Legende=None, choixNouveau=True, valideExistant=True, Effacable=False):

    BaseRepertoire = Repertoire

    while True:
        
        Choix = ListeRepertoire(Repertoire, Suffixe) 

        RepRel = Repertoire.split(media.SAUVE_REP)[-1]
        if RepRel:
            Legen = '[%s] %s'%(RepRel,Legende)
        else:
            Legen = Legende
            
        Res = SelectObjet(defaut='', Legende=Legen, liste=Choix, choixNouveau=choixNouveau, valideExistant=valideExistant, Effacable=Effacable)
    
        if Res[0]:
            
            if Res[0].endswith(SUFFIXE_DOSSIER):
                # step into the directory
                SubRep = Res[0].rstrip(SUFFIXE_DOSSIER)
                Repertoire = os.path.join(Repertoire, SubRep)
                continue
            
            else:   
                Selection, AEffacer = Res
                
                for NomFichier in AEffacer:
                    Effacer( NomFichier+Suffixe, Repertoire)
                    
                return os.path.join(Repertoire, Selection)
        
        elif Repertoire != BaseRepertoire:
            # go up one level
            if Repertoire.endswith(os.path.sep):
                Repertoire.rstrip(os.path.sep)
            Repertoire = os.path.dirname(Repertoire)
            
        else:
            return None
    

def SelectObjet(defaut='', liste=[], Legende=None, choixNouveau=True, valideExistant=True, Effacable=False):
    
    choix = sorted(liste)
    
    if choixNouveau:
        choix = [NOUVELLE_ENTREE] + choix 
    
    if not choix:
        menus.BoiteMessage( lignes=['Rien a choisir.'], pos=(40,100), alpha_fond=200).boucle()
        return None, []
    
    if Legende is None:
        legende = 'Choisir :'
    else:
        legende = Legende
        
    menuChoix = menus.MenuOptions( choix, legende=[legende], pos=(5,5), centre=False, forceListe=True )
    
    AEffacer = []
    
    while True:
        
        Liste_d_Index = menuChoix.boucle()
            
        if Liste_d_Index is None:
            return None, AEffacer
        
        else:
        
            while True:
                
                if all(Index < 0 for Index in Liste_d_Index):
                    
                    Mondes = [ choix[-(Index+1)] for Index in Liste_d_Index ]
                    
                    if Effacable:
                        if menus.ChampChoix(False, legende=['Effacer %s ?'%(', '.join(Mondes))], pos=(200,100)).boucle():
                            for Monde in Mondes:
                                AEffacer.append( Monde )
                                choix.remove( Monde )
                                
                            return None, AEffacer
                    
                    break
                
                else:
                    
                    Mondes = [ choix[Index] for Index in Liste_d_Index ]
                    
                    if len(Liste_d_Index) != 1 or any(Index < 0 for Index in Liste_d_Index):
                        # Trop de choix
                        break
                    
                    Monde = Mondes[0]
                     
                    if Monde == NOUVELLE_ENTREE:
                    
                        Monde = menus.ChampNomMonde( legende=['Nom du nouveau :'], defaut='', pos=(40,100), alpha_fond=200).boucle()
                        
                        if Monde is None:
                            break

                    if valideExistant and Monde in choix and not Monde.endswith(SUFFIXE_DOSSIER):
                    
                        if menus.ChampChoix(False, legende=['%s existe deja.'%rel_path(Monde, media.SAUVE_REP)," ecraser l'existant ?."], pos=(200,100)).boucle():
                            return Monde, AEffacer
                       
                        elif Monde == NOUVELLE_ENTREE:
                            continue
                       
                        else:
                            break
                            
                    return Monde, []
    return None, []
    
    
def Effacer(nom, sauve_dir):
    fichier = os.path.join(sauve_dir,nom)
    print('effacage de', fichier) 
    os.remove( fichier )
     
def Sauvegarde(Obj, nom='', FoncDialogue=False, suff='', sauve_dir='', 
               copie=False, ablanc=False, throw=False, securite=True, loquace=True):
    """
        copie : renommage de Obj en nom
    """
    nomFichier = nom
    
    if FoncDialogue:

        nomFichier = FoncDialogue(defaut=nomFichier)
        
        if not nomFichier:
            return
        
        nom_rel = rel_path(nomFichier, media.SAUVE_REP)
        
    else:
        
        nom_rel = nomFichier
    
    sauve_dir_nom_rel_suff = os.path.join(sauve_dir, nomFichier+suff)
    filePath = os.path.join(os.getcwd(), sauve_dir_nom_rel_suff)
    
    if securite and not FoncDialogue and not nom.endswith(DERNIERE_LANCEE) and 'efface' not in nom and os.path.exists(filePath):
        
        if not menus.ChampChoix(False, legende=['%s existe deja.'%rel_path(sauve_dir_nom_rel_suff, media.SAUVE_REP)," ecraser l'existant ?."], pos=(200,100)).boucle():
            return

    if not copie:
        Obj.nom = nom_rel
    
    import pygame
    
    if isinstance( Obj, pygame.Surface ):
        
        pygame.image.save(Obj, filePath)
        
    else:
        try:
            
            dataStreamStr = pickle.dumps(Obj, protocol=-1)
    
        except:
            if throw:
                raise
            else:
                traceback.print_exc()
                return False


        if not ablanc:

            fichierObj = open( filePath, 'wb' )
            
            try:
                
                fichierObj.write(dataStreamStr)
        
                # sauvegarde reussie
                if loquace:
                    print('sauvegarde de', nomFichier)
                return True
            
            except:
                if throw:
                    raise
                else:
                    traceback.print_exc()
                    return
                
            finally:
                fichierObj.close()

# Map of class/module names to be replaced on load to allow backward compatibility with saved instances
LOAD_SUBSTITUTION = {
                    #'vieux_module.':'nouv_module.', # notez le point a la fin
                    #'Vieille_Classe' : 'Nouv_Classe'
                    }


class Unpickler(pickle.Unpickler):

    def find_class(self, module, name):
        # print LOAD_SUBSTITUTION
        # import vect2d
        def mapname(name):
            if name in LOAD_SUBSTITUTION:
                novName = LOAD_SUBSTITUTION[name]
                if SERIALIZATION_INFO is not None:
                    SERIALIZATION_INFO.setdefault(name, {}).setdefault('substitue par ', {}).setdefault(novName, {})
                name = novName

            else:
                # replace all old.* submodules by new.*
                for vieuNom, novNom in LOAD_SUBSTITUTION.items():
                    if novNom.endswith('.') and vieuNom in name and name.startswith(vieuNom):
                        novName = name.replace(vieuNom, novNom)
                        if SERIALIZATION_INFO is not None:
                            SERIALIZATION_INFO.setdefault(name, {}).setdefault('substitue par ', {}).setdefault(novName,
                                                                                                                {})
                        name = novName
            return name
            # return name.replace('vieux_module', 'nouveau_module')

        module = mapname(module)
        name = mapname(name)
        return pickle.Unpickler.find_class(self, module, name)



def loads(fileObj):
    """ Deserialisation prennant en compte le changement d'un nom de module.
    """

    unpickler = Unpickler(fileObj)
    return unpickler.load()


def rel_path(filePath, dossier):
    nom_rep = os.path.dirname(filePath)
        
    if not nom_rep or nom_rep == dossier:
        return os.path.basename(filePath)
    elif nom_rep == os.path.curdir:
        return os.path.basename(filePath)
    
    rel_path = os.path.relpath(nom_rep, dossier)
    nom = os.path.basename(filePath)
    nom_rel = os.path.join(rel_path, nom)
    return nom_rel

def TestSauvegarde(Obj):
    
    donnees = pickle.dumps(Obj, protocol=-1)
    return pickle.loads(donnees)
    
def OuvrirPartie(nom, sousDossier='', loquace=True):
    
    return Ouvrir(nom, dossier=os.path.join(media.SAUVE_REP, sousDossier), loquace=loquace)

def Ouvrir(nom, dossier='', suffixe='', loquace=False):

    if dossier:
        if not os.path.isdir(dossier):
            doss = os.path.join(media.SAUVE_REP, dossier)
        else:
            doss = dossier
        filePath = os.path.join(doss, nom)
    else:
        filePath = nom
        
    if suffixe:
        filePath += suffixe 
        
    nom_rel = rel_path(filePath, dossier)
    
    if loquace:
        print('Ouverture de', nom_rel)
    
    
    fileObj = open( filePath, 'rb' )
    
    with SerializationContext():
        Obj = loads(fileObj)
    
    if nom_rel.endswith( SUFF_PARTIE ):
        nom_rel = nom_rel.rsplit(SUFF_PARTIE)[0]
        
    Obj.nom = nom_rel
    
    fileObj.close()
    
    return Obj
    
class SerializationContext(object):
    """ Captures serialization info """
    
    def __enter__(self):
        global SERIALIZATION_INFO
        SERIALIZATION_INFO = {}
        
    def __exit__(self, exception_type, exception_val, trace):
        """ Prints Serialization Info """
        global SERIALIZATION_INFO
        lines = []
        for nomClasse, Info in SERIALIZATION_INFO.items():
            
            lines.append(nomClasse)
            for modif, modif_info in Info.items():
                for argName, argName_info in modif_info.items():
                    msg = '           %-8s'%modif 
                    instances = argName_info.get('instances')
                    if instances:
                        msg += ' x %d %-20s'%(len(instances), argName )                    
                        if 'details' in argName_info:
                            msg += ' ' + ', '.join(info for info in argName_info['details'])
                    else:
                        msg += ' ' + ','.join(list(modif_info.keys()))
                    lines.append(msg)
                    
        SERIALIZATION_INFO = None
        
        if lines: 
            lines.append('')
            logging.log(logging.ERROR, '\n'.join(lines))
        
import collections
def diff(obj1, obj2, loquace=True, memo=None):
    
    racine = memo is None
    if racine:
        memo = set([id(obj1)])
    elif id(obj1) in memo:
        return False
    else:
        memo.add(id(obj1))
        
    if type(obj1) != type(obj2):
        if loquace:
            return ' types different : %s, %s'%(type(obj1),type(obj2))
        return True
    
    if isinstance( obj1, (tuple, list, set) ):
        if len(obj1) != len(obj2):
            if loquace:
                return ' tailles different : %d, %d'%(len(obj1),len(obj2)) 
            return True
        
        for i, (val1, val2) in enumerate(zip(obj1, obj2)):
            if isinstance(obj1, set):
                pass
            hasdiff = diff(val1, val2, loquace=loquace, memo=memo)
            if hasdiff:
                if loquace:
                    return '[%d]'%i + hasdiff
                return True
            
    elif isinstance( obj1, (dict, collections.OrderedDict) ):
        for kk in obj1:
            if kk not in obj2:
                if loquace:
                    return ' clef %s manquante'%(kk)               
                return True
            
            else:
                hasdiff = diff(obj1[kk],obj2[kk], loquace=loquace, memo=memo)
                if hasdiff:
                    if loquace:
                        return '[%s]' + hasdiff
                    return True
            
        for kk in obj2:
            if kk not in obj1:
                if loquace:
                    return ' clef %s manquante'%(kk)
                return True
            
    elif isinstance( obj1, Phenixable ):
        for nomAttr, val1 in obj1.__dict__.items():
            if nomAttr not in obj1.__exc__state__:
                val2 = getattr(obj2, nomAttr)
                hasdiff = diff(val1, val2, loquace=loquace, memo=memo)
                if hasdiff:
                    if loquace:
                        Msg = ''
                        if racine:
                            Msg = str(obj1)
                        Msg += '.%s'%nomAttr + hasdiff
                        return Msg
                    return True
    
    elif obj1 != obj2:
        if loquace:
            return ' %s != %s'%(obj1, obj2)
        return True
    
    return False
        
        
class Phenixable( object ):
    """ Interface de serialisation
        pour prendre en compte automatiquement
        l'ajout, le renommage ou le retrait d'attributs.
    """
    __exc__state__ = [] # liste des attributs a exclure de la serialisation

    __attr_renomme__ = {} # attributs a renommer. { vieux_nom : nouv_nom, ... }

    __attr_reinit__ = [] # attributs a reinitialiser. [ nom, ... ]
    
    
    def __setstate__(self, Dict):
        """ Deserialisation des elements.
        
            comparaison des attributs deserialises avec ceux aue l'on trouve dans une nouvelle instance de cette classe.
            
            Les attributs manquants dans l'ancienne version sont ajoutes avec la valeur par defaut.
            Les attributs manquants dans la nouvelle version sont juges obsoletes et sont elimines.
            
        """
        #print 'setstate',self.__class__.__name__
        self.__dict__ = Dict
     
        classe = type(self)
        nomClasse = classe.__module__ +'.'+ classe.__name__
        
        try:
            
            newObj = classe()
                
        except Exception:
            print('Probleme pour instancier', nomClasse)
            traceback.print_exc()
            return

        attrRenommes = set()
        # Renommage d'attributs
        for argName in self.__attr_renomme__:
            if argName in self.__dict__:
                novArgName = self.__attr_renomme__[argName]
                self.__dict__[novArgName] = self.__dict__.pop(argName)
                attrRenommes.add(novArgName)
                if SERIALIZATION_INFO is not None:
                        SERIALIZATION_INFO.setdefault(nomClasse, {}).setdefault('renomme',{}).setdefault(argName,{}).setdefault('instances',[]).append(self)
                
        # Reinitialization d'attributs
        for argName in self.__attr_reinit__:
            if argName in self.__dict__ and hasattr(newObj, argName):
                novVal = getattr( newObj, argName)
                ancVal = self.__dict__[argName]
                
                if novVal != ancVal:
                    if SERIALIZATION_INFO is not None:
                        Details = SERIALIZATION_INFO.setdefault(nomClasse, {}).setdefault('reinit',{}).setdefault(argName,{})
                        Details.setdefault('instances',[]).append(self)
                        Details.setdefault('details',set()).add('%s -> %s'%(ancVal, novVal))
                        
                self.__dict__[argName] = novVal
        
        keys = list(self.__dict__.keys())
    
        # Ajout de nouveaux attributs trouves dans une nouvelle instanciation de la classe
        novAttrs = []
        for argName in newObj.__dict__:
            if argName not in self.__dict__:
                self.__dict__[argName] = newObj.__dict__[argName]
                novAttrs.append(argName)
                
                if argName not in self.__exc__state__:
                    # Ne pas declarer les variables explicitement exclues de la serialization. 
                    if SERIALIZATION_INFO is not None:
                        SERIALIZATION_INFO.setdefault(nomClasse, {}).setdefault('ajout',{}).setdefault(argName,{}).setdefault('instances',[]).append(self)
                                    
        # Retrait d'attributs (ceux qui ne sont pas trouves dans une nouvelle instanciation de la classe)
        for argName in keys:
            """
            if argName == 'groupes':
                groupes = Dict.pop(argName)
                if groupes:
                    self.__dict__['groupe'] = groupes[0] 
                continue
            """
            if argName == 'Selection':
                vals = self.__dict__[argName]
                for a in list(vals):
                    if isinstance(a, int):
                        vals.remove(a)
                        
            # Conversion des sauvegardees comme instances en leur type.
            if argName in ['__axe_collision__', '_bloque_', '__point_collision__']:
                arg = self.__dict__[argName]
                if not isinstance(arg, list):
                    if arg is None:
                        arg = []
                    else:
                        arg = [arg] 
                        
                    self.__dict__[argName] = arg
                    
            # Elimination des attributs trouves dans une l'ancienne version mais pas dans la nouvelle.
            if argName not in newObj.__dict__ and argName not in attrRenommes:
                if SERIALIZATION_INFO is not None:
                    SERIALIZATION_INFO.setdefault(nomClasse, {}).setdefault('retrait',{}).setdefault(argName,{}).setdefault('instances',[]).append(self)
                self.__dict__.pop(argName)
        
        """
        import socle
        classe = socle.vect2d.Vec
        for argName, val in self.__dict__.iteritems():
            
            if isinstance(val, classe):
                try:
                    if argName == 'point1':
                        print argName, self
                        pass
                    self.__dict__[argName] = socle.tryangle.Vec(*val)
                    if SERIALIZATION_INFO is not None:
                        SERIALIZATION_INFO.setdefault(nomClasse, {}).setdefault('Conversion RealVec',{}).setdefault(argName,{}).setdefault('instances',[]).append(self)
                except:
                    if SERIALIZATION_INFO is not None:
                        SERIALIZATION_INFO.setdefault(nomClasse, {}).setdefault('Echec Conversion RealVec',{}).setdefault(argName,{}).setdefault('instances',[]).append(self)
        """
    def __getstate__( self ):
        
        dico = self.__dict__.copy()
        
        #if 'camp_' in dico:
        #    print type(self), 'camp', str(dico['camp_'])
            #print dico
        
        for attri in self.__exc__state__:
            
            if attri in dico:
                dico.pop(attri)
                
        return dico
            
            
class SauvegardeAutoExc( Exception ):
    """ Exception qui prend une copie d'un objet en le serialisant a sa creation
        pour pouvoir le reinstancier plus tard.
    """
    def __init__( self, Obj ):
        self.__donnees = pickle.dumps(Obj, protocol=-1)

    def Ressuscite( self ):
        return pickle.loads(self.__donnees)


def RepareCamps(self):
    """utilite temporaire pour les parties dont les unites avaient fait une copie a neuf de l'attribut camp_"""
    if hasattr( self, 'graphe' ):
        CampsVus = {}
        for unite in self.graphe.AllVertices():
            Camp = getattr( unite, 'camp_' )
            if Camp:
                CampSimil = CampsVus.get(Camp.nom)
                if CampSimil:
                    if Camp is not CampSimil:
                        unite.camp_ = CampSimil
                        print('maj', unite, CampSimil)
                else:
                    CampsVus[Camp.nom] = Camp
 

def ListeFichiers(dossier, sous_dossiers=True, suff=''):
    """ Liste de fichiers dans un dossier ayant un certain suffixe """
    for fichier in os.listdir(dossier):
                
        if fichier not in ['.svn']:
            
            if sous_dossiers and os.path.isdir( os.path.join(dossier, fichier) ):
                for fichierr in ListeFichiers(os.path.join(dossier, fichier), sous_dossiers=sous_dossiers, suff=suff):
                    yield os.path.join( fichier, fichierr )
            elif not suff or fichier.endswith(suff):
                yield fichier
                    
def Resauve(dossier, suff='', bavard=True, ecrase=False):
    """ Resauve tous les objets serialises trouve dans un certain dossier.
        suff : filtre les fichiers par suffixe.
    """
    #from . import utils.io
    
    print()
    if ecrase:
        print('Resauve parties de', end=' ') 
    else:
        print('Serialise parties de', end=' ')
        
    print(os.path.relpath(dossier, media.RACINE_REP))
    
    Warnings = []
    for fichier in ListeFichiers(dossier, sous_dossiers=True, suff=SUFF_PARTIE):
                
        print(fichier.ljust(35), end=' ')
        buff = utils.io.StringOut(flags=utils.io.STDERR)
        with buff:
        #if True:
            Obj = Ouvrir(nom=fichier, dossier=dossier, loquace=False)
            
            RepareCamps(Obj)
            Sauvegarde(Obj=Obj, nom=fichier, sauve_dir=dossier, ablanc=not ecrase, 
                       securite=False, throw=True, loquace=False)
         
        message = buff.getString()
        if message:
            print()
            print(message)
            Warnings.append(fichier)
        else:
            print()
    
    if Warnings:
        print()
        print()
        print('Voir alertes pour:')
        for fichier in Warnings:
            print(fichier.rjust(30))
             
def main():
    # Resauvegarde toutes les parties (pour rafraichir les attributs manquants/obsoletes)
    Resauve( dossier = media.SAUVE_REP, ecrase=True )
            
#this calls the 'main' function when this script is executed
if __name__ == '__main__': 
    main()
