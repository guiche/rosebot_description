'''
Created on Feb 16, 2013

@author: J913965
'''

import sys

class Console( object ):

   
    def __init__( self, app, numLignes=25, couleur=(80, 80, 80) ):

        #self.fonte      = fonte
        self.couleur    = couleur        
        self.app        = app
        self.log        = ''
        self.lignes     = []
        self.maxlignes  = numLignes
        self.Affiche = True

    def __enter__(self):
        
        self.stdout_ante = sys.stdout
        self.stderr_ante = sys.stderr
        self.stdout_ante.flush()
        self.stderr_ante.flush()
        sys.stdout = self
        sys.stderr = self
        
    def vide(self):
        
        self.log = ''
        self.lignes = [] 
                
    def __exit__(self, exc_type, exc_value, traceback):
        
        sys.stdout = self.stdout_ante
        sys.stderr = self.stdout_ante
       
    def write( self, arg ):

        self.log += arg
        self.lignes = self.log.splitlines(True)[-self.maxlignes:]
        self.log = ''.join(self.lignes)
        self.lignes = [ ligne.strip() for ligne in self.lignes ]            
        self.stdout_ante.write(arg)
                
    def affiche_texte( self ):
        
        if self.Affiche:
            
            hauteur_ligne = 14
            lignes = self.lignes
            pos = [hauteur_ligne,-(len(lignes)+1) * hauteur_ligne]
            
            for ligne in lignes:
                
                self.app.afficheParatexte( ligne, pos, couleur=self.couleur )
                pos[1] += hauteur_ligne 
            