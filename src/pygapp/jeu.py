
from . import sauvegarde
from .partieutils import NouvellePartieException, Ouvrir, ListeDesParties
import logging
import traceback
import random
from . import app

CLASSE_PARTIE = app.Application

def pygameInit():
    import pygame
    if not pygame.display.get_init():
        try:
                        
            #Pygame init
            pygame.init()
            pygame.mixer.init()
            pygame.mouse.set_visible(1)
            
        except:
            traceback.print_exc()
            input('Appuyez sur Entree pour fermer')
    
        # format loggin
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%H:%M:%S')

    if not pygame.font:  print('Warning, fonts disabled')
    if not pygame.mixer: print('Warning, sound disabled')


class ContexteJeu(object):
            
    def __enter__(self):
        
        random.seed(0)
                
    def __exit__(self, _type, value, traceback):
        """ De-init"""
        import pygame
        if pygame.display.get_init():
            pygame.display.quit()
            
                
def BoucleJeu(nom_partie=''):
    
    with ContexteJeu():
        
        InstanceJeu = None
        
        DERNIERE_LANCEE = sauvegarde.DERNIERE_LANCEE
        a_lancer = nom_partie if nom_partie else DERNIERE_LANCEE
        
        if a_lancer in ListeDesParties():
            try:
                InstanceJeu = Ouvrir(nom=a_lancer)
            except:
                traceback.print_exc()
        
        while True:
            
            try:
                
                if InstanceJeu is None:
                    InstanceJeu = CLASSE_PARTIE()
                
                while True:
                    
                    try:
                        
                        InstanceJeu.GrandBoucle()
                        break
        
                    except sauvegarde.SauvegardeAutoExc as exc:
                        InstanceJeu = exc.Ressuscite()
                        
                    except NouvellePartieException as exc:
                        DimEcran = InstanceJeu.DimEcran
                        
                        try:
                            InstanceJeu = exc.Ouvrir()
                        except:
                            traceback.print_exc()
                            continue
                        
                        if InstanceJeu.nom != DERNIERE_LANCEE:
                            InstanceJeu.Sauvegarde( nom = DERNIERE_LANCEE, copie=True )
                        # previent le retaillage de l'ecran si les parametres
                        # de la partie sauvee sont differents
                        InstanceJeu.DimEcran = DimEcran
                            
                break
            
            except:
                traceback.print_exc()
                if input('Appuyez sur Entree pour continuer'):
                    InstanceJeu = None
                else:
                    # echap termine la boucle
                    break
                
    print('jeu termine')
    
#this calls the 'main' function when this script is executed
if __name__ == '__main__':
    from . import media
    media.setupResourcePath(__file__) 
    BoucleJeu()

