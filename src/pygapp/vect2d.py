from math import sqrt, cos, sin
 
 
Rotateurs = {}
 
def Rot( rad, Arron=3 ): 
 
    arronRad = round(rad,Arron)
    Rotateur = Rotateurs.get(arronRad)
 
    if not Rotateur:
        Rotateur = cos(rad), sin(rad)
        Rotateurs[arronRad] = Rotateur
 
    return Rotateur

def vec_copier(self, other):
    self[0] = other[0]
    self[1] = other[1]
    return self
    
def vec__copy__(self):
    return Vec(self[0],self[1])

def vec__add__(self, other): return type(self)(self[0] + other[0], self[1] + other[1])

vec__radd__ = vec__add__

def vec__sub__(self, other): return  type(self)(self[0] - other[0], self[1] - other[1])

def vec__rsub__(self, other): return type(self)(other[0] - self[0], other[1] - self[1])

def vec__mul__(self, a):
    ''' produit scalaire ou par un scalaire '''
    if isinstance( a, (float,int) ):
        return Vec(self[0] * a, self[1] * a)
    else:
        return self[0]*a[0] + self[1]*a[1]

def vec_iadd_mul(self, vec, scal):
    self[0] += vec[0] * scal
    self[1] += vec[1] * scal
    return self

def vec__rmul__(self, a):  return Vec(self[0] * a, self[1] * a)

def vec__div__(self, a):
    ''' division par un scalaire '''    
    return Vec(self[0] / a, self[1] / a)

def vec__rdiv__(self, a):  return Vec(self[0] / a, self[1] / a)

def vec__iadd__(self, other):
    self[0] += other[0]
    self[1] += other[1]
    return self

def vec__isub__(self, other):
    self[0] -= other[0]
    self[1] -= other[1]
    return self

def vec__imul__(self, a):
    self[0] *= a
    self[1] *= a
    return self

def vec__idiv__(self, a):
    self[0] /= a
    self[1] /= a
    return self

def vec__lt__(self, a):
    return (self[0]**2 + self[1]**2) < a**2

def vec__gt__(self, a):
    return (self[0]**2 + self[1]**2) > a**2

def vec__le__(self, a):
    return (self[0]**2 + self[1]**2) <= a**2

def vec__ge__(self, a):
    return (self[0]**2 + self[1]**2) >= a**2

def vec__abs__(self):
    return Vec(abs(self[0]), abs(self[1]))

def vec__neg__(self):
    ''' vecteur oppose '''
    return Vec(-self[0], -self[1])

def vec__nonzero__(self):
    return any(self) #len(self) != 0 and (self[0] or self[1])

def vec__xor__(self, other):
    ''' produit vectoriel '''
    return self[0]*other[1] - self[1]*other[0]

def vec__call__(self, x,y):
    self[0] = x
    self[1] = y
    return self
    
def vec_dist2(self, other):
    return (self[0]-other[0])**2 + (self[1]-other[1])**2

def vec_dist(self, other):
    return sqrt((self[0]-other[0])**2 + (self[1]-other[1])**2)

def vec_tan(self, v):
    """ tangente de l'angle ( self,v ) """
    return ( self ^ v ) / ( self * v )

def vec_tan_abs(self, v):
    """ tangente en valeur absolue de l'angle ( self,v ) """
    return abs( (self ^ v) / (self * v) )

def vec_tan_abs_min(self, v, tan):
    """ (self,v) est dans un cone d'angle de tangente +- tan """
    return abs(self ^ v) < tan * (self * v)

def vec_angle_inf(self, v, v1, v2):
    """ (self,v) est un angle plus petit que (v1,v2)"""
    return abs(self ^ v) * (v1 * v2) <= abs(v1 ^ v2) * (self * v)

def vec_angle(self, vec2, angle):
    prod = self.unite() ^ vec2.unite()
    if prod <= sin(angle):
        return prod
    else:
        return False

def vec_rot(self, arg):
    ''' Rotation
        arg : radian ou matrice de rotation
         '''
    return Vec(self[0], self[1]).irot(arg)

def vec_irot(self, arg):
    ''' Rotation
        arg : radian
         '''
    import math
    cosa = math.cos(arg)
    sina = math.sin(arg)
    #cosa, sina = Rot( arg )
    x,y = self[0], self[1]
    self[0] =  x*cosa - y*sina
    self[1] =  y*cosa + x*sina
    return self

def vec_rot_v( self, v ):
    ''' Rotation directe de self selon l'angle donne par le vecteur unitaire v = [ cosa, sina ]'''
    return Vec(self[0], self[1]).irot_v(v)
    
def vec_irot_v( self, v ):
    ''' Rotation directe de self selon l'angle donne par le vecteur unitaire v = [ cosa, sina ]'''
    self[0], self[1] = self[0]*v[0] - self[1]*v[1], self[1]*v[0] + self[0]*v[1]
    return self

def vec_rot_vc( self, v, c ):
    ''' Rotation directe de self selon l'angle donne par le vecteur unitaire v = [ cosa, sina ], autour du point c'''
    return Vec(self[0], self[1]).irot_vc(v, c)
        
def vec_irot_vc( self, v, c ):
    ''' Rotation directe de self selon l'angle donne par le vecteur unitaire v = [ cosa, sina ], autour du point c'''
    self[0], self[1] = (self[0]-c[0])*v[0] - (self[1]-c[1])*v[1] + c[0], (self[1]-c[1])*v[0] + (self[0]-c[0])*v[1]+c[1]
    return self

def vec_arot_v( self, v ):
    ''' Rotation indirecte de self selon l'angle donne par le vecteur unitaire v = [ cosa, sina ] '''
    return Vec(self[0], self[1]).iarot_v(v)
    
def vec_iarot_v( self, v ):
    ''' Rotation indirecte de self selon l'angle donne par le vecteur unitaire v = [ cosa, sina ] '''
    self[0], self[1] =  self[0]*v[0] + self[1]*v[1], self[1]*v[0] - self[0]*v[1]
    return self

def vec_arot_vc( self, v, c ):
    ''' Rotation indirecte de self selon l'angle donne par le vecteur unitaire v = [ cosa, sina ], autour du point c '''
    return Vec(self[0], self[1]).iarot_vc(v, c)
    
def vec_iarot_vc( self, v, c ):
    ''' Rotation indirecte de self selon l'angle donne par le vecteur unitaire v = [ cosa, sina ], autour du point c '''
    self[0], self[1] =  (self[0]-c[0])*v[0] + (self[1]-c[1])*v[1] + c[0], (self[1]-c[1])*v[0] - (self[0]-c[0])*v[1] + c[1]
    return self


def vectorise( cls, faible=False ):

    VecMethods = [    
    
                      vec__add__,
                      vec__radd__,
                      vec__iadd__,
                      
                      vec__sub__,
                      vec__rsub__,
                      vec__isub__,
                      
                      vec_dist,
                      vec_dist2,
                      ]
                      
    if not faible:
        VecMethods += [
                       
                      vec__neg__,
                      vec__nonzero__,
                      
                      vec__abs__,
                      vec__lt__,
                      vec__gt__,
                      vec__le__,
                      vec__ge__,
                      
                      vec__mul__,
                      vec__rmul__,
                      vec__imul__,
                      
                      vec__div__,
                      vec__rdiv__,
                      vec__idiv__,
                      
                      vec_copier,
                      vec__call__,
                      vec__copy__,
                      vec_iadd_mul,
                      
                      
                      vec__xor__,
                      
                      vec_tan,
                      vec_tan_abs,
                      vec_tan_abs_min,
                      
                      vec_angle,
                      vec_angle_inf,
                      
                      vec_rot,
                      vec_irot,
                      vec_rot_v,
                      vec_irot_v,
                      vec_arot_v,
                      vec_iarot_v,
                      
                      vec_rot_vc,
                      vec_irot_vc,
                      vec_arot_vc,
                      vec_iarot_vc ]
    
    for method in VecMethods:
        nom_func = method.__name__[3:]
        
        if not nom_func.endswith('_'):
            nom_func = nom_func[1:]
            
        if True:# not hasattr( cls, nom_func ):
            setattr(cls, nom_func, method )
             
    
class Vec(list):
    ''' Vecteur en deux dimensions '''

    #__slots__ = ["x", "y"]

    def __init__(self, x=0,y=0):
        list.__init__(self,(x,y))
        #self.x = x
        #self.y = y
        
    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]
       
    """
    def __setitem__(self,index,item):
        if index == 0:
            self.x = item
        else:
            self.y = item
    
    def __getitem__(self,index):
        if index == 0:
            return self.x
        else:
            return self.y
            
    def __len__(self):
        return 2
       
    def extend(self, li):
        self.x = li[0]
        self.y = li[1]
    
    """
    
    @property
    def nor(self):
        ''' norme '''
        return sqrt(self[0]**2 + self[1]**2)
 
    @nor.setter
    def nor(self, value):
        ''' norme '''
        fact = value / self.nor
        self[0] *= fact
        self[1] *= fact
        
    @property
    def nor2(self):
        ''' Carre de la norme '''
        return self[0]**2 + self[1]**2

    @property
    def uni( self ):
        ''' Vecteur unite '''
        inv_nor = 1./self.nor
        return Vec(self[0]*inv_nor, self[1]*inv_nor)

    @property
    def iuni( self ):
        ''' Vecteur unite '''
        inv_nor = 1./self.nor
        self[0] *= inv_nor
        self[1] *= inv_nor
        return self

    @property
    def uni_nml( self ):
        ''' Vecteur unite '''
        inv_nor = 1./self.nor
        return Vec(-self[1]*inv_nor, self[0]*inv_nor)

    @property
    def iuni_nml( self ):
        ''' Vecteur unite '''
        inv_nor = 1./self.nor
        self[0], self[1] = -self[1]*inv_nor, self[0]*inv_nor
        return self
    
    def copier_uni(self, v):
        inv_nor = 1./v.nor
        self[0], self[1] = v[0]*inv_nor, v[1]*inv_nor
        return self
        
    def unite(self, Long=1.):
        ''' Vecteur unite ou de longueur specifie '''
        fact = Long/self.nor
        return Vec(self[0]*fact, self[1]*fact)
  
    @property
    def nml(self):
        ''' normale suivant une rotation directe '''
        return Vec(-self[1], self[0]) 

    @property
    def inml(self):
        ''' normale suivant une rotation directe '''
        self[0], self[1] = -self[1], self[0]
        return self

    @property
    def anml(self):
        ''' normale suivant une rotation indirecte '''
        return Vec(self[1], -self[0]) 

    @property
    def ianml(self):
        ''' normale suivant une rotation indirecte '''
        self[0], self[1] = self[1], -self[0]
        return self

    
    def __repr__(self):
        """ representation precise """
        return '%f,%f' % tuple(self)
    
    def __str__(self):
        """ representation concise """
        if self.nor2 > 1:
            numChiffres = 1
        else:
            numChiffres = 2
        return '%s,%s' % tuple( str(round(x,numChiffres) if isinstance(x,float) else x) for x in self )
       
    def __pow__(self, other):
        return  self[0] ** other, self[1] ** other
 
    def ent(self):
        ''' coordonnees entieres arondies '''
        return Vec(int(self[0]), int(self[1]))

    def ient(self):
        ''' ent en place '''
        self[0] = int(self[0])
        self[1] = int(self[1])
        return self

    def maxabs(self,val):
        self[0] = max(-val, min( self[0], val ) )
        self[1] = max(-val, min( self[1], val ) )
 
 
    def inside(self, c1, c2):
        if (self[0] >= c1[0] and self[0] <= c2[0]) or \
           (self[0] >= c2[0] and self[0] <= c1[0]):
            if (self[1] >= c1[1] and self[1] <= c2[1]) or \
               (self[1] >= c2[1] and self[1] <= c1[1]):
                return True
        return False
 
 
vectorise( Vec )


 
def pp( arg, noReturn=True, indent=0 ):
    ''' pretty printing '''
 
    Indentation = ' '*indent
    '''
    if isinstance( arg, datetime.date ):
        out =  Indentation + arg.strftime('%a %d %b %y')
    '''
    if isinstance( arg, ( list, set, tuple ) ):
 
        if isinstance( arg, set ):
            arg2 = sorted(arg)
 
        if len(arg) > 3:
            out = ''
            for a in arg2:
                out += Indentation + '%s\n'%pp(a, noReturn=False)
        else:
            subList = [pp(a, noReturn=False) for a in arg2]
            if subList and '\n' in subList[0]:
                joinStr = '\n'
            else:
                joinStr = ', '
 
            out = Indentation + joinStr.join( subList )
 
    elif isinstance( arg, dict ):
        out = ''
        maxKeyLength = max([ len(str(k)) for k in list(arg.keys())+[''] ])+1
        for Key in sorted(arg.keys()):
 
            KeyLine  = Indentation + '%%%ds:' % maxKeyLength % str(Key)
            out     += KeyLine
            Lines    = pp( arg[Key], noReturn = False, indent = indent + maxKeyLength + 2 )
            if len( Lines.split('\n') ) > 1:
                out += '\n' + Lines
            else:
                out += ' ' + Lines.strip()
            out += '\n'
    else:
        if noReturn:
            import pprint
            pprint.pprint(arg, indent=indent)
            return
        else:
            return Indentation + str(arg)
 
    if noReturn:
        print(out)
    else:
        return out
    
