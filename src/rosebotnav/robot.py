'''
Created on 25 Sep 2009

@author: Guiche
'''
from tricarto import tryangle
from tricarto.tryangle import PathFindAlgo
from tricarto.tryangle import Vec
from tricarto.tryangle.reseau import margePivot
from pygapp.partieutils import PasAPas

import math
from . import mobile
from .mobile import EtreMobile, EtatCollision
from .buts import Itineraire

v_rot_pi12  = math.cos(math.pi/4), math.sin(math.pi/4)
v_rot_pi12_ = math.cos(math.pi/4), -math.sin(math.pi/4)

angleDroit  = Vec(0, 1)
angleGauche = Vec(0, -1)

angleEtroit = piSur12 = math.pi / 12


class Robot(mobile.EtreMobile, mobile.OrientableInterface):
    
    #__attr_reinit__ = 'buts_',

    def __init__(self,  camp    = None,
                        taille  = 190, #demi largeur - largeur physique : 35cm
                        profondeur_av = None, # demi profondeur axe des roues - avant, hors bras
                        profondeur_ar = None, # demi profondeur axe des roues - arriere
                        Orient0 = (0,1),
                        **KWArgs):

        self.camp_ = camp
        mobile.EtreMobile.__init__(self, taille=taille, **KWArgs)

        self.profondeur_av = profondeur_av
        self.profondeur_ar = profondeur_ar

        self.reinit()        
         
        self._orient_    = Vec(*Orient0) # orientation physique du robot
        self.itineraire  = Itineraire()
        self.but_        = None
        
        self.marche_arriere = False

        self.marge_contour = 1.3

        
    def reinit(self,
                voie    = 252., # mm; distance entre les centres des deux roues
                acc_v   = 600.,
                acc_cen = 500,
                v_max   = 800.):

        self._acc_v_     = acc_v # mm/s acceleration ligne droite
        self._acc_f_     = .75*self._acc_v_ # mm/s decelleration de freinage

        self._acc_w_    = self._acc_v_ / 2 / voie # rad/s/s
        self._acc_wf_   = self._acc_w_ #self._acc_f_ / self.voie * 2. # rad/s/s
        self._acc_cen_  = acc_cen # m/s/s acceleration tangentielle max avant decrochage
                
        self.v_max = v_max # mm/s
        self.w_max = v_max / 2 / voie # rad/s

    @property
    def v_car(self):
        """ vitesse telle que l'on peut freiner en moins d'un rayon """
        return math.sqrt(2*self._acc_f_*self.rayon)
    
    @property
    def d_car(self):
        """ distance de freinage jusqu'a l'arret complet """
        return max(self.srayon, self._v_.nor2/self._acc_f_)
    
    @property
    def orient_(self):
        """orientation dans le sens de la marche"""
        if self.marche_arriere:
            return -self._orient_
        else:
            return self._orient_
        
    @orient_.setter
    def orient_(self, val):
        if self.marche_arriere:
            self._orient_ = -val
        else:
            self._orient_ = val

    def frontRadius(self, depla=None):
        if depla is None:
            depla = self.orient_
        return self.profondeur_av if depla * self._orient_ > 0 else self.profondeur_ar

    def sideRadius(self):
        return self.rayon

    @property
    def couleur(self):
        if self.camp_:
            return self.camp_.couleur
        else:
            return (240,75,138)

    def vitPivot(self, rpivot, versPivot):
        """ Objectif de vitesse pour aborder une phase de pivot """
        
        # acc centripete = w*w*r = v*v/r
        return math.sqrt(min(abs(rpivot), abs(versPivot.nor))*self._acc_cen_)

    def PreTour(self, deltaT):
        
        mobile.EtreMobile.PreTour(self, deltaT)
        if not self.racine.VueSubjective:
            self.PreBouge(deltaT)
    
    def incremeIti(self):
        
        self.itineraire.increme()

    def PreBouge(self, deltaT):
        
        but_pos, rpivot, but_suiv, rpivot_suiv = self.itineraire.maj_pos(self)
        self.but_ = but_pos
        but_orient_ = self.itineraire.orient
        
        vdirecte = self.orient_ * self._v_
        self.but_aff = None
        
        if but_pos is None:
            dv_max = deltaT * self._acc_f_
            # En l'absence d'objectif, orientation dans la direction du deplacement
            # pas d'objectif :  s'arreter au plus vite.               

            if vdirecte > 0:
                self._eff_ = -min(dv_max, vdirecte) * self.orient_
                                
            elif vdirecte < 0:
                self._eff_ = min(dv_max, -vdirecte) * self.orient_
                        
            if but_orient_:
                if self.marche_arriere:
                    self.marche_arriere = False
                    #self.w *= -1
                    
                if self.Sorienter(deltaT, but_orient_, angle_tol=self.itineraire.tol_o):
                    self.itineraire.orient = None 

            else:

                self.itineraire.finaliser(self)

                if self.w:
                    #freinage angulaire
                    self.w += self.getdW(deltaT, obj=0)

        else:
              
            margePivot = self.marge_contour
            
            if not rpivot:

                if but_orient_:
                    decal_but = (self._orient_ ^ but_orient_.uni) * but_orient_.uni * min(self.rayon, (but_pos-self.pos).nor)
                    self.but_ = self.but_ - decal_but

                self.print_log('tirer droit simple')
                if self.tirerDroit(deltaT, but=self.but_, but_tol=self.itineraire.tol):
                    self.incremeIti()

            else:

                # etape de pivot
                
                versPivot = but_pos - self.pos 
                versBut = but_suiv - self.pos 
                
                ## Test de passage du pivot
                if rpivot * rpivot_suiv >= 0:
                    # deux pivots de meme sens ou bien un pivot et un but final (ie. sans pivot, rpivot_suiv = 0)
                    if rpivot_suiv:
                        vecLat = versBut - versPivot                    
                    else:
                        vecLat = versBut
    
                    distLaterale = (-rpivot*vecLat).uni_nml * versPivot
                    incremeCond = (vecLat * versPivot) <= 0 and (vecLat ^ versPivot )*rpivot <= 0
                    
                else:
                    # deux pivots de sens differents : chicane
                    distLaterale = -(versBut-versPivot).uni * versPivot
                    incremeCond = versBut * versPivot < 0
                    
                if incremeCond or distLaterale > abs(rpivot) / self.marge_contour:
                    
                    if rpivot*rpivot_suiv <= 0 \
                            or ((self.orient_ ^ versBut) < 0) ^ (rpivot < 0):
                        #rpivot * rpivot_suiv : chicane
                        self.incremeIti()
                        self.print_log('rotation increme', incremeCond)


                distPivot = versPivot.nor

                if distPivot > abs(rpivot) * margePivot:
                    self.print_log('pivoter encore loin')
                    
                    # reduction de la vitesse d'entree en virage
                    # car il n'y a aucun w a priori
                    vPrePivot = .75 * self.vitPivot(rpivot, versPivot)
                    #butInt = but_pos + (self.orient_*rpivot).inml
                    #butInt = but_pos + (versBut.uni_nml*rpivot)
                    butInt = self.butPrePivot(but_pos, rpivot, versPivot)
                    self.tirerDroit(deltaT, butInt, obj_v=vPrePivot)
                    
                elif distPivot < abs(rpivot) / margePivot:
                    # trop pres
                    
                    # s'orienter normalement au pivot et prendre du champ
                    
                    bonne_orient = self.Sorienter(deltaT, (rpivot*versPivot).nml)
                    if bonne_orient or self.orient_ * versPivot < 0:
                        # en avant
                        self.print_log('pivoter trop pres en avant')
                        self.majV(deltaT)
                    else:
                        # freinage en attendant la bonne orientation
                        self.print_log('pivoter trop pres freinage')
                        self.majV(deltaT, objV=0)
                        
                else:
                    # bonne distance au pivot 
                    
                    conePivot = 0.2

                    if versPivot.tan_abs_min((-rpivot*self.orient_).inml, conePivot) and vdirecte >= 0:
                        # bonne orientation pour pivoter
                        versSuiv = but_suiv - self.pos
                        if rpivot_suiv:
                            versSuiv = versSuiv + ((versSuiv-versPivot).uni*rpivot_suiv).inml
                            
                        if (versPivot ^ versSuiv)*rpivot > 0 and (self.orient_ ^ versSuiv)*rpivot > 0 and versSuiv > self.rayon:
                            #versSuiv > self.rayon : pour le cas de deux pivots tres proches
                            self.print_log('pivoter ralignement suiv')
                            bonne_orient = self.Sorienter(deltaT, versSuiv)
                            if bonne_orient:
                                objV=None
                            else:
                                objV = 0
                            self.majV(deltaT, objV=objV)
                            
                        else:
                            self.print_log('pivoter')
                            self.pivoter(deltaT, versBut=versSuiv, rpivot=rpivot, versPivot=versPivot)
                        
                    elif self.orient_ * versPivot > 0:
                        self.print_log('pivoter tirer droit prepivot')
                        vPrePivot = self.vitPivot(rpivot, versPivot)
                        obj_temp = self.butPrePivot(but_pos, rpivot, versPivot) 
                        self.tirerDroit(deltaT, obj_temp, obj_v=vPrePivot, coneDroit=1)
                        
                    else:
                        self.print_log('pivoter reglage orient')
                        # freinage et reglage de l'orientation avant de pivoter
                        bonne_orient = self.Sorienter(deltaT, (rpivot*versPivot).nml)
                        self.majV(deltaT, objV=0)

        self._v_ += self._eff_

    def butPrePivot(self, but_pos, rpivot, versBut):
        return but_pos + ((versBut*rpivot).uni_nml - versBut.uni).uni * abs(rpivot)

    def pivoter(self, deltaT, versBut, rpivot, versPivot):
        
        vnor = abs(self._v_ * self.orient_)

        vitPivot = self.vitPivot(rpivot, versPivot) 
                    
        obj_v = vitPivot

        v_echap = self._v_ * versPivot.uni 
        
        if v_echap < 0:
            obj_v *= 1./(1 - .05*v_echap) # ralentit pour ne pas depasser le pivot 

        if vnor > obj_v:# and versBut * self.orient_ < 0:
            # decellere
            vnor = max(vnor - self._acc_f_*deltaT, obj_v)
            if vnor == 0:
                self._eff_ = - self._v_
                self.Sorienter(deltaT, versBut)
                return
            
        elif vnor < obj_v:
            # accelere
            vnor = min(vnor + self._acc_v_*deltaT, obj_v)
        
        if vnor == 0:
            self.Sorienter(deltaT, versBut)
            return

        #ww = min(self._v_rad, self._acc_cen_ / vnor)
        ww = vitPivot / ((versPivot.nor + abs(rpivot))/2)
        
        angleRestant = self.diffAngle(versBut)
        
        ww = min(ww, self.maxVel(abs(angleRestant), 0, self._acc_wf_))
                
        if rpivot > 0 or self.orient_ ^ versPivot < 0:
            ww *= -1 # sens de rotation

        self.Sorienter(deltaT, obj_w=ww)

        self._eff_ = self.orient_ * vnor - self._v_

    def girantVers(self, versBut):
        return self.w * (self.orient_ ^ versBut) >= 0
    
    def tirerDroit(self, deltaT, but, but_tol=None, obj_v=None, obj_w=0, coneDroit=.51, rpivot=None):
        """
            @param but: point vers lequel se diriger
            @param coneDroit: angle (radian) entre l'orientation et l'objectif a partir duquel on commence a avancer

            renvoie True dans le cas ou le but est atteint (dans la limite de but_tol)
        """
        versBut = but - self.pos
        
        dv_frein_max = deltaT * self._acc_f_ # vitesse annulable en un seul tour en freinage max
        self.but_aff = but
        but_decellerer = obj_v is not None

        dist = versBut.nor
        angle_tol = -dist/(1+dist) * .09 + .1

        bonne_orient = self.Sorienter(deltaT, versBut, angle_tol=angle_tol, obj_w=obj_w)

        if but_tol is not None and versBut <= but_tol and (not but_decellerer or abs(self._v_ - obj_v) < dv_frein_max):
            # on est proche du but_ 
            self.print_log('proche du but', versBut, '_v_', self._v_, 'dv max', dv_frein_max)
            if but_decellerer:
                # annulons la vitesse
                v_rel = self._v_ - obj_v
                self._eff_ -= v_rel

            self.print_log('increme tirer droit')
            return True

        else:
            orientation_seule = True
            if bonne_orient:
                orientation_seule = False

            elif versBut.tan_abs_min(self.orient_, coneDroit):

                if self.girantVers(versBut) and (rpivot is None or versBut > self.rayon):

                    orientation_seule = False

            else:
                if self._v_ * versBut > 0:
                    self._eff_ *= 0
                    return
                else:
                    obj_v = 0

            if rpivot is None and versBut < 2*self.rayon:
                obj_v = 0

            self.ValideObstacles(but)

            if orientation_seule:
                self.majV(deltaT, objV=obj_v)
            else:
                self.majV(deltaT, versBut, objV=obj_v)

    def majV(self, deltaT, versBut=None, objV=None):

        acc_f_usu = self._acc_f_
        dv_frein_usu = deltaT * acc_f_usu    # reduction vitesse en freinage usuel (non max)
        
        versButUni = self.orient_
        vdirecte = versButUni * self._v_

        but_tol = self.rayon / 10
        # Distance de freinage (d tel que: v - a*t = 0 et v-1/2*a^2*t = d)
        # d = 1/2 * v^2 / a
        
        decel = objV is not None and vdirecte > objV
        
        if versBut is not None:
            doit_freiner = False
            if decel:
                d_freinage  = self.distFrein(vdirecte, objV, acc_f_usu)
                decelProche = versBut <= d_freinage + but_tol
                
                if decelProche:
                    dv_frein_max = deltaT * self._acc_f_ # vitesse annulable en un seul tour en freinage max
        
                    # nombre de tours a freinage constant pour annuler la vitesse (suppose deltaT constant)
                    n_frein = math.ceil(2*versButUni*versBut/(vdirecte-objV)/deltaT+1)
                    if n_frein > 0:
                        dv_frein_usu = min(dv_frein_max, vdirecte / n_frein)
                        # distance de freinage en supposant un freinage constant pendant n-1 tours:
                        # dd_freinage = Sigma( v - dv * i )[i=1 a n_frein-1] * delta T
                        # dd_freinage = (n_frein-1) * ( abs(vdirecte) - dv_frein_usu*n_frein/2. ) * deltaT
                        doit_freiner = True
        else:
            doit_freiner = decel
            d_freinage = None

                                                                                                              
        if doit_freiner:
            # Phase de freinage si l'on est proche du but ou que l'on va dans le mauvais sens.
            # Calcul du temps d'arret en supposant un freinage maximum et constant.
            # (et supposant deltaT constant aussi pour annuler la vitesse).
            v_reduc = -min(dv_frein_usu, vdirecte-objV)
            
            self._eff_ = self.orient_ * v_reduc                 
            self.print_log('    freinage', 'd_frei', d_freinage, 'versBut', versBut)
                                     
        else:
            # Phase d'acceleration
            self.print_log('    phase accel, vdir', vdirecte, self.orient_*100)
            
            dv_max = self._acc_v_ * deltaT
                 
            if objV is None or (versBut is not None):
                dv = dv_max
            else:
                dv = min(dv_max, objV-vdirecte)
                
            self._eff_ = self.orient_ * dv   

    def ControleDirection(self, avantArriere, gaucheDroite, deltaT):
        """ """
        
        self.w = -gaucheDroite * self.w_max
            
        if avantArriere:
            self._v_ = self.orient_ * self.v_max * avantArriere
        else:
            self._v_ *= 0 

    def ray_braq(self, versBut):
        # distance laterale parcourue pour un changement de direction vers versBut
        v_proj = self._v_ * versBut.uni_nml
        r = v_proj * v_proj / 2. / self._acc_f_
        return r

    def ValideObstacles(self, but_pos=None, verifAllure=True):
        """ verifAllure : si on vient d'assigner un objectif, utiliser verifAllure=False
            pour signifier que l'orientation actuelle n'est pas celle qui sera suivie pour aller vers le nouveau but.
        """

        versBut = but_pos - self.pos

        _dpos, obstacles = self.DetecteObstacles(versBut)
        
        if obstacles:
            for obst in obstacles:

                if isinstance(obst, tryangle.Wertex):
                    if not verifAllure or (but_pos-obst.pos)*self.orient_ > 0:
                        # obstacle imprevu pris en compte si il est plus proche que le prochain point d'itineraire
                        
                        cote = 1 if (obst.pos-self.pos) ^ versBut > 0 else -1

                        som = obst._vertex
                        if som.dualVert:
                            som = som.dualVert

                        self.ajoutObstacleImmediat(som, cote)
                        
                else:
                    # obstacle segment imprevu
                    if not verifAllure:
                        if versBut * obst.univec < 0:
                            som = obst.org
                            cote = 1 
                        else:
                            som = obst.dest
                            cote = -1 
                            
                    elif self.orient_ * obst.univec < 0:
                        som = obst.org
                        cote = 1
                    else:
                        som = obst.dest
                        cote = -1
                    
                    if ( not verifAllure or # pas de verification basee sur la direction suivie car celle-ci n'est peut etre pas alignee 
                        ((but_pos-som.coords) * self.orient_ > 0) # verifie que l'obstacle est avant le but, en fonction de l'orientation actuelle  
                        ):
                        if (( obst.univec ^ (but_pos-som.coords) > 0) ^ (obst.univec ^ (self.pos-som.coords) > 0)
                        or (but_pos-obst.org.coords)*(but_pos-obst.dest.coords) > 0 ):
                            # le but et la position doivent etre de cotes differents du segment.
                            # a moins que le point ne soit pas entre les deux extremites du segments.
                            self.ajoutObstacleImmediat(som.dualVert, cote)
                            break
   
    def ajoutObstacleImmediat(self, som, cote):

        if som.radius:
            marge = cote * som.radius * self.marge_contour
            obst = self.graphe.VertexToWertex[som]
        else:
            marge = cote
            obst = som.coords

        self.itineraire.ajout(obst, cote=marge, enTete=True)
        self.print_log("detection d'obstacle", som)
                        
    def Action(self, deltaT, deltaT0, profondeur=0):
        return EtreMobile.Avance(self, deltaT=deltaT, deltaT0=deltaT0, profondeur=profondeur)
    
    def dernierIti(self):
        iti = self.itineraire
        while iti.suivant is not None:
            iti = iti.suivant
        return iti
    
    def Chemin(self, but, depart=None):
        """ liste de points, porte joignant pos a but """

        Algo = PathFindAlgo.MinDist

        if depart is None:
            depart = self.pos

        return self.racine.graphe.Chemin(depart, but, Algo, 0., 2*self.surrayon*self.rayon)

    def calculItiChemin(self, *points_de_passage):

        jalons_chemin = [self.pos]
        jalons_chemin.extend(points_de_passage)

        jalons_chemin.append(self.itineraire.posns[-1])

        chemin = []
        num_pts_passage = len(jalons_chemin)

        for i, point_de_passage in enumerate(jalons_chemin):
            if i > 0:
                troncon = self.Chemin(point_de_passage, depart=jalons_chemin[i-1])

                if i == num_pts_passage-1:
                    chemin += troncon
                else:
                    chemin += troncon[:-1]

        buts = []
        cotes = []
        self.print_log('*** Nouveau chemin ***', *points_de_passage)

        for point, signe in chemin:
            buts.append(point)
            cotes.append(signe)
            self.print_log('\t', point, signe)
    
        self.itineraire.setButs(buts, cotes)

        if buts:
            self.ValideObstacles(buts[0], verifAllure=False)
                    
        self.valideSensMarche()

    def AssBut(self, but=None, ajout=False, itineraire=True, tol=None, **kwargs):
        ''' Assigne un objectif de position '''
        
        self.but_ = None

        if but is None:
            self.itineraire.__init__()
            
        else:

            if itineraire:

                if tol is None:
                    but_tol = self.rayon / 10
                else:
                    but_tol = tol

                novIti = Itineraire(rayon=self.rayon, buts=[but], tol=but_tol, **kwargs)

                if ajout:
              
                    iti = self.itineraire
                    while iti.suivant is not None:
                        iti = iti.suivant
                              
                    iti.suivant = novIti
                    
                elif ajout is None:
                    novIti.suivant = self.itineraire
                    self.itineraire = novIti
                    self.itineraire.tol = self.rayon*1.5
                    self.calculItiChemin()

                else:
                    self.itineraire = novIti
                    self.calculItiChemin()
                                        
            else:
                
                if ajout:
                    self.itineraire.ajout(but, enTete=False)
                else:
                    # pas de calcul de chemin
                    self.itineraire = Itineraire(rayon=self.rayon, depart=self.pos, buts=[but],  tol=but_tol, **kwargs)

    def valideSensMarche(self):
        
        but, rpivot, _but_suiv, _rpivot_suiv = self.itineraire.maj_pos(self)

        if but is None:
            return

        # marche arriere ou marche avant vers le nouveau but?
        versBut = but - self.pos
        
        if self.itineraire.marcheAvAr is None:
            if rpivot and versBut < 2 * max(self.rayon, self.ray_braq(versBut)):# and self._v_ > self.v_car /2:
                # si l'on est deja engage dans un mouvement de contournement,
                # prenons la decision de marche arriere 
                # par rapport a la normale au but a contourner.
                # Sauf si l'on est assez loin...
                self.marche_arriere = rpivot * versBut.nml * self._orient_ < 0
            else:
                self.marche_arriere = (but - self.pos) * self._orient_ < 0

            self.print_log('Marche Arriere', self.marche_arriere)
            
        else:
            self.marche_arriere = not self.itineraire.marcheAvAr

    def getRectContour(self, pos, orient, sideRadius=None):

        profondeur_av = orient * self.profondeur_av
        profondeur_ar = orient * -self.profondeur_ar

        if sideRadius is None:
            sideRadius = self.sideRadius()

        largeur = orient.nml * sideRadius
        contour = [pos + profondeur_av + largeur,
                   pos + profondeur_ar + largeur,
                   pos + profondeur_ar - largeur,
                   pos + profondeur_av - largeur]

        return contour

    def dessineContour(self, pos, orient, couleur, trait=0, cercle=False, sideRadius=None):
        # affiche la boite de deplacement.

        if self.profondeur_av is not None:

            contour = self.getRectContour(pos, orient, sideRadius)

            contour.append(contour[0]) # derniere arrete du trace

            self.racine.dessineLigne(*contour, couleur=couleur, trait=trait)

        else:
            self.racine.dessineCercle(pos, self.srayon, couleur, trait, coordsReeles=True)


        if cercle:
            self.racine.dessineCercle(pos, self.srayon, couleur, 1, coordsReeles=True)

    def Afficher(self):
        import pygame
        couleur = self.couleur
                        
        rayon = self.rayon
        pos   = self.pos
                    
        if rayon is None:
            return
        
        fact = 1. if self.racine.AfficheMouvement else self.surrayon
        self.racine.dessineCercle(pos, rayon*fact, couleur, trait=2)

        self.dessineContour(pos=self.pos, orient=self._orient_, couleur=self.couleur)

        rayon = int(self.rayon)
        
        if self.racine.AfficheMouvement >= 1:
            if self._coince_ > 0:
                # Cercle sombre si l'unite est coincee.
                couleur = [ int(val/4) for val in couleur ]
                self.racine.dessineCercle( pos, self.rayon/1.2, couleur, trait=4 )
                    
        # ligne d'orientation
        posLigne = pos + rayon * self._orient_
        couleur_orient = [ (255-i)/2 for i in couleur ] # Couleur_Blanc
        self.racine.dessineLigne( pos, posLigne, couleur=couleur_orient )

    def DessineCollision(self, axe_coll, point_contact, couleur):
        """:type point_contact: Vec
           :type axe_coll: Vec """
        
        axe = axe_coll * self.rayon / 2
                            
        self.racine.dessineLigne( point_contact, point_contact + axe, couleur=couleur )
        axn = axe.nml
        self.racine.dessineLigne( point_contact + axn, point_contact - axn, couleur=couleur )
    
    def AfficherInfo(self, deltaT, DansSelection=False):
        import pygame
        
        if self._TacheTour != EtatCollision.Debut and self.racine.pasapas in [PasAPas.Selection, PasAPas.SelecSeule, PasAPas.Unite]:
            couleur = 100,100,100
            self.racine.dessineCercle( self.pos, int(self.rayon/4.), couleur, trait=2 )

        if self.racine.AfficheMouvement:
            
            Vecteurs = []
            if self.racine.AfficheMouvement >= 2 and self._eff_:
                Vecteurs.append( ( 4*self._eff_, self.CouleurTrac, 6) )
                
            if self._v_:
                Vecteurs.append( (self._v_, self.CouleurVitesse, 3) )

            for vec, couleur, trait in Vecteurs:
                # Dessine le vecteur vitesse
                self.racine.dessineLigne(self.pos, self.pos + vec, couleur=couleur, trait=trait, lisse=True)
            
            if self.w:
                points = []
                for signe in 1, -1:
                    pt1 = self.pos - signe * self.rayon * self._orient_.nml
                    pt2 = pt1 + 2 * signe * self.rayon*self.w * self._orient_
                    if signe > 0:
                        points += [pt2, pt1]
                    else:
                        points += [pt1, pt2]
                    
                self.racine.dessineLigne(*points, couleur=(0,150,150), trait=3, lisse=True)
            
                    
        #self.racine.dessineCercle(self.pos, (self.entourage), self.CouleurTrac)
        #self.racine.dessineCercle(self.pos, (self.entourage2), self.CouleurVitesse)
        if self.point_arret_selection():
            pass
        
        if DansSelection:

            if self.racine.AfficheMouvement:

                if self.racine.Afficher and self.racine.AfficheMouvement > 1:
                    dessineCollisions = [self]
                    
                    #for autr in self.__collisions__:
                    #    if autr not in self.racine.Selection:
                    #        dessineCollisions.append(autr)
                    couleur = 0,200,180        
                    for elem in dessineCollisions:
                    
                        for p, axe_coll in elem.__info_collision__:
                            
                            self.DessineCollision(axe_coll, p, couleur)
                
                
                #self.racine.dessineLigne(self.pos, self.pos + self._v_*self._v_.nor/self._acc_f_, couleur=self.CouleurVitesse, lisse=True)
                    
                Depla = self._v_*deltaT
                
                if self._glissiere_:
                    
                    glissiere, glissObj = self._glissiere_
                    #gli_nml     = glissiere.nml
                    pos     = self.pos
                    couleur = 255,255,0
                    #for signe in 1, -1:
                    #    p1 = pos + glissiere.nml * signe * self.rayon - glissiere * self.rayon / 2.
                    #    self.racine.dessineLigne( p1, p1 + glissiere * self.rayon, couleur=couleur, trait=2, lisse = False)
                        
                    Depla = glissiere * (Depla * glissiere)
                    
                    self.racine.dessineLigne(pos, pos + Depla, couleur=couleur, trait=2, lisse = False)
                    if glissObj:
                        pos2 = pos + (glissObj.pos-pos).iuni * self.rayon
                        self.racine.dessineLigne(pos2, pos2 + Depla*.5, couleur=couleur, trait=2, lisse = False)
                    
                
                if self.racine.AfficheMouvement == 2:
                    
                    Mods = pygame.key.get_mods()
                    
                    DeplaSouris = len(self.racine.Selection) == 1 and Mods & pygame.KMOD_ALT
                    
                    if DeplaSouris:
                        # Regle le deplacement sur la position de la souris
                        PosSouris = self.racine.CoordsReeles(pygame.mouse.get_pos())
                        Depla = PosSouris - self.pos
                        
                        if Mods & pygame.KMOD_SHIFT:
                            # aligne la vitesse sur la direction de la souris
                            # en preservant la norme
                            v_nor = self._v_.nor
                            self._v_ = PosSouris - self.pos
                            self._v_.nor = v_nor
                            

                    if Depla > 1:

                        sideRadius = .8*self.sideRadius()
                        depla_max, Obstacles = self.PremierObstacle(Depla,
                                                                    sideRadius = sideRadius,
                                                                    relative    = True,
                                                                    checkResult = False,
                                                                    coteTol     = 0 )
                                                    
                        if not DeplaSouris and Obstacles and self._glissiere_ and self._glissiere_[1] and self._glissiere_[1] not in Obstacles: 
                            Obstacles.append(self._glissiere_[1])

                        # Affiche la position souhaitee apres le deplacement
                        if depla_max is None:
                            depla_max = Depla

                        nouvPos = self.pos + depla_max
                        orient_depla = depla_max.uni

                        if Depla * self._orient_ < 0:
                            orient_depla *= -1

                        self.dessineContour(pos=nouvPos, orient=orient_depla, trait=1, couleur=self.CouleurVitesse, sideRadius=sideRadius)

                        # affiche la boite de deplacement.
                        for signe in 1, -1:
                            decal = signe * sideRadius * Depla.uni_nml
                            self.racine.dessineLigne(self.pos + decal, nouvPos + decal, couleur=self.CouleurVitesse, lisse=True)
                            
                        self.racine.dessineLigne(self.pos, self.pos + Depla, couleur=self.CouleurVitesse, lisse=True)
                        
                        for Autr in Obstacles:
                            if isinstance(Autr, tryangle.Wertex):
                                Axe  = (nouvPos - Autr.pos).iuni
                                self.racine.dessineCercle(Autr.pos,
                                                          self.rayon/2,
                                                          self.CouleurVitesse,
                                                          trait=3)
                            else:
                                # surligne segment
                                Axe = -Autr.univec.nml
                                self.racine.dessineLigne(Autr.org.coords,
                                                         Autr.dest.coords,
                                                         couleur=self.CouleurVitesse,
                                                         trait=3,
                                                         lisse=False)

                            if self.sideRadius() == 0:
                                pointCollision = nouvPos - Axe * self.frontRadius(Depla)

                                self.DessineCollision(Axe, pointCollision, self.CouleurVitesse)
                                    


            # position precedente
            if False:
                self.racine.dessineCercle(self.pos_prev, self.rayon, self.couleur, trait=1)
            
            # Affiche l'objectif
            
            if self.racine.AfficheMouvement:
                
                if getattr(self, 'but_aff', None):
                    self.racine.dessineCercle(self.but_aff, 15, couleur=(45,200,100), trait=0)

                prox, rpivot, _pivot, _rpivot_suiv = self.itineraire.maj_pos(self)
                                    
                if rpivot and self._v_ and abs(rpivot) * 1.1 >= self.pos - prox:
                    self.racine.dessineLigne(self.pos, self.pos-rpivot*self.orient_.nml, couleur=(130,70,110), trait=3, lisse=True)

                if hasattr(self, 'versBut'):
                    self.racine.dessineLigne(self.pos, self.pos+self.versBut, couleur=(10,170,50), trait=3, lisse=True)

                if hasattr(self, 'vieux_but'):
                    self.racine.dessineCercle( self.vieux_but, 10/self.racine.VueRatio, (170,255,100), trait=0 )
                    
            if self.but_:
                
                if self.racine.AfficheMouvement:
                    
                    pos_iti = self.but_
                    if pos_iti:
                        self.racine.dessineLigne(self.pos, pos_iti, couleur=(0,255,0), trait=1, lisse=True)
                                                
                    if False and self.racine.AfficheMouvement > 1:
                        # voisin evitables  
                        for som in self.Voisins(Degre=1, IncludeBornes=True):
                            axe = som.pos - self.pos
                            if self.orient_ * axe > 0:
                                if self.bloque_exterieur(som):
                                    self.racine.dessineLigne(self.pos, som.pos, couleur=(250,250,0), trait=1, lisse=True)

            self.itineraire.affiche(self.racine)