'''
Created on 25 Sep 2009

@author: Guiche
'''

import math

from . import robot
from pygapp.app import clic
from pygapp.partieutils import PasAPas
from tricarto import vecToAngle, angleToVec
from tricarto.afficheur import AfficheurDeGraphe, barycentre, pygame, souris, Vec


def roundVec(vec, threshold, numdigits=1):

    if vec < threshold:
        return angleToVec(round(vecToAngle(vec)/math.pi, numdigits)*math.pi) * vec.nor

    return vec


class Partie(AfficheurDeGraphe):

    def __init__(self):
                
        DimEcran = 650, 550 #1200, 700
        
        AfficheurDeGraphe.__init__(self, ClasseUnite=robot.Robot, DimEcran=DimEcran)

        self.AfficheTriangles = False

        self.SelectCollision = False # Transmet la selection aux unites carambolees au tour precedent.
                        
        self.ValideMouvement = False

        self.Camps = []

        self.sequenceAction = None

        self.nom = 'rosebotnav'

    #_champs_description_  = AfficheurDeGraphe._champs_description_ + \
    #                         list('SauvegardeAutoActivee ImgParSec'.split()) 
         
    def AfficheInterruption(self):
        pass # AFAIRE

    def InitPreBoucle(self):

        # Ajout des unites sur la carte
        if not self.graphe:
            self.peupler()

        AfficheurDeGraphe.InitPreBoucle(self)

        try:
            import rospy
        except ImportError:
            self.SauvegardeAutoActivee_ = True

        self.ClasseUnite.racine = self  # variable statique


    def MajTourFuncIter(self,deltaT,tempsT):
        
        robot.Robot.horloge = self.NumeroDuTour

        TousSommets = self.graphe.AllVertices()
        
        ## selection automatique du robot pour ne pas avoir a reselectionner tout le temps
        ## (deselection par clic intempestive de l'ardoise souris). 
        #self.Selection = set(TousSommets)
        
        for camp in self.Camps:
            for groupe in camp.groupes:
                groupe.maj_barycentre()

        if self.SelectCollision:
            # Transmet la selection aux unites carambolees au tour precedent.
            for etant in list(self.Selection):
                if etant._emboutisseurs_:
                    self.Selection.discard(etant)
                    for autr in etant._emboutisseurs_:
                        self.Selection.add(autr)
                
        #etant = list(self.Selection)[0]
        #self.cmdPlot.update(etant._v_ * etant._orient_)
                
        if self.pasapas == PasAPas.SelecSeule:
            PreTourEtants = self.Selection
        else:
            PreTourEtants = TousSommets
        
        for etant in PreTourEtants:
            etant.PreTour(deltaT=deltaT)

        majFuncIter = ( lambda : agent.Action(deltaT, deltaT) for agent in PreTourEtants ) 
        
        return majFuncIter


    def PeintUneToile(self, deltaT):

        AfficheurDeGraphe.PeintUneToile(self, deltaT)

        NombSelec = len(self.Selection)

        if NombSelec == 1:
            agent = next(iter(self.Selection))
            couleur = agent.couleur

            pos_souris = self.CoordsReeles(souris.get_pos())
            if self.etatSouris.jalons:
                pos = self.etatSouris.jalons[-1]
                orient = roundVec(pos_souris-pos, 2*agent.rayon)

                if not orient:
                    orient = agent._orient_
                else:
                    orient.iuni

                self.dessineLigne(pos, pos_souris, couleur=couleur, trait=2)
                agent.dessineContour(pos=pos, orient=orient, couleur=couleur, trait=1, cercle=True)

            else:
                pos = pos_souris
                self.dessineCercle(pos, agent.srayon, couleur, 1, coordsReeles=True)

        if NombSelec and not pygame.key.get_mods() & pygame.KMOD_CTRL:

            if clic.droite_enfonce() or clic.gauche_enfonce():
                # Dessine chemin prospectif

                pos_souris = self.CoordsReeles(souris.get_pos())

                for agent in self.Selection:
                    couleur = agent.couleur
                    positions = []
                    if hasattr(agent, 'Chemin'):
                        Chemin = agent.Chemin(pos_souris)
                        positions = [point for point, _porte in Chemin]
                        positions.append(agent.pos)

                for pos in positions:
                    self.dessineCercle(pos, 3./self.VueRatio, couleur, 3, coordsReeles=True)

        if self.sequenceAction is not None:
            self.sequenceAction.draw(self)

    def Eradiquer(self):
        self.VidangeSelect()
        #tryangle.Mesh.DeleteMesh()
    
    def AssigneObjectifs(self, ref_pos, **kwargs):
        
        for etre in self.Selection:
                        
            obj_pos = ref_pos
            if hasattr(etre, 'AssBut'):
                orient = kwargs.pop('orient', None)
                if orient:
                    orient = roundVec(orient, etre.rayon*2)
                etre.AssBut(obj_pos, orient=orient, **kwargs)
            
        if self.SauvegardeAutoActivee:
            self.AutoSauveExc()


    def ChocVitesse( self, Radial=True, Effectif=True, Inverse=True ):
        ''' Donne une impulsion de vitesse au groupe Unites '''
        
        Unites = self.Selection
        couleur = 185,185,185
        
        if self.VueSubjective:
            etre = self.SuitSelection
            pos = reference_pos = etre.pos
            dV = etre.orient_ * 80
            
            self.dessineLigne( pos, pos + dV, couleur=couleur )
            
            if Inverse:
                dV *= -1
                
            if Effectif:
                etre._v_ += dV * 10

        else:     
            reference_pos = self.CoordsReeles(souris.get_pos())
            
        if not Radial:
            baryCentre = barycentre(unit.pos for unit in Unites)
            
        for Vert in Unites:
            
            if Radial:
                dV = Vert.pos - reference_pos
            else:
                dV = baryCentre - reference_pos
            
            if Inverse:
                dV *= -1
                    
            if Effectif:
                son_saut = "smb_jump-super.wav" if dV > 150 else "smb_jump-small.wav" 
                self.joue_son(son_saut)
                
                if not self.VueSubjective or Vert is not self.SuitSelection:
                    Vert._v_ += dV
            else:
                pos = Vert.pos
                self.dessineLigne( pos, pos + dV, couleur=couleur )

    def TraiteEtat(self, deltaT):
        """ """
        AfficheurDeGraphe.TraiteEtat(self, deltaT)
        
        KeyPressed = pygame.key.get_pressed()        
        mods = pygame.key.get_mods()
        
        # Visualisation du choc de vitesse
        if KeyPressed[pygame.K_z] and hasattr(self, 'ChocVitesse'):
            
            Radial  = not mods & pygame.KMOD_CTRL
            Inverse = mods & pygame.KMOD_SHIFT
            
            self.ChocVitesse(Radial, Effectif=False, Inverse=Inverse)

    def TraiteEvenementMid(self, event):

        AfficheurDeGraphe.TraiteEvenementMid(self, event)

        mods = pygame.key.get_mods()

        if event.type == pygame.MOUSEMOTION:
            if mods & pygame.KMOD_ALT:
                if self.etatSouris.jalons:
                    self.etatSouris.jalons[0] += self.VecReel(event.rel)


        if event.type == pygame.MOUSEBUTTONDOWN:

            if event.button == clic.droit:
                pass

            elif event.button == clic.milieu:
                pass
            elif event.button == clic.gauche:
                if clic.droite_enfonce():
                    pass  # self.AssPositionArmee = True

            elif event.button in (clic.mol_bas, clic.mol_haut):  # Molette

                if clic.droite_enfonce():

                    # Molette quand le bouton droit est enfonce

                    if self.Selection:

                        if mods & pygame.KMOD_ALT or mods & pygame.KMOD_SHIFT:

                            fact = 1.1
                            if event.button == clic.mol_haut:
                                fact = 1 / fact

                            if mods & pygame.KMOD_SHIFT:
                                pass  # PlanPos.ecartementHL[1] *= fact

                            if mods & pygame.KMOD_ALT:
                                pass  # PlanPos.ecartementHL[0] *= fact


                elif mods & pygame.KMOD_CTRL:
                    pass

        elif event.type == pygame.MOUSEMOTION and \
                (event.buttons[2] or (len(self.etatSouris.jalons) == 0 and event.buttons[0])):

            # deplacement souris avec la boutton droit enfonce ou le bouton gauche enfonce.
            if self.Selection:
                pass  # point2 = self.CoordsReeles(event.pos)

            if Vec(*event.rel) > 2:
                # pas de double-clic
                pass  # self.AssPositionArmee = False

        elif event.type == pygame.MOUSEBUTTONUP:

            if event.button == clic.droit:

                if not self.ModeInfra:
                    pos_souris = self.CoordsReeles(event.pos)

                    if self.Selection and not clic.gauche_enfonce():

                        if mods & pygame.KMOD_CTRL or len(self.Selection) == 1:
                            # Assignement d'objectifs aux unites de la selection

                            if self.etatSouris.jalons:
                                orient = None

                                obj_pos = self.etatSouris.jalons[-1]

                                if event.pos - self.CoordsVues(obj_pos) >= 15:
                                    # si la distance entre le clic descendant et le clic montant est trop courte,
                                    # n'assignons pas d'orientation
                                    orient = pos_souris - obj_pos

                                forceMarcheAvant = None

                                if mods & pygame.KMOD_SHIFT:
                                    forceMarcheAvant = True

                                calculeItineraire = True
                                ajout = True if mods & pygame.KMOD_CTRL else False

                                if mods & pygame.KMOD_ALT:
                                    ajout = None

                                self.AssigneObjectifs(obj_pos,
                                                          itineraire=calculeItineraire,
                                                          ajout=ajout,
                                                          orient=orient,
                                                          avAr=forceMarcheAvant)

                self.SequencePos = False

                if self.ModeInfra:
                    # Deplacement des unites a leur objectif directement
                    for etre in self.Selection:
                        if hasattr(etre, 'but_'):
                            but = etre.but_
                            if but is not None:
                                etre.pos = but.objectif()
                                obj_orient = but.objectif_orient()
                                if obj_orient:
                                    etre.orient = obj_orient
                                    etre.AssBut()  # effacage de l'objectif

            elif event.button == clic.gauche:

                pass

        elif event.type == pygame.KEYDOWN:

            touche = event.key
            from pygapp.partieutils import TOUCHES_NUM

            if touche in TOUCHES_NUM:

                if not event.mod & pygame.KMOD_CTRL:
                    # selection des coquillages
                    intKey = int(pygame.key.name(touche)[-1])
                    if 0 <= intKey <= 5:
                        self.configCoquillages = intKey
                        if event.mod & pygame.KMOD_SHIFT:
                            self.insereCoquillages(intKey)

            elif touche == pygame.K_e:
                from pygapp import menus
                from beachbots import scenarios
                self.sequenceAction = menus.ChampChoix(None, choix=scenarios.SEQUENCES, legende=["Choisir une sequence : "]).boucle()

            elif touche == pygame.K_v:
                if event.mod & pygame.KMOD_SHIFT:
                    self.valide_Mouvement_ = not self.valide_Mouvement_
                    print('ValideMouvement', self.valide_Mouvement_)

            elif touche == pygame.K_w:
                # annule/active le mouvement de rotation propre
                if any(etre.w for etre in self.Selection):
                    val = 0
                else:
                    val = 2
                for etre in self.Selection:
                    etre.w = val

            elif touche == pygame.K_b:
                hasbut = any(etre.itineraire or etre.but_ for etre in self.Selection)
                for etre in self.Selection:
                    if hasbut:
                        if mods & pygame.KMOD_CTRL:
                            # Efface les objectifs
                            etre.AssBut()
                        else:
                            # saute le premier but seul
                            etre.itineraire.increme()
                    else:
                        etre.AssBut(etre.pos)


    def peupler(self):
        self.graphe = GrapheMinimal()


def GrapheMinimal():

    from tricarto.meshutils import ReseauTriangle

    graphe = ReseauTriangle( nom='Triangle', largeur=100, hauteur=100, marge=10 )

    robot.Robot(pos=(40, 40), taille=5, Reseau=graphe)

    return graphe