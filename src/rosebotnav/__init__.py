import pygapp.media
import pygapp.sauvegarde

pygapp.media.DOSSIER_SAUVEGARDES = 'sauvegardes'
pygapp.media.DOSSIER_IMAGES      = 'images'
pygapp.media.DOSSIER_SONS        = 'sons'

pygapp.media.setupResourcePath(__file__) 

from .racine import Partie

pygapp.sauvegarde.LOAD_SUBSTITUTION = {'socle.': 'tricarto.', 'rosebotnav.socle.': 'tricarto.'}

from . import racine

def main(classePartie=racine.Partie):

    import pygapp.jeu as jeu

    pygapp.jeu.CLASSE_PARTIE = classePartie

    jeu.BoucleJeu()
    

# this calls the 'main' function when this script is executed
if __name__ == '__main__': 
    main()
