'''
Created on 10 avr. 2013

@author: Apsara
'''
from pygapp.sauvegarde import Phenixable
from tricarto.tryangle import sommet


class Itineraire(Phenixable):
    """ suite de buts """

    Couleur = 50, 255, 10

    def __init__(self,
                 rayon=20,
                 depart=None,
                 buts=[],
                 cotes=None,
                 orient=None,
                 avAr=None,
                 rectiligne=False,
                 tol=1,
                 tol_o=.05, # tolerance orientation, en radian
                 rappelSurObjectif = None):
        """
            @param rappelSurObjectif: methode appelee lorsque l'objectif est atteint.

            TODO suppression des donnees de cotes, deductible de la succession des points
        """
        self.index_ = 0
        self.setButs(buts, cotes)

        self.orient = orient
        self.rayon = rayon
        self.tol = tol
        self.tol_o = tol_o
        self.suivant = None
        self.precedant = None
        
        self.rectiligne = rectiligne
        self.marcheAvAr = avAr

        self.rappelSurObjectif = rappelSurObjectif

        if depart is None:
            self.depart = None
        else:
            self.depart = 1*depart

    def setButs(self, buts, cotes):

        self.posns = buts

        if cotes is None:
            self.cotes = [0] * len(buts)
        else:
            self.cotes = cotes

        self.index_ = -len(buts) # index a reculons

        assert(len(self.posns) == len(self.cotes)), "%s %s - len %d %d"%(str(buts), str(cotes), len(buts), len(self.cotes))

        # ERROR : NoneType error with len(self.posns) or len(self.cotes)
        # if buts and len(cotes) == 0:
        #     self.cotes  = [0] * len(buts)
        # else:
        #     self.cotes = cotes
        # self.index_  = -len(buts) # index a reculons
        #
        # assert(len(self.posns) == len(self.cotes)), "posns: %d, cotes: %d"%(len(self.posns), len(self.cotes))

    @property
    def prevPoint(self):
        """ Point d'ou l'on vient avant le pivot courant """
        if self.index_ == -len(self.posns):
            return self.depart
        else:
            return self.posns[self.index_-1]

    def atteint(self):
        return self.index_ == 0

    def __bool__(self):
        return bool(self.posns)
    
    def __bool__(self):
        return bool(self.posns)
    
    def __len__(self):
        return len(self.posns)
    
    def __str__(self):
        msg = '[%d:%d] '%(self.index_,len(self.posns)) + ', '.join(str(but) for but in self.posns[:3])
        return msg
                
    def ajout(self, pos, cote=0, enTete=True):

        if enTete and self.index_ != 0:
            self.posns.insert(self.index_,pos)
            self.cotes.insert(self.index_,cote)
        else:
            self.posns.append(pos)
            self.cotes.append(cote)
        
        self.index_ -= 1
    
    def increme(self):
        self.index_ += 1
        
    def maj_pos(self, agent):
        
        marge_contour = agent.marge_contour
        
        if self.index_:
            marge = agent.rayon * marge_contour 
            but = self.posns[self.index_]
            rpivot = self.cotes[self.index_]
            if rpivot != 0:
                if rpivot < 0:
                    rpivot -= marge
                else:
                    rpivot += marge

            index_suiv = self.index_+1
            if index_suiv:
                but_suiv = self.posns[self.index_+1]
                rpivot_suiv = self.cotes[self.index_+1]
                if rpivot_suiv != 0:
                    if rpivot_suiv < 0:
                        rpivot_suiv -= marge
                    else:
                        rpivot_suiv += marge
            else:
                but_suiv = None
                rpivot_suiv = 0

            if isinstance(but, sommet.Wertex):
                but = but.pos

            if isinstance(but_suiv, sommet.Wertex):
                but_suiv = but_suiv.pos

            return but, rpivot, but_suiv, rpivot_suiv

        return None, 0, None, 0

    def finaliser(self, agent):
        """ appeler quand objectif atteint en position et en orientation """
        if self.rappelSurObjectif:
            self.rappelSurObjectif()
            self.rappelSurObjectif = None

        if self.suivant:
            agent.itineraire = self.suivant
            agent.calculItiChemin()

    def affiche(self, racine, index=0):
        
        if self.index_ or self.orient: # le but n'est pas encore atteint.
            
            buts = self.posns[self.index_:] if self.index_ else self.posns[-1:]

            if buts:
                for but in buts:
                    if hasattr(but, 'affiche'):
                        but.affiche(racine)
                    else:
                        if hasattr(but, 'pos'):
                            pos = but.pos
                        else:
                            pos = but

                        racine.dessineCercle(pos, 2/racine.VueRatio, self.Couleur, trait=2)

                racine.dessineCercle(but, self.rayon, self.Couleur, trait=2)
            
                etiquette = ""
                if index:
                    etiquette = str(index)

                if self.rectiligne and False:
                    etiquette += ' rect'

                if etiquette:
                    racine.afficheParatexte(etiquette, but, couleur=self.Couleur, coordsReeles=True)

                if self.orient:
                    racine.dessineLigne(but, but + self.orient.uni*300, couleur=self.Couleur, trait=1, lisse=True)

        if self.suivant:
            self.suivant.affiche(racine, index+1)
