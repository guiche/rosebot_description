
from tricarto.tryangle import Vec
from pygapp import sauvegarde

class Camp( sauvegarde.Phenixable ):
    
    EnsembleValeurs = []


    CAMP_NEUTRE = "Neutre"

    def __init__(self, Nom='', couleur=(200,200,200)):
        self.nom        = Nom
        self.couleur    = couleur
        self.groupes    = []
        if Nom:
            # Nom '' est invalide
            for camp in self.EnsembleValeurs:
                if camp.nom == Nom:
                    raise Exception("Camp %s existe deja"%Camp)
            self.EnsembleValeurs.append(self)
         
    def __str__(self):
        return self.nom
    
    def __eq__(self, autre):
        return self.nom == autre.nom and self.nom != self.CAMP_NEUTRE

    def __ne__(self, autre):
        return self.nom != autre.nom or self.nom == self.CAMP_NEUTRE
    
    def __setstate__(self,dico):
        sauvegarde.Phenixable.__setstate__(self,dico)
        while None in self.groupes:
            self.groupes.remove(None)
            
        if self not in self.EnsembleValeurs:
            self.EnsembleValeurs.append(self)

            
    @classmethod
    def getCamp(cls, nom, **KWargs):
        for camp in cls.EnsembleValeurs:
            if camp.nom == nom:
                return camp
            
        camp = Camp(nom,**KWargs)
        #cls.EnsembleValeurs.append(camp)
        return camp
