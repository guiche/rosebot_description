'''
Created on 28 Mar 2013

@author: j913965
'''
import tricarto.tryangle as tryangle
import math
from tricarto.tryangle import Vec, Wertex
from tricarto.tryangle import SommetBorne
from pygapp.partieutils import SousEnsembleException
import tricarto.tryangle.sommet

import random
import traceback
sinPiSur8 = math.sin(math.pi/8)

Wertex.surrayon = 1.


class EtatCollision:
    Debut   = 'Debut'
    EnCours = 'EnCours'
    Fini    = 'Fini'


def orient_directe( pt1, pt2, pt3 ):
    return (pt2-pt1) ^ (pt3-pt1) >= 0


CouleurJaune = (0, 255, 255)


class ElemActifInterface(object):
    
    racine = None
    
    def PreTour(self,deltaT):
        pass
    
    def Action(self,deltaT, deltaT0, profondeur=0):
        pass
    
    
class OrientableInterface(object):

    @property    
    def orient(self):
        return self.orient_
    
    @orient.setter
    def orient(self, val):
        """ Garantit (par ex. pour l'editeur) que l'orientation est normalisee a 1 """
        self.orient_ = val
        self.orient.nor = 1
    
    def objectif_orient(self,deltaT):
        if self.but_:
            return self.but_.objectif_orient()
    
    
    def diffAngle(self, direction):
        return math.atan2(self.orient_^direction, self.orient_*direction)


    def tempsAtteinte(self, versBut, objV=0):
        
        v_but = versBut.uni * self._v_

        d = versBut.nor

        t_0 = max(d/self.v_max, math.sqrt(2*d/self._acc_v_))        

        
        if v_but:
            return min(d / v_but, t_0) 
        else:
            return t_0
        
        return 999
        t_vmax = versBut.nor / self.v_max
        return t_vmax
        """
        t_acc = abs(self.v_max - self._v_* versBut.uni)/self._acc_v_

        if objV is None:
            return t_vmax + t_acc
        
        t_frein = abs(objV - self.v_max)/self._acc_f_
        return t_acc + t_vmax + t_frein
        """
        
        
    def tempsOrient(self, direction, objW=0, angle_tol = .05):


        diffAngle = abs(self.diffAngle(direction))
        
        if self.w:
            t_w = abs(diffAngle / self.w)
            if objW is None:
                return t_w

            t_frein = abs(self.w-objW) / self._acc_wf_
            return t_w + t_frein
             
        elif diffAngle < angle_tol:
            if objW is None:
                return 0
            else:
                t_frein = abs(self.w-objW) / self._acc_wf_
                return t_frein
                
        else:
            return self.w_max / self._acc_w_


    def distFrein(self, v, objV, acc):
        # distance theorique (temps continu) pour annuler la vitesse avec un freinage maximum et constant.
        # en premiere approx, suppose le freinage visqueux constant
        return (v**2-objV**2) * .5 / acc
    
    def maxVel(self, dist, objV, acc):
        # dist = (vdirecte**2-objV**2) * .5 / acc_f_usu
        return math.sqrt(dist * 2. * acc + objV**2)
     
        
    def Sorienter(self, deltaT, direction=None, obj_w=None, angle_tol=.05):
        """ True si l'agent est aligne sur la direction souhaitee"""
        #AFAIRE braquage depend de la vitesse

        if obj_w is not None:
            wrel = self.w - obj_w                                
        else:
            wrel = self.w

        dw_max = self._acc_w_ * deltaT

        if direction is not None:

            angle = self.diffAngle(direction)

            if abs(angle) < angle_tol:
                if obj_w is None:
                    return True
                elif abs(wrel) < dw_max:
                    self.w = obj_w
                    return True
        else:
            angle = None

        d_tol = angle_tol
        
        if angle is None:
            objWProche = True
        elif wrel * angle < 0 or wrel == 0:
            objWProche = False
        else:
            d_freinage = self.distFrein(wrel, 0, self._acc_wf_) + d_tol
            
            objWProche = abs(angle) <= d_freinage


        if objWProche:
            # ajustement de w en fonction de la direction cible ou de obj_w
            
            if angle:
                # nombre de tours a freinage constant pour annuler la vitesse (suppose deltaT constant)
                n_frein = math.ceil(2*abs(angle/wrel)/deltaT+1)
                dw = min(dw_max, abs(wrel) / n_frein)
            else:
                dw = dw_max
        
            dw = min(dw, abs(wrel))

            if (obj_w is not None and wrel > 0) or (obj_w is None and angle > 0):
            #if angle > 0: # on cherche a ralentir dans la fermeture de l'angle : accel angulaire dans le sens oppose.
                dw *= -1

            #self.print_log('    ajust angul', self.w, obj_w, dw)
        else:
            
            dw = dw_max
                
            if angle is not None and angle < 0:
                dw *= -1


        self.w += dw
        
        return False
            
    def getdW(self, deltaT, obj):

        dw = deltaT * self._acc_w_
        
        dw = min(abs(self.w-obj), dw)
        
        if self.w < obj:
            return dw
        else:
            return -dw
            
            
class EtreMobile( tryangle.Wertex, ElemActifInterface ):
    
    
    _depla_strict_ = False # si une unite s'arrete des qu'elle cogne une unite dont le tour est deja fini.
    
    _v_rad      = math.pi/2 # vitesse radiale en rad/s
    _acc_cen_   = 550 # acceleration centripete max avant decrochage

    CouleurVitesse = 255,0,255
    CouleurTrac    = 255,127,0
        
    #__attr_reinit__ = '_acc_v_', '_acc_f_', '_visco_'
    
    def __init__(self,  pos     = None, 
                        Reseau  = None,
                        taille  = 150,
                        nom     = None,
                        V0      = None ):
        
        tryangle.Wertex.__init__(self, pos=pos, reseau=Reseau, taille=taille, nom=nom)
                        
        self.masse_      = math.pi*taille**2
        self._eff_       = Vec(0, 0) # effort interne de deplacement
                
        if V0 is not None:
            self._v_     = V0
        
        self._acc_v_     = 30 * taille
        self._acc_w_     = 1.5 # rad/s
        self._acc_f_     = 50 * taille # decelleration de freinage
        #self._acc_cen_   = 50 * taille # acceleration centripete max
        self._visco_     = 1.15
        
        self._TacheTour = EtatCollision.Debut
        
        self._emboutis_    = []
        self._coince_    = 0
        self._inobstrue_ = True
        self._glissiere_ = None
        self.obst_projetes = []
        
        self.InitInfoCollision()
        self._print_log = True
        
        self.coeff_glisse = 1
        self.detectionCollisionActive = True
            
    def point_arret_selection(self):
        return self.racine.pasapas and self in self.racine.Selection 

    def print_log(self, *val):
        if self._print_log:
            print(self.racine.NumeroDuTour, ' '.join(str(v) for v in val))
             
    def Renomme(self):
        self.nom_ = ''
                   
    def Interse(self, arg, increment):
        if isinstance(arg, EtreMobile):
            distance2 = (self.pos+increment-arg.pos).nor2
            r1 = self.rayon
            r2 = arg.rayon
            if distance2 < ( r1 + r2 ) ** 2:
                return r1 + r2 - math.sqrt(distance2)
            else:
                return False
        else:
            assert False
                                    
    def InitInfoCollision(self):

        self.__info_collision__  = []
        self._emboutisseurs_     = []

    def PreTour(self, deltaT):
        #
        # Comportement purement physique
        #
        self._TacheTour = EtatCollision.Debut
                            
    def Action(self,deltaT, deltaT0, profondeur=0):
        if profondeur == 0 and self._TacheTour == EtatCollision.Fini:
            return
        
        return self.Avance(deltaT, deltaT0, profondeur)

    def Deplace(self, dpos, deltaT, dist_tol):
        """
            :type dpos: Vec
        """
        # Si le deplacement est suffisament petit, ignorons-le.
        if not dist_tol or dpos > dist_tol:
                
            self.dpos = dpos
                
            # freinage du aux frotements
            
            visco_dt = self._visco_ * deltaT * self.coeff_glisse
                             
            self._v_ *= math.exp(-visco_dt)
            return True
        
        return False
    
    def DetecteObstacles(self, dpos_ini):
            
        try:
            dpos, obstacles = self.PremierObstacle(dpos_ini,
                                                   relative     = True,
                                                   checkResult  = self.racine.ValideMouvement,
                                                   coteTol      = 0)
        except:
            if self._print_log:
                traceback.print_exc()
            dpos = dpos_ini
            obstacles = []
        
        if hasattr(self, 'orient_') and self.orient_.tan_abs_min(dpos_ini, 0.01):
            # si agent va dans la direction voulue
            self.obst_projetes = obstacles
        else:
            self.obst_projetes = []
            
        if dpos is None:
            dpos      = dpos_ini
            obstacles = []

        return dpos, obstacles
    
    def Avance(self, deltaT, deltaT0, profondeur):
        ''' Etant donne le pre calcul du vecteur vitesse,
            met a jour la position ou la modification du vecteur vitesse en cas de collision.'''
                
        if self._depla_strict_ or profondeur == 0:
            assert self._TacheTour != EtatCollision.EnCours
        
            if self._TacheTour == EtatCollision.Fini:
                return
        
        #print '  '* profondeur, self, self._TacheTour.ljust(5), self._v_ * deltaT, deltaT * 1000
        
        if self._v_ < .001:
            self._v_ *= 0

        if self.w:
            dw = self.w * deltaT
            if hasattr(self, 'orient_'):
                self.orient_ = self.orient_.rot(dw)
                self._v_ = self._v_.rot(dw)
        
        aBouge = False

        if True:
                        
            if self._TacheTour == EtatCollision.Debut:
                self.InitInfoCollision()
             
            self._TacheTour = EtatCollision.EnCours
                        
            # Afaire : projeter sur self._v_ les vitesses des sommets voisins
            # et si cela est positif, leur donner le loisir d'avancer en priorite
            # pour eviter les collisions recursives ci-apres.
                        
            self._emboutis_ = []
            
            NombPasses = 0
            
            if self.point_arret_selection() or (profondeur > 4):
                _point_d_arret = None # point d'arret
                
            try:
                   
                while True:

                    dpos_ini = self._v_ * deltaT
                        
                    if self._glissiere_ and ( dpos_ini ^ self._glissiere_[0] ) < 0:
                        dpos_ini = self._glissiere_[0] * ( dpos_ini * self._glissiere_[0] )
                    
                    if self.detectionCollisionActive:
                            
                        dpos, Obstacles = self.DetecteObstacles(dpos_ini)
                        
                    else:
                        dpos = dpos_ini
                        Obstacles = []
                        
                    NombPasses += 1
                    NouvPos = self.pos + dpos
                    
                    NouvellePasseChemin = False
                                        
                    if Obstacles:

                        dpos_nor2    = dpos_ini.nor2
                        deltaT_ratio = 1-(dpos * dpos_ini)/dpos_nor2 if dpos_nor2 else 1. 
                        novDeltaT    = deltaT * deltaT_ratio
                            
                        autr = Obstacles[0]
                        
                        if isinstance(autr, tryangle.osub):
                        
                            if len(Obstacles) != 1:
                                raise SousEnsembleException( [self]+Obstacles,"%s %d multi coll avec un seg"%(self,len(Obstacles)))
                            
                            # si le seul obstacle est un segment, 
                            # excecute le reste du rebond a ce tour pour un comportement plus fluide.
             
                            if self.Collision( Obstacles, NouvPos ):
                                # mauvaise superposition contre un segment.
                                # qui fait que l'on ne peut s'en decoller meme si la vitesse
                                # pointe au loin.
                                pass # Afaire : ignorer l'obstacle contre lequel on coince
                                
                                if self._glissiere_:
                                    if self._glissiere_[1]:
                                        Obstacles = [ self._glissiere_[1] ]
                                    self._glissiere_ = None

                                else:
                                    assert "%s on attend une glissiere"%self
                                
                            else:
                                # proportion du temps restant a ce tour
                                deltaT = novDeltaT 
                                assert deltaT > 0
                                NouvellePasseChemin = deltaT_ratio > .15
                                    
                                aBouge = self.Deplace(dpos, deltaT, dist_tol=1)
                                
                                self._coince_   = 0
                                self._glissiere_ = [autr.univec, None]
                                break

                        
                        else:
                            
                            if self._glissiere_ and Obstacles and (dpos-dpos_ini) ^ self._glissiere_[0] > 1:
                                obstacle_glissiere = self._glissiere_[1]
                                if NombPasses <= 1 and obstacle_glissiere and obstacle_glissiere not in Obstacles: 
                                    Obstacles.append(obstacle_glissiere)
                            
                            for autr in Obstacles:
                                
                                                    
                                if not isinstance(autr, SommetBorne):
                                    # Collision contre d'autres sommets mobiles
                                    
                                    autr_doit_bouger = autr._TacheTour == EtatCollision.Debut
                                    
                                    if  autr_doit_bouger or (self._depla_strict_ and novDeltaT > .001): # 1ms
                                        
                                        axe = autr.pos - self.pos
                                                                            
                                        if (autr._v_-self._v_) * axe  > 0:
                                            # l'autre unite va dans le meme sens.
                                            if autr._glissiere_ and autr._glissiere_[1] == self:
                                                autr._glissiere_ = None
                                                 
                                            if autr.Action(deltaT=deltaT0 if autr_doit_bouger else novDeltaT, deltaT0=deltaT0, profondeur=profondeur+1 ):
                                                # le calcul n'est necessaire que si autr s'est deplace.
                                                NouvellePasseChemin = True
                                                                                                        
                    if NouvellePasseChemin:
                        continue
                    
                    else:
                        
                        if Obstacles:
                          
                            self._inobstrue_ = False 

                            self.Collision( Obstacles, NouvPos )
                            
                            if dpos < 1.4: #and all( autr._coince_ for autr in Obstacles )          
                                self._coince_ += 1
                            else:
                                self._coince_ = 0
                        
                            dist_tol = 1.
                            
                        else:
                            self._inobstrue_ = True
                            self._glissiere_ = None
                            self._coince_    = 0
                            
                            if self._glissiere_ is None and (dpos - dpos_ini) > 1e-3:
                                # pas de Collision
                                Message = "Deplacement realise differe du depla souhaite en l'absence d'obstacles"
                                print(dpos_ini,'->', dpos, dpos-dpos_ini, Message)
                                if True:
                                    _coords, _Obstacle = self.PremierObstacle(dpos_ini, relative=True,checkResult=self.racine.ValideMouvement)
                                    raise SousEnsembleException( [self],Message)
                            
                            dist_tol = 0.
                        
                        aBouge = self.Deplace(dpos, deltaT, dist_tol)
                                                  
                        break
                            
                                           
            except Exception:
                self._TacheTour = EtatCollision.Fini
                raise
                
            if NombPasses > 3 and self.racine.AfficheMouvement >= 1:
                print(str(self).ljust(15), "tour %d cas curieux - %d passes"%(self.racine.NumeroDuTour, NombPasses))
        
            
        #print '  '*profondeur, self, ' --> ', dpos, aBouge
        
        self._TacheTour = EtatCollision.Fini
        
        return aBouge

    def frontRadius(self, depla=None):
        return self.rayon

    def sideRadius(self):
        return 0

    def PremierObstacle(self, depla, **kwargs):

        sideRadius = kwargs.pop("sideRadius", self.sideRadius())
        return tricarto.tryangle.sommet.Wertex.PremierObstacle(self,
                                                               depla,
                                                               frontRadius=self.frontRadius(depla),
                                                               sideRadius=sideRadius,
                                                               **kwargs)

    def Collision(self, Obstacles, NouvPos):
        ''' echange des quantites de mouvement.
            Mise a jour de la vitesse.
            :type NouvPos: Vec
            :type Obstacles: list
        '''
        
        NumObstacles = len(Obstacles)
        
        Axes = []
        
        if NumObstacles > 0:
            v_orig_dir = self._v_.uni
            
        AutrBloquants   = []
        NumBloquants    = 0
        point_arret_selection = self.point_arret_selection()
        
        for autr in Obstacles:   
            
            if point_arret_selection:
                pass # point d'arret

            if isinstance(autr, tryangle.osub):
                # Collision contre un segment                               
                nml = (autr.dest.coords - autr.org.coords).uni #.iuni semble causer un probleme de memoire (valeur memoire corrompue depuis le changement boost python de Point2D
                axe = nml.anml
                
            else:
                self._emboutis_.append( autr )
                self._glissiere_ = None

                axe = (NouvPos-autr.pos).uni
                nml = axe.nml
                
            #axe : axe unitaire de collision dirige de autr vers self
            Axes.append( axe )
            
            self.__info_collision__.append( ( NouvPos - self.rayon * axe, axe ) )        
        
            # projection des vitesses sur l'axe de collision
            v1 = axe * self._v_
        
            if v1 >= 0:
                
                if NumObstacles == 1:
                    # Probleme : s'il n'y a qu'un obstacle, il doit se trouver dans la direction du deplacement
                    return True # renvoie True pour signifier qu'il faut ignorer cet obstacle.
                    #import racine
                    #raise SousEnsembleException( [self, autr], "%s->%s : s'il n'y a qu'un obstacle, il doit se trouver dans la direction du deplacement"%(self,autr))
                    
                continue
            
            if isinstance(autr, (SommetBorne, tryangle.osub)):
                # Coin de segment ou segment
                                                
                m1 = self.masse_
                m2 = 0
                if m2:
                    ratio_M = m1/m2
                else:
                    ratio_M = 0
                    
                coef_elas = self.elas * autr.elas/(self.elas + autr.elas)
                
                k = (1 + coef_elas)/(1+ratio_M)
                v2 = 0
                ve = k * (v2-v1)
                                
                coef_frot = min(self.frot, autr.frot)
                dV1 = ve*axe
                
                if coef_frot:
                    w1 = self.w
                    w2 = 0
                    r1 = self.rayon
                    r2 = 0
                    V1 = self._v_
                    V2 = Vec(0,0)
                    dV = V1-V2
                                            
                    wca = dV * nml - r1 * w1 - r2 * w2
                    if wca == 0 or 1 < 3.5 * coef_frot * (1+ ratio_M) * ve / abs(wca):
                        # roulement                        
                        if wca != 0:
                            # adherence pendant le choc
                            dV1 -= 2/7.*wca*nml / (1+ratio_M)
                            self.w += 5./7/r1*wca / (1+ratio_M)
                            #dw2 = -ratio_M * r1/r2*dw1
                                        
                    else:
                        # choc glissant
                        dw1 = 2.5/r1 * coef_frot * ve
                        
                        if wca > 0:
                            WCa_uni = nml
                        else:
                            dw1 *= -1
                            WCa_uni = -nml
                        
                        dV1 -= ve*coef_frot*WCa_uni
                                                    
                        self.w += dw1
                
                self._v_ += dV1 
                #autr._v_ -= ratio_M*dV1
                                    
                NumBloquants += 1
                
            else:
                
                if autr._TacheTour != EtatCollision.Debut and self in autr._emboutis_:
                    # Evitons un autre echange d'impulsion si a ce tour le cas autre->self a deja ete traite
                    # et resulte en un rebond mou de self sur autr etant donnee une plus grande impulsion de autr.
                    continue
                                        
                v2 = axe * autr._v_
                        
                if autr._inobstrue_: #not( autr._coince_ or abs(v1) < abs(v2) and v1 * v2 >= 0 ):
                    
                    if v1 * v2 < 0:
                        autr._inobstrue_ = False
                    
                    #if abs(v2) > abs(v1) and v2*v1 > 0:
                    #    raise SousEnsembleException( [self, autr], "%s rattrape %s qui va plus vite !?"%(self, autr))
                    
                    ratio_M = self.masse_/autr.masse_
                        
                    coef_elas = self.elas * autr.elas/(self.elas + autr.elas)
                    
                    k = (1 + coef_elas)/(1+ratio_M)
                    
                    ve = k * (v2-v1)
                                    
                    coef_frot = min(self.frot, autr.frot)
                    dV1 = ve*axe
                    
                    if coef_frot:
                        w1 = self.w
                        w2 = autr.w
                        r1 = self.rayon
                        r2 = autr.rayon
                        V1 = self._v_
                        V2 = autr._v_
                        dV = V1-V2
                            
                        wca = dV * nml - r1 * w1 - r2 * w2
                        if wca == 0 or 1 < 3.5 * coef_frot * (1+ ratio_M) * ve / abs(wca):
                            # roulement                        
                            if wca != 0:
                                # adherence pendant le choc
                                dV1 -= 2/7.*wca*nml / (1+ratio_M)
                                dw1 = 5./7/r1*wca / (1+ratio_M)
                                self.w += dw1
                                autr.w -= ratio_M * r1/r2 * dw1
                                            
                        else:
                            # choc glissant
                            dw1 = 2.5 / r1 * coef_frot * ve
                            
                            if wca > 0:
                                WCa_uni =  nml
                            else:
                                dw1 *= -1
                                WCa_uni = -nml
                            
                            dV1 -= ve*coef_frot*WCa_uni
                                                        
                            self.w += dw1
                            autr.w -= ratio_M * r1/r2 * dw1
                    
                    self._v_ += dV1 
                    autr._v_ -= ratio_M*dV1
                
                else:
                    # L'unite devant va plus vite selon l'axe de collision et pourtant on la rattrape:
                    # cela veut dire qu'elle est coincee.
                    
                    m1 = self.masse_
                    m2 = autr.masse_
                    
                    if v2 * v1 < 0:
                        # les deux mobiles vont dans des directions opposes.
                        if abs(v2) > abs(v1):
                            # l'impulsion nette passe a self
                            dp =  m2 * v2 * .7
                        else:
                            # l'impulsion nette passe a autr
                            dp =  -m1 * v1 * .7
                                                
                        if autr._coince_ > 0:
                            AutrBloquants.append(autr)
                            NumBloquants += 1
                        
                    else:
                        # Transmission de l'impulsion de self vers autr
                        dp = -m1 * v1
                        if autr._coince_ > 0:
                            #Transmission d'une plus grande partie si l'autre unite est bloquee.
                            dp *= .6
                            AutrBloquants.append(autr)
                            NumBloquants += 1
                        else:
                            dp *= .5
                    
                    
                    # echange d'impulsion                 
                    self._v_ += axe * dp / m1
                    autr._v_ -= axe * dp / m2
        
                autr._emboutisseurs_.append( self )
        
        if NumBloquants >= 2:
            v_residu = self._v_ * v_orig_dir
            
            if v_residu > 0.:
                
                if self._coince_ == 0:
                    v_residu /= 2.
                    
                dp_residu = v_orig_dir * v_residu * self.masse_ / NumBloquants
                
                for autr in AutrBloquants:
                    autr._v_ += dp_residu / autr.masse_
                    
                self._v_ -= v_residu * v_orig_dir 
        
                
        if NumObstacles == 1 and getattr(autr,'_coince_',0) > 0:
            #v_radiale = Axe * self._v_ 
            #if v_radiale <= 1.:#Tolerance - le seuil en theorie pourrait etre 0
                ## Glissiere
                self._glissiere_ = nml, autr 
