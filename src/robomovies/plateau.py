from tricarto.tryangle import Reseau, toPointList, sommet
from rosebotnav.formation import Camp
from tricarto.tryangle.sommet import SommetBorne, Vec, Connecte
import rosebotnav


class Partie(rosebotnav.racine.Partie):

    ROBOT_CLASS = rosebotnav.robot.Robot

    def peupler(self, vide=True, fond=True):

        self.graphe = Reseau(nom="robomovies", exact=False)

        # Sommets ajoutes pour initialiser le graphe avec un triangle de sorte que les autres puissent etre ajoutes
        # incrementalement
        marge = 1000
        cadreExterieur = [(-marge ,-marge), ( 2000 +marge ,-marge), ( 2000 +marge , 3000 +marge), (-marge, 3000+ marge), ]

        aireDepart = [(0, 778), (400, 778), (400, 800), (70, 800), (70, 1200), (400, 1200), (400, 1222), (0, 1222)]

        coords = [(0, 0), ] + \
                 aireDepart + \
                 [(0, 2000), ] + \
                 [(1200, 2000), (1200, 1900), (1800, 1900), (1800, 2000)] + \
                 [(3000, 2000), ] + \
                 [(3000 - a, b) for a, b in reversed(aireDepart)] + \
                 [(3000, 0)] + \
                 [(2033, 0), (2033, 580), (967, 580), (967, 0)]

        coords = [(b, a) for a, b in coords]
        Bornes = self.graphe.AddVertices(toPointList(cadreExterieur + coords))
        for Borne in Bornes:
            self.graphe.VertexToWertex[Borne] = SommetBorne(Borne, reseau=self.graphe)

        Connecte(Bornes[:4], cloture=True)
        Connecte(Bornes[4:], cloture=True)

        self.graphe.ResetDual()  # searchDual.resetConvexAreas()

        couleurJaune = (255, 255, 0)
        couleurVert = (0, 255, 0)

        CampJaune = Camp.getCamp('Jaune', couleur=couleurJaune)
        CampVert = Camp.getCamp('Vert', couleur=couleurVert)
        CampGob = Camp.getCamp('Gobelet', couleur=(200, 200, 200))
        CampPiedJaune = Camp.getCamp('PiedJaune', couleur=couleurJaune)
        CampPiedVert = Camp.getCamp('PiedVert', couleur=couleurVert)

        # Robot camp jaune
        roboJaune = self.ROBOT_CLASS(Reseau=self.graphe, pos=(1000, 300), Orient0=Vec(0, 1), camp=CampJaune, nom="Robot Jaune")

        sommet.Wertex.surrayon = 1

        self.Selection = set([roboJaune])

        if not vide:

            # Robot camp vert
            AjouteUnite(self.graphe, (1000, 2700), Orient0=Vec(0, -1), camp=CampVert, nom="Robot Vert")

            Gobelets = (910, 800), (2090, 800), (250, 1750), (1500, 1650), (2750, 1750)
            for a, b in Gobelets:
                AjouteUnite(self.graphe, (b, a), camp=CampGob, taille=48)

            Pieds = [(90, 200), (90, 1750), (90, 1850), (1100, 1770), (850, 100), (850, 200), (870, 1355), (1300, 1400)]

            for a, b in Pieds:
                AjouteUnite(self.graphe, (b, a), camp=CampPiedJaune, taille=30)
                AjouteUnite(self.graphe, (b, 3000 - a), camp=CampPiedVert, taille=30)

        sommet.Wertex.surrayon = 1

        # self.graphe.CheckMesh()

        if self.Affiche:

            self.DimEcran = Vec(500, 680)

            if fond:
                self.image_fond = 'robomovies.jpg'
                self.image_echelle = 2
                self.image_pos[0] = -30
                self.image_pos[1] = -35

            self.VueOrg = Vec(-100, 3100)
            self.VueRatio = .2
            self.VueLimites = Vec(-100, -100), Vec(2100,
                                                   3100)  # Vec(xmin, ymin), Vec(xmax, ymax) # coordonnees reeles de l'aire visible si elle est limitee
