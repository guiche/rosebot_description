## ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup
import os

os.environ['DISTUTILS_DEBUG'] = "1"

# fetch values from package.xml
setup_args = generate_distutils_setup(
	packages=['rosebotnav', 'pygapp', 'tricarto'], #setuptools.find_packages(),
 	package_dir={'':'src'},	
	requires=['rospy'],
	#include_package_data=True,
    package_data={'rosebotnav': ['sauvegardes/*', 'media/images/*', 'media/fonts/*'],},
)

setup(**setup_args)
