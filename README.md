# Lisez-moi #


### Description ###

Code de navigation du robot Rosebot sous forme de paquet ROS.
Contient du code python et du code c++ reliés par le code d'interfaçage boost::python

### Dépendences ###

- python 2 (>=2.7)
- c++ 11
- boost python (boost >= 1.49)
- ROS (version indigo)

optionnel :
- pygame (pour la partie commande graphique)

### Installation ROS et dépendances ###

Instructions :
(pour carte Intel Edison)
http://wiki.ros.org/indigo/Installation/Debian 

libs:
orocos_kdl dépend de eigen3 :
apt-get install libeigen3-dev

installer SIP et KDL
Suivre instructions la
http://movingthelamppost.com/blog/html/2013/07/12/installing_pyqt____because_it_s_too_good_for_pip_or_easy_install_.html

Puis cmake dans les python bindings git clone de la :
http://www.orocos.org/kdl/installation-manual#toc5

```
#!shell


rosinstall_generator geometry_msgs nav_msgs std_msgs sensor_msgs rosout rosbag orocos_kdl tf angles tf2 tf2_ros tf2_msgs actionlib --rosdistro indigo --deps --wet-only --exclude roslisp --tar > indigo-custom_ros.rosinstall

```

Problèmes de dépendances rencontrés sur ubuntu 14.04 résolus par l'installation de kdl, tf :
```
#!shell

sudo apt-get install ros-indigo-python-orocos-kdl
sudo apt-get install ros-indigo-tf
sudo apt-get install python-pygame

```


### Installation ###

- Créer répertoire de travail catkin <catkin> (voir doc d'installation de ROS)
- copier le paquet  rosebot_description dans <catkin>/src/
- compiler le paquet ROS en faisant :

```
#!sh

<catkin> catkin_make 
<catkin> source ./devel/setup.bash #mise à jour des déclaration ROS
```

dans ~/.bashrc du PC, ajouter l'adresse d'Edison


```
#!shell

export ROS_MASTER_URI=http://192.168.2.12:11311
export ROS_IP=`hostname -I`

```
- rendre tous les scripts python (dans ./src) executables

### Lancement ###

Sur la carte du robot (pour faire tourner le noeud moteur, le noeud de nav et pas de minicarte de commande):

```
#!sh

roslaunch rosebot_description rosebot.launch
```

Sur le PC pour tester avec la simulation par minicarte (et envoyer des commandes d'objectif) :
```
#!sh

roslaunch rosebot_description carte_cmd.launch

```