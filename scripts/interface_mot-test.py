#!/usr/bin/env python
#

## Simple talker demo that listens to motor board messages and publish them on ROS workspace 

#rosservice call /change_state_rose_bot 0 'true'
#  true to enable the motor control, false to disable it
# 0 to do nothin, 3 to reset the odometry, 8 to resend servoing parameters

import signal

from termcolor import colored

import tf
import rospy

import serial
import time
# import numpy

from std_msgs.msg import String
from geometry_msgs.msg import Twist, Point, Quaternion
from nav_msgs.msg import Odometry
import PyKDL as kdl

from ctypes import cast, pointer, c_int, POINTER, c_float
import struct
from rosebot_description.srv import ChangeState, ChangeColor

from dynamic_reconfigure.server import Server
from rosebot_description.cfg import ServoingConfig


TIME_STAMP = None
TIME_DELTA = 0.02 #seconds   #If any new Twist message comes before this waiting time, the message will be trashed.


#CHANGE_COLOR_SERVICE = 'change_color'

# Initializing the port


class DriverNode():

	


	def receive(self):
		while True:
			try:
				
				l = self.ser.readline()
				
				if (len(l)<3): raise NameError('too short')
				elif l[0] == 'D' and l[1] == 'e':
					print "debug=", l   
				else :					
					#print "line=", l   
					pass			  
				self.verifyCs_(l)
				self.interpretedData(l[2:])
				

			except (RuntimeError, TypeError, NameError, ValueError) as e:
				# import traceback
				# traceback.print_exc()
				# print e
				return

	def verifyCs_(self,buff):
		if (len(buff)<3): raise NameError('too short')
		readCs = int(buff[0:2],16)
		cs = 0
		for c in buff[2:-1]:	#rm '/n'
			cs = cs + ord(c)
		cs = cs % 256
		if cs != readCs:

			raise NameError('wrong cs ' + str(cs) + " != " + str(readCs))


	def interpretedData(self,data):
		
		cmd = data[0]
		if cmd == 'O':
			array = data[1:].split(";")
			if len(array) >= 3:
				#pos = (hexToFloat(array[0]), hexToFloat(array[1]))
				#orient = hexToFloat(array[2])
				
				self.odom_linear_x = self.hexToFloat(array[0])
				self.odom_linear_y = self.hexToFloat(array[1])
				self.odom_angular_theta = self.hexToFloat(array[2])

				#print "pos=", pos, "orient=", orient
				#print "pos=", self.odom_linear_x, "orient=", self.odom_linear_y

			if len(array) >= 5:
				distanceSpeed = self.hexToFloat(array[3])
				angleSpeed = self.hexToFloat(array[4])
				
				self.odom_linear_x_speed = distanceSpeed
				self.odom_angular_theta_speed = angleSpeed

				#print "distanceSpeed=", distanceSpeed, "angleSpeed=", angleSpeed

			rospy.loginfo("Publish odom")
			
			

			msg = Odometry()
			msg.header.stamp = rospy.Time.now()
			msg.header.frame_id = 'odom' # i.e. '/odom'
			msg.child_frame_id = 'map' # i.e. '/base_footprint'
			#prepare the odom message
			msg.header.stamp = rospy.Time.now()
			msg.pose.pose.position.x = self.odom_linear_x
			msg.pose.pose.position.y = self.odom_linear_y
			msg.pose.pose.orientation = Quaternion(*(kdl.Rotation.RPY(0, 0, self.odom_angular_theta).GetQuaternion()))

			msg.twist.twist.linear.x = self.odom_linear_x_speed
			msg.twist.twist.linear.y = self.odom_linear_y_speed
			msg.twist.twist.angular.z = self.odom_angular_theta_speed


			## publish the tf for rviz and further debug
			tf_br.sendTransform((self.odom_linear_x, self.odom_linear_y, 0),			 (msg.pose.pose.orientation.x,msg.pose.pose.orientation.y,msg.pose.pose.orientation.z,msg.pose.pose.orientation.w),
								rospy.Time.now(),
								"RoseBot",
								"map")

			self.pub_odom.publish(msg) #publish the odom message



	def hexToFloat(self,s):
		i = int(s, 16)				   # convert from hex to a Python int
		cp = pointer(c_int(i))		   # make this into a c integer
		fp = cast(cp, POINTER(c_float))  # cast the int pointer to a float pointer
		return fp.contents.value		 # dereference the pointer, get the float

	def hexToInt(self,s):
		return int(s, 16)

	def floatToHex(self,f):
		return ''.join('%.2x' % ord(c) for c in struct.pack('>f', f))

	def intToHex(self,i):
		s = "%X" % i


	def computeCs(self,buff):
		cs = 0
		for c in buff:	#rm '/n'
			cs = cs + ord(c)
		cs = cs % 256
		return '%.2x' % cs


	# def sendTtwist(self, Twist):
	#	 self.ser.write('T'+str(int(Twist*10000.0))[0:6]+'!')
	#print "sent"+str(int(Twist*10000))
	# def sendVtwist(self, Vtwist):
	#	 self.ser.write('V'+str(int(Vtwist*10000.0))[0:6]+'!')

	def sendTtwist(self, Twist):
		cmd = 'T' + self.floatToHex(Twist)
		cs = self.computeCs(cmd)
		self.ser.write(cs+cmd+'\n')

	def sendVtwist(self, Vtwist):
		cmd = 'V' + self.floatToHex(Vtwist)
		cs = self.computeCs(cmd)
		self.ser.write(cs+cmd+'\n')
		
	def setPIDLorder(self, kp, ki, kd):		
		#self.vparams.setPIDParams(kp, ki, kd)
		cmd = '{' + self.floatToHex(kp) + ';' + self.floatToHex(ki) + ';' + self.floatToHex(kd)
		cs = self.computeCs(cmd)
		self.ser.write(cs+cmd+'\n')
		rospy.loginfo("PID Linear Coef Sent")
		
	def setPIDAorder(self, kp, ki, kd):		
		#self.wparams.setPIDParams(kp, ki, kd)
		cmd = '(' + self.floatToHex(kp) + ';' + self.floatToHex(ki) + ';' + self.floatToHex(kd)
		cs = self.computeCs(cmd)
		self.ser.write(cs+cmd+'\n')
		rospy.loginfo("PID Angular Coef Sent")	  


	def enablePower(self):
		cmd = 'U' + self.floatToHex(float(1))
		cs = self.computeCs(cmd)
		self.ser.write(cs+cmd+'\n')		
		rospy.loginfo("Robot Start")

		
	def disablePower(self):
		cmd = 'U' + self.floatToHex(float(0))
		cs = self.computeCs(cmd)
		self.ser.write(cs+cmd+'\n')		
		rospy.loginfo("Robot Stop")
		
	def sendReset(self):
		cmd = 'S' + self.floatToHex(float(0))
		cs = self.computeCs(cmd)
		self.ser.write(cs+cmd+'\n')
		self.ser.write(cs+cmd+'\n')
		rospy.loginfo("Reset Robot Servoing parameters")
		
	def sendStartCoord(self, X, Y, Theta):
		cmd = 'Z' + self.floatToHex(X) + ';' + self.floatToHex(Y) + ';' + self.floatToHex(Theta)
		cs = self.computeCs(cmd)
		self.ser.write(cs+cmd+'\n')
		rospy.loginfo("Sent Starting Coord")
		
		

	incomming_message_type = 0
	nbr_incom_char = 0
	sign = False
	inputString = ''
	message_started = False

	current_start_stop_state = False
	
# 
#	 def handleStateChange(self, req):
# 
#		 if(req.start_stop):
#			 self.enablePower()
# 
#		 else:
#			 self.disablePower()
#			 
#		 print req.state
#		 
#		 if(req.state == 3):
#			 self.sendReset()
# 
#		 if(req.state == 8):
#			 gainsPL = rospy.get_param('~gainsPL')
#			 gainsPA = rospy.get_param('~gainsPA')
#			 self.setPIDLorder(gainsPL['p'],gainsPL['i'],gainsPL['d'])
#			 self.setPIDAorder(gainsPA['p'],gainsPA['i'],gainsPA['d'])		


	#req.state
	#req.start_stop
	#	return True

	def handleData(self, c):
		pass


	odom_linear_x = 0.0
	odom_linear_y = 0.0
	odom_angular_theta = 0.0

	odom_linear_x_speed = 0.0
	odom_linear_y_speed = 0.0
	odom_angular_theta_speed = 0.0

	twist_x_speed = 0.0
	twist_theta_speed = 0.0

	sys_rdy = False
	
	
	
	def callbackDynamicParam(self, config, level):
		#rospy.loginfo("""Reconfiugre Request: {int_param}, {double_param},\ {str_param}, {bool_param}, {size}""".format(**config))
		#print config
				
		if (config['reset_odom'] == True):
			self.sendReset()	
				
		if (config['motor_ctrl'] == True):
			self.enablePower()
		else :				
			self.disablePower()		
							
		self.setPIDLorder(config['KpPoLi'],config['KiPoLi'],config['KdPoLi'])
		self.setPIDAorder(config['KpPoAng'],config['KiPoAng'],config['KdPoAng'])
			
		return config

	

	#function to be called when a twist message is available
	def callbackTwist(self, data):

		global TIME_STAMP
		self.twist_x_speed = data.linear.x
		self.twist_theta_speed = data.angular.z

		if not TIME_STAMP :
			TIME_STAMP = rospy.Time().now().to_sec()
			return
			
		delta = rospy.Time().now().to_sec() - TIME_STAMP

		if delta < TIME_DELTA:
			pass
		else:
			self.sendVtwist(self.twist_x_speed)
			self.sendTtwist(self.twist_theta_speed)
			TIME_STAMP = rospy.Time().now().to_sec()


	def stop_node_handler(self, *args, **kwargs):
		self.running = False
		rospy.signal_shutdown("Shuting down the node on Ctrl+C")
		self.ser.flush()
		self.ser.close()
		
	def handleChangeColor(self, req):
		""" Changement de couleur"""
		rospy.loginfo("================= Change color =================")
		if req.team_color == 0:
			coord = rospy.get_param('~coordVert')
			self.sendStartCoord(coord['X'], coord['Y'], coord['Theta'])		
			time.sleep(0.2)
			#self.sendStartCoord(0.850, 2.770, 4.71238)
			rospy.loginfo("Send Green")
			
		elif req.team_color == 1:
			coord = rospy.get_param('~coordViolet')
			self.sendStartCoord(coord['X'], coord['Y'], coord['Theta'])
			time.sleep(0.2)
			#self.sendStartCoord(0.850, 0.230, 1.57079)
			rospy.loginfo("Send Purple")
			#send Purple 
		return True


	def __init__(self):

		self.running = False
		signal.signal(signal.SIGINT, self.stop_node_handler)
		signal.signal(signal.SIGTERM, self.stop_node_handler)

		## init_node
		rospy.loginfo("Launching motor board driver node")
		rospy.init_node('motor_driver', anonymous=True, disable_signals=True)
		
		## load node params from roslaunch params 
		rate_param = rospy.get_param('~rate', 70)
		serial_port = rospy.get_param('~port', '/dev/ttyACM0')
		serial_baud = rospy.get_param('~baud', 230400)

		change_color_service_name = rospy.get_param('~change_color_service_name', 'change_color')
		rospy.Service(change_color_service_name, ChangeColor, self.handleChangeColor)
		
		#_s = rospy.Service('change_state_rose_bot', ChangeState, self.handleStateChange)
		
		rate = rospy.Rate(rate_param)
					   
		self.ser = serial.Serial(
			port=serial_port,
			baudrate=serial_baud,
			xonxoff=False,
			timeout=1
		)	
		
		rospy.loginfo("Openning SerialCom... ")

		self.ser.close()
		self.ser.open()
		self.ser.flush()
		#rospy.loginfo("Port Open  -- Soft Reseting")
		#self.sendReset()
		time.sleep(0.5)			
	
		
		self.pub_odom = rospy.Publisher('odom', Odometry, queue_size=10)	
		rospy.Subscriber('cmd_vel', Twist, self.callbackTwist)
						
		tf_br = tf.TransformBroadcaster()
		
		global TIME_STAMP
		TIME_STAMP = rospy.Time().now().to_sec()

		

		_srv = Server(ServoingConfig, self.callbackDynamicParam)
		
		

		#rospy.loginfo("Port Open  -- Sending Coeff")		
		#full_param_name = rospy.search_param('gainsPL')   ##search path of a parameter by its name
		#param_value = rospy.get_param(full_param_name)
		#gainsPL = rospy.get_param('~gainsPL')
		#gainsPA = rospy.get_param('~gainsPA')
				
		#
		#self.setPIDLorder(gainsPL['p'],gainsPL['i'],gainsPL['d'])
		#self.setPIDAorder(gainsPA['p'],gainsPA['i'],gainsPA['d'])
		
		#self.sendStartCoord(0.850, 2.770, 4.71238) #green
		rospy.loginfo("Port Open  -- Sending Start Coordinates")
		self.sendStartCoord(0.850, 0.330, 1.57079) #violet
			
	
		self.sys_rdy = True
		self.running = True
		

		while self.running:

			#print "=================================="
			#self.sendTtwist(0.5)
			#self.sendVtwist(0.2)

			#self.receive()

			
			rate.sleep()  # free the cpu



	


if __name__ == '__main__':
	try:
		ne = DriverNode()
	except rospy.ROSInterruptException: pass
