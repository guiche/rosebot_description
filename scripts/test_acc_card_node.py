#!/usr/bin/env python

# Interface with arduino card
# - IR rangefinder sensors
# - binary switch
# - starter
# - servos

import signal
import rospy
import time
import mutex

#import IOBoardComm_v1_debug as IOBoardComm_v1
import IOBoardComm_v1

from std_msgs.msg import Int16
from rosebot_description.srv import SendCommandServo


import yaml

MAX_REQUEST_NUMBER=1

class DriverNode:

    # THIS METHOD IS PEP8 COMPATIBLE !!!
    def check_starter_and_color(self):
        # get color switch state :
        color = self.io_board_comm.readINPin(self.ports['COLOR_SWITCH_PIN'])
        if color is not None:
            self.interpretColorSwitchState(color)

        # get starter state :
        starter_state = self.io_board_comm.readINPin(self.ports['STARTER_PIN'])
        if starter_state is not None:
            self.interpretStarterState(starter_state)

    def check_ir_sensors(self):
        ir_fl = self.io_board_comm.readINPin(self.ports['IR_FRONT_LEFT_PIN'])
        ir_fr = self.io_board_comm.readINPin(self.ports['IR_FRONT_RIGHT_PIN'])
        ir_rc = self.io_board_comm.readINPin(self.ports['IR_REAR_CENTER_PIN'])

        if ir_fl is None and ir_fr is None and ir_rc is None:
            rospy.logerr("No IR values received!")
            return

        ir_fl = 0 if ir_fl is None else ir_fl
        ir_fr = 0 if ir_fr is None else ir_fr
        ir_rc = 0 if ir_rc is None else ir_rc
        self.publishIRData(ir_fl, ir_fr, ir_rc)

    def interpretColorSwitchState(self, data):
        if data == 0:
            if self.verbose:
                rospy.loginfo("Color switch state is 0")
            if self.color_switch_state != 0:
                self.color_switch_state = 0
        elif data == 1:
            if self.verbose:
                rospy.loginfo("Color switch state is 1")
            if self.color_switch_state != 1:
                self.color_switch_state = 1
        else:
            rospy.logerr("Color switch state is not recognized")

    def interpretStarterState(self, data):
        if data == 0:
            if self.verbose:
                rospy.loginfo("Starter state is 'inside'")
        elif data == 1:
            if self.verbose:
                rospy.loginfo("Starter state is 'outside'")
        else:
            rospy.logerr("Starter state is not recognized")


    def publishIRData(self, d1, d2, d3):
        if self.verbose:
            rospy.loginfo("IR sensors : {}, {}, {}".format(d1, d2, d3))
        self.ir_publisher_fl.publish(Int16(d1))
        self.ir_publisher_fr.publish(Int16(d2))
        self.ir_publisher_rc.publish(Int16(d3))

    def setup_ir_calibration_data(self, ir_calib_file):
        self.ir_calib_data = load_yaml(ir_calib_file)
        if len(self.ir_calib_data) == 0:
            rospy.loginfo("Setup default calibration")
            self.ir_calib_data = {
                'front_left':
                    {'distance': [999, 0.35, 0.3, 0.25, 0.2, 0.15, 0.1],
                      'signal': [30, 40, 75, 92, 120, 140, 205]},
                'front_right':
                    {'distance': [999, 0.35, 0.3, 0.25, 0.2, 0.15, 0.1],
                     'signal': [30, 40, 75, 92, 120, 140, 205]},
                'rear_center':
                    {'distance': [999, 0.35, 0.3, 0.25, 0.2, 0.15, 0.1],
                     'signal': [30, 40, 75, 92, 120, 140, 205]}
            }
        # print self.ir_calib_data

    def setup_ports(self, ac_ports_file):
        self.ports = load_yaml(ac_ports_file)
        if len(self.ports) == 0:
            rospy.loginfo("Take default port configuration")
            self.ports['COLOR_SWITCH_PIN'] = 4
            self.ports['STARTER_PIN'] = 7
            self.ports['ARM_PIN'] = [3, 55, 180]
            self.ports['LATCH_LEFT_PIN'] = [5, 5, 130]
            self.ports['LATCH_RIGHT_PIN'] = [6, 30, 165]
            self.ports['FUNNY_ACTION_PIN'] = 9
            self.ports['IR_FRONT_LEFT_PIN'] = 14
            self.ports['IR_FRONT_RIGHT_PIN'] = 15
            self.ports['IR_REAR_CENTER_PIN'] = 16
            self.ports['BATTERY_PIN'] = 17

        rospy.loginfo("Ports : {}".format(self.ports))

    def configure_pins(self):

        self.io_board_comm.enterConfigMode()
        self.io_board_comm.setPinPurpose(self.ports['COLOR_SWITCH_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['STARTER_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['ARM_PIN'][0], IOBoardComm_v1.SERVOMSG)
        self.io_board_comm.setPinPurpose(self.ports['LATCH_LEFT_PIN'][0], IOBoardComm_v1.SERVOMSG)
        self.io_board_comm.setPinPurpose(self.ports['LATCH_RIGHT_PIN'][0], IOBoardComm_v1.SERVOMSG)
        self.io_board_comm.setPinPurpose(self.ports['IR_FRONT_LEFT_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['IR_FRONT_RIGHT_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['IR_REAR_CENTER_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['FUNNY_ACTION_PIN'], IOBoardComm_v1.SERVOMSG)
        self.io_board_comm.exitConfigMode()

    def stop_node_handler(self, *args, **kwargs):
        self.running = False
        rospy.signal_shutdown("Shutdown the node")

    def send_command_servo(self, req):
        """
        Callback to handle 'Send a command to a servo'.
        argument req containts
        :param pin_nb: pin to command
        :param value_servo: value of the command
        :return status: status of the command : true if send, false otherwise
        """

        while not self.mutexLock.testandset():
            time.sleep(0.001)
            if self.verbose:
                print "Wait for the mutex in service"


        pin_nb, value_servo = req.pin_nb, req.value_servo
        status = False
        servo_pin = [self.ports['ARM_PIN'][0],
                     self.ports['LATCH_LEFT_PIN'][0],
                     self.ports['LATCH_RIGHT_PIN'][0],
                     self.ports['FUNNY_ACTION_PIN'],
                     # composed pins :
                     self.ports['LATCH_LEFT_PIN'][0] + self.ports['LATCH_RIGHT_PIN'][0] # = 11 means both latches
                     ]

        if pin_nb not in servo_pin:
            rospy.logwarn("Pin number asked ({0}) is not a servo (servo pin : {1})".format(pin_nb, servo_pin))
        else:

            if pin_nb < 10:
                # unit pin action
                status = self.io_board_comm.setServoPos(pin_nb, value_servo)
            elif pin_nb == 11:
                print "DOUBLE ACTION"
                def y(x, xmin, xmax, ymin, ymax):
                    return int((x-xmin)*1.0/(xmax-xmin)*(ymin-ymax)) + ymax
                value_servo_left = value_servo
                value_servo_right = y(value_servo,
                                      self.ports['LATCH_LEFT_PIN'][1],
                                      self.ports['LATCH_LEFT_PIN'][2],
                                      self.ports['LATCH_RIGHT_PIN'][1],
                                      self.ports['LATCH_RIGHT_PIN'][2])

                print "CALL LEFT : ", self.ports['LATCH_LEFT_PIN'][0], value_servo_left
                status = self.io_board_comm.setServoPos(self.ports['LATCH_LEFT_PIN'][0], value_servo_left)
                print "CALL RIGHT : ", self.ports['LATCH_RIGHT_PIN'][0], value_servo_right
                status &= self.io_board_comm.setServoPos(self.ports['LATCH_RIGHT_PIN'][0], value_servo_right)


            if status:
                rospy.loginfo("set {0} on pin {1}".format(value_servo, pin_nb))
            else:
                rospy.logerr("Error occured setting servo position")
        self.mutexLock.unlock()

        return status

    def __init__(self):

        self.running = False
        signal.signal(signal.SIGINT, self.stop_node_handler)

        rospy.loginfo("Launching accessory card driver node")
        rospy.init_node('acc_card_driver', anonymous=True, disable_signals=True)

        rate = rospy.get_param('~rate', 20)
        serial_port = rospy.get_param('~port', '/dev/ttyACM0')
        serial_baud = rospy.get_param('~baud', 115200)

        ir_signal_topic = rospy.get_param('~ir_signal_topic', 'ir_signal')

        ir_calib_file = rospy.get_param('~ir_calib_file', '')
        ac_ports_file = rospy.get_param('~ac_ports_file', '')

        # Define initial states:
        self.color_switch_state = None
        self.starter_state = None

        self.verbose = rospy.get_param('~verbose', False)

        # load serial ports conf:
        self.setup_ports(ac_ports_file)

        # load ir calibration data:
        self.setup_ir_calibration_data(ir_calib_file)

        rospy.loginfo("Ready to listen service")
        rate = rospy.Rate(rate)

        rospy.loginfo("Openning port ...")
        self.io_board_comm = IOBoardComm_v1.IOBoardComm()
        if not self.io_board_comm.initBoard(serial_port, serial_baud):
            rospy.logerr("Port is not open")
            return

        self.configure_pins()

        send_command_servo_service_name = rospy.get_param('~send_command_servo_service_name', 'send_command_servo')
        rospy.Service(send_command_servo_service_name, SendCommandServo, self.send_command_servo)


        # configure ir publisher
        self.ir_publisher_fl = rospy.Publisher(ir_signal_topic + '_fl', Int16, queue_size=10)
        self.ir_publisher_fr = rospy.Publisher(ir_signal_topic + '_fr', Int16, queue_size=10)
        self.ir_publisher_rc = rospy.Publisher(ir_signal_topic + '_rc', Int16, queue_size=10)

        self.running = True

        self.mutexLock = mutex.mutex()

        rospy.loginfo("Start running")
        while self.running:
            debug_start = time.time()
            if self.verbose:
                rospy.loginfo("==================================")

            if self.mutexLock.testandset():
                self.check_starter_and_color()
                self.check_ir_sensors()
                self.mutexLock.unlock()
            rate.sleep()  # free the cpu
            debug_elapsed = time.time() - debug_start
            if self.verbose:
                print "Frame duration =", debug_elapsed


        self.io_board_comm = None

def load_yaml(filename):
    try:
        f = open(filename)
        data = yaml.safe_load(f)
        f.close()
        return data
    except IOError:
        rospy.logerr("Failed to load file : {}".format(filename))
        return {}


if __name__ == '__main__':
    try:
        ne = DriverNode()
    except rospy.ROSInterruptException:
        pass
