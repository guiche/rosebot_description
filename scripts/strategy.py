
import json
from tricarto import angleToVec
import math
import time
from threading import RLock, Timer
import copy

TIMEOUT = 15


class ActionMutex:
    _mutex = RLock()

    def __enter__(self):
        self._mutex.acquire()

    def __exit__(self, type, value, traceback):
        self._mutex.release()



def flipSidePos(pos):
    return pos[0], 3000 - pos[1]


def flipSideOrient(orient):
    if orient is None:
        return None
    elif isinstance(orient, (float, int)):
        return -orient
    else:
        return orient[0], -orient[1]


class ActionItem:
    pass


class ActuatorItem(ActionItem):
    def __init__(self, pin_id, value, **kwargs):
        self.pin = pin_id
        self.value = value
        self.kwargs = kwargs


class PositionItem(ActionItem):

    def __init__(self, x, y, angle, **kwargs):
        self.x = x
        self.y = y
        self.angle = angle
        self.kwargs = kwargs


class ActionSequence(object):

    def __init__(self, name, action_items, timeout=TIMEOUT):

        self.name = name
        self.items = action_items
        self.posIdx = 0
        self.timeout = timeout# timeout for action sequence, in seconds
        self.timeStart = None


    def addActionItems(self, *items):
        self.items.extend(*items)

    def __str__(self):
        return self.name

    # TODO
    def incrementItem(self, interfaceMobile, send_command_servo_service, onTerminationFunc=None):

        if self.timeStart is None:
            self.timeStart = time.time()

        if self.posIdx < len(self.items):
            # appel recursif d'incrementation
            def onObjFunc():
                return self.incrementItem(interfaceMobile, send_command_servo_service, onTerminationFunc=onTerminationFunc)

            if self.posIdx == 0:
                print '\nDebut action :', self.name

            item = self.items[self.posIdx]

            if isinstance(item, PositionItem):

                # coords, orient = args[:2]
                coords = [item.x, item.y]
                orient = item.angle

                if isinstance(orient, (float, int)):
                    orient = angleToVec(orient)

                kwargs = item.kwargs
                # if len(args) > 2:
                #     kwargs = args[2]

                print '     vers position', self.posIdx, ':', coords, orient, item.kwargs
                interfaceMobile.AssBut(coords, orient=orient, ajout=False, rappelSurObjectif=onObjFunc, **kwargs)
                self.posIdx += 1

            elif isinstance(item, ActuatorItem):

                print "     Actuator :", item.pin, item.value
                if send_command_servo_service is None:
                    print "ERROR : send_command_servo_service is NONE !!!! PRoUT PRoUT"
                else:
                    # call send_command_servo_service(pin_id, value)
                    if not send_command_servo_service(item.pin, item.value):
                        print "ERROR : Call service failed"

                # Need to increment index because of blocking method while calling service
                self.posIdx += 1
                onObjFunc()


        else:

            if self.isSuccessful():
                print '     action', self.name, 'terminee en %.1f'%(time.time() - self.timeStart)

                if onTerminationFunc:
                    onTerminationFunc()

    def isSuccessful(self):
        return self.posIdx == len(self.items)

    def abort(self):
        self.posIdx = len(self.items) +1

    def flipSide(self):

        for item in self.items:
            if isinstance(item, PositionItem):

                item.x, item.y = flipSidePos([item.x, item.y])
                item.angle = flipSideOrient(item.angle)


    def draw(self, interfaceDraw):

        from tricarto import Vec
        couleur = 0,200,0

        for idx, item in enumerate(self.items):
            if isinstance(item, PositionItem):
                # coords, orient = pos2D[:2]
                coords = [item.x, item.y]
                orient = item.angle

                interfaceDraw.dessineCercle(coords, rayon=150, trait=1, couleur=couleur)

                if orient:

                    if isinstance(orient, (float, int)):
                        orient = angleToVec(orient)

                    orient.nor = 200
                    interfaceDraw.dessineLigne(coords, orient + coords, couleur=couleur)

                etiquette = str(idx)

                interfaceDraw.afficheParatexte(etiquette, Vec(20,20) + coords, couleur=couleur, coordsReeles=True)



class Strategy(object):

    def __init__(self, *actions):
        self.actions = [copy.deepcopy(action) for action in actions]
        self.actionIdx = 0
        self.timeStart = None
        self.send_command_servo_service = None


    def saveToFile(self, cheminFichier):
        with open(cheminFichier) as f:
            json.dump(self, f)

    def currentAction(self):
        if self.actionIdx:
            return self.actions[self.actionIdx-1]

    def nextAction(self):
        if self.actionIdx < len(self.actions):
            return self.actions[self.actionIdx]

    def incrementPos(self, interfaceMobile):

        with ActionMutex():

            if self.timeStart is None:
                self.timeStart = time.time()

            nextAction = self.nextAction()
            self.actionIdx += 1

            if nextAction:
                # appel recursif d'incrementation des sequences

                curIdx = self.actionIdx

                def onTermination():

                    with ActionMutex():

                        if not nextAction.isSuccessful():
                            if nextAction.timeout:
                                nextAction.abort()
                                print 'aborted', nextAction, 'on timeout %.1f'%nextAction.timeout


                        if self.actionIdx == curIdx:
                            return self.incrementPos(interfaceMobile)

                nextAction.incrementItem(interfaceMobile, self.send_command_servo_service, onTerminationFunc=onTermination)

                if nextAction.timeout is not None:
                    Timer(nextAction.timeout, onTermination, ()).start()

            else:
                print
                print 'strategie terminee en %.1f'%(time.time() - self.timeStart)


    def flipSide(self):
        for action in self.actions:
            action.flipSide()

