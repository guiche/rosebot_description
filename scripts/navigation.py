#!/usr/bin/env python
"""
  navigation - converts a twist message to motor commands.  Needed for navigation stack
"""
import rospy
from geometry_msgs.msg import Twist
from rosebot_description.srv import E_Stop, StartGame, cible_commande, SendCommandServo

from beachbots import scenarios
from simulation2d import Simulation2d, mutexScope, Vec, coordsToVec, angleToVec


def sharpToString(req):
    out = ""
    if req.sensor_front_left:
        out += "left: % f "%req.sensor_front_left
    if req.sensor_front_right:
        out += "right: % f "%req.sensor_front_right
    if req.sensor_back:
        out += "back: % f"%req.sensor_back
    return out

class Navigation(Simulation2d):

    TEMPO_ARRET_URGENCE = 2  # en secondes

    def __init__(self, tempsLimite=91):
        """

        """
        Simulation2d.__init__(self)

        self.map.Courir = True
        self.twist_msg = Twist()

        self.pub_cmd_vel = rospy.Publisher(self.canal_cmd_vit, Twist, queue_size=1)

        self.TEMPS_IMPARTI = tempsLimite # en secondes
        self.tempsAuDemarrage = None
        self.temps_arret_urgence = None


        origAssBut = self.robot.AssBut

        def novAssBut(pos, **kwargs):
            with mutexScope():
                origAssBut(Vec(*pos), **kwargs)

        self.robot.AssBut = novAssBut

        emergency_stop_service_name = rospy.get_param('~emergency_stop_service_name', 'emergency_stop')
        start_game_service_name = rospy.get_param('~start_game_service_name', 'start_war')

        rospy.Service(emergency_stop_service_name, E_Stop, self.traiteSenseurIR)
        rospy.Service(start_game_service_name, StartGame, self.miseEnMarche)
        rospy.Service(self.canal_cible, cible_commande, self.cibleCallback)
        rospy.Service(self.canal_mock_sharp, E_Stop, self.traiteSenseurIR)

        # To avoid a dead-lock wait_for_service should be called after creating services needed by 'interface_acc' node
        



        self.send_command_servo_service_name = rospy.get_param('~send_command_servo_service_name', 'send_command_servo')
        self.send_command_servo_service = rospy.ServiceProxy(self.send_command_servo_service_name, SendCommandServo)



    def cibleCallback(self, msg):
        """ Methode appelee uniquement en mode test
            lorsque des objectifs de positions sont envoyes depuis la mini-carte.
        """

        if self.tempsAuDemarrage is None:
            # mise en marche par la mini-carte en mode test
            self.miseEnMarche(None)
            # ignorons la premiere cible au demarrage
            return True

        if msg.theta == self.ANGLE_INVALIDE:
            orient = None
        else:
            orient = angleToVec(msg.theta) * self.robot.rayon * 1.2

        coordsCible = coordsToVec([msg.x, msg.y])
        self.robot.AssBut(coordsCible, orient=orient, ajout=msg.ajout)
        #rospy.loginfo("-D-cibleCallback: %s" %(str(msg)))

        return True

    def getCmdVel(self):
        """ Calcul de la commande de vitesse """

        with mutexScope():
            # simulation avance de deltaT
            self.boucleIter.next()

            v = self.robot._v_ *  self.robot._orient_

            # pourquoi plafonner les vitesses explicitement ici ?
            if v > 0:
                v = min( self.robot.v_max, v)
            else:
                v = max(-self.robot.v_max, v)

            v *= 0.001

            w = self.robot.w
            if w > 0:
                w = min(w, self.robot.w_max)
            else:
                w = max(w, -self.robot.w_max)

            return v,w

    def traiteSenseurIR(self, req):
        """ arret du robot, par exemple en fin de partie """
        # elimine l'objectif du robot

        v_min_tol = 10 # cm / s
        vitesse = self.robot._v_ * self.robot._orient_

        needToStop = False

        if req.sensor_back:
            if vitesse > -v_min_tol:
                rospy.loginfo("Ignore sharp arriere : pas de deplacement vers l'arriere")
            else:
                needToStop = True

        if req.sensor_front_left or req.sensor_front_right:
            if vitesse < v_min_tol:
                rospy.loginfo("Ignore sharps avant : pas de deplacement vers l'avant")
            else:
                needToStop = True


        if needToStop:

            depla = 250 * self.robot._v_.uni

            depla_max, Obstacles = self.robot.PremierObstacle(depla, sideRadius=0.80*self.robot.sideRadius())

            if depla_max:
                print depla.nor, "max", depla_max.nor, "uni", depla.uni, "max uni", depla_max.uni

            if (depla_max is None or depla_max.nor > 1.01 * depla.nor) or not Obstacles:
                self.miseEnArret(req)
            else:
                rospy.loginfo("Obstacle plateau trouve : %s ; ignore sharps %s"%(Obstacles, sharpToString(req)))

        return True

    def miseEnArret(self, req):

        self.temps_arret_urgence = rospy.Time.now().secs
        rospy.loginfo("arret d'urgence : "+sharpToString(req))


    def miseEnMarche(self, req):
        """ Debut de partie """

        if self.send_command_servo_service is not None:
            rospy.loginfo("Wait for the acc card service '{}'".format(self.send_command_servo_service_name))
            rospy.wait_for_service(self.send_command_servo_service_name)

        if self.tempsAuDemarrage is None:
            self.tempsAuDemarrage = rospy.Time.now().secs

            # Deduction de la couleur a partir de la position au demarrage
            strat = scenarios.initStrategy(self.send_command_servo_service)

            if self.robot.pos[1] > 1500:
                self.robot.camp_ = self.robot.Camps[1]
                strat.flipSide()

            rospy.loginfo("========== Camp: %s ============"%self.robot.camp_)

            rospy.loginfo("================= DEBUT DE LA PARTIE  =================")

            strat.incrementPos(self.robot)

        return True

    def publie_cmd_vel(self, v, w):
        self.twist_msg.linear.x = v
        self.twist_msg.angular.z = w

        self.pub_cmd_vel.publish(self.twist_msg)
    
    def spinOnce(self):
        """ Methode appelee dans la boucle du noeud ROS """
        #############################################################

        if self.tempsAuDemarrage is not None:
            tempsEcoule = rospy.Time.now().secs - self.tempsAuDemarrage
            if self.TEMPS_IMPARTI is not None and tempsEcoule > self.TEMPS_IMPARTI:
                rospy.loginfo(">>>>>> FIN DE PARTIE apres %f sec <<<<<<<", tempsEcoule)
                # Call 'FUNNY ACTION' command service
                self.send_command_servo_service(9, 125)
                import time
                time.sleep(2)
                # Call 'FUNNY ACTION' command service to rearm
                self.send_command_servo_service(9, 35)

                self.tempsAuDemarrage = None

        if self.temps_arret_urgence is not None:
            if (rospy.Time.now().secs - self.temps_arret_urgence) > self.TEMPO_ARRET_URGENCE:
                self.temps_arret_urgence = None

        if self.tempsAuDemarrage is None or self.temps_arret_urgence is not None:
            v, w = 0, 0
        else:
            v, w = self.getCmdVel()

        self.publie_cmd_vel(v, w)
        
        

#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    node = Navigation()
    node.spin()
