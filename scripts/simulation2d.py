#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Quaternion
from nav_msgs.msg import Odometry
import math
from beachbots.plateau import Partie
from rosebotnav import Vec, angleToVec, vecToAngle
from PyKDL import Rotation as kdlRotation #@UnresolvedImport
from threading import RLock


class mutexScope:

    _mutex = RLock()
    
    def __enter__(self):
        self._mutex.acquire()
        
    def __exit__(self, type, value, traceback):
        self._mutex.release()


def quat_to_angle(quat):
    rot = kdlRotation.Quaternion(quat.x, quat.y, quat.z, quat.w)
    return rot.GetRPY()[2]


def coordsToVec(coords):
    """
        Conversion de coordonnees (en m) de la carte
        en coordonnees interne de l'IA du robot (en mm)
        pour la simulation
    """
    x, y = coords
    return Vec(x*1000, y*1000)


def vecToCoords(v, scale=.001):
    """
        conversion de mm en m (reciproque de coordsToVec)
    """
    return v*scale


class Simulation2d(object):

    ANGLE_INVALIDE = 999

    canal_cible     = rospy.get_param('canal_cible')
    canal_mock_sharp = "mock_sharp"
    canal_etat = "etat"
    canal_cmd_vit   = rospy.get_param('canal_cmd')
    canal_odom      = rospy.get_param('canal_odom')
    canal_pose_ra1  = rospy.get_param('canal_pose_ra1')
    canal_pose_ra2  = rospy.get_param('canal_pose_ra2')
    
    #############################################################
    def __init__(self, partie='robomovies_vide_blanc.par', Affiche=False):
    #############################################################
        rospy.init_node(self.__class__.__name__)
    
        self.map = Partie()#OuvrirPartie(nom=partie)

        self.map.Affiche = Affiche
        self.map.InitPreBoucle()
        self.map.AfficheMouvement = 3
        self.map.Const_IPS = False
        #self.map.Courir = False

        self.robot = iter(self.map.Selection).next()
        self.robot._print_log = False
        self.robot.detectionCollisionActive = False

        self.map.config_coquillage = int(rospy.get_param("/config_coquillage"))
        if not 0 <= self.map.config_coquillage <= 5:
            raise Exception("Mauvaise config coquillage : %d"%self.map.config_coquillage)

        rospy.loginfo(" ======= CONFIG COQUILLAGE : %d ========="%self.map.config_coquillage)

        self.robot.reinit(voie=rospy.get_param('wheelSeparation')*1000 )
        rospy.loginfo("robot.voie %f" % (self.robot.voie))
        
        rospy.Subscriber(self.canal_odom, Odometry, self.odomCallback)
        rospy.Subscriber(self.canal_pose_ra1, Odometry, self.odomCallbackRAFunc('robotAdverse1'))
        rospy.Subscriber(self.canal_pose_ra2, Odometry, self.odomCallbackRAFunc('robotAdverse2'))

        self.rate = rospy.get_param("~rate", rospy.get_param('updateRateCmdVel'))
        nodename = rospy.get_name()
        self.rateObj = rospy.Rate(self.rate)
        self.map.ImgParSec = self.rate # TODO set the rate based on elapsed time since last call for more accuracy
        self.boucleIter = self.map.boucleIter(exo_controle=1./self.rate)
        
        
        self.robotAverse1 = None
        self.robotAverse2 = None
        
        rospy.loginfo("%s started at rate %d" % (nodename, self.rate))

        
    #############################################################
    def spin(self):
    #############################################################
            
        ###### boucle principale ######
    
        while not rospy.is_shutdown():

            try:
                try:
                    self.spinOnce()
                except StopIteration:
                    break
                try:
                    self.rateObj.sleep()
                except rospy.exceptions.ROSTimeMovedBackwardsException: 
                    pass
            except rospy.exceptions.ROSInterruptException:
                break

    def spinOnce(self):
        raise NotImplementedError        
    

    #############################################################
    def odomCallback(self, msg, majVel=False):
    #######################################
        ######################
        #rospy.loginfo("-D- odomCallback: %s" % str(msg))
        
        with mutexScope():

            x, y = msg.pose.pose.position.x, msg.pose.pose.position.y

            # Filtrage de valeurs aberrantes envoyees par l'odometrie
            xMin, xMax = 0.01, 2
            yMin, yMax = 0.01, 3

            if not( xMin < x < xMax and yMin < y < yMax ):
                rospy.logerr("Ignore odom hors des limites de la carte [2mx3m] : %f, %f"%(x, y))
                return

            pos = coordsToVec((x, y))
            #rospy.loginfo("-D- odomCallback: correction pos %s" %( self.robot.pos - pos))

            self.map.Selection = set([self.robot])

            try:
                self.robot.pos = pos
            except:
                import traceback
                traceback.print_exc()
                rospy.logerr("Echec MaJ position robot par l'odom : %s %s"%(self.robot.pos, pos))
                return

            qt = Quaternion(msg.pose.pose.orientation.x,
                            msg.pose.pose.orientation.y,
                            msg.pose.pose.orientation.z,
                            msg.pose.pose.orientation.w)

            angle_orient = quat_to_angle(qt)
            nov_orient = angleToVec(angle_orient)

            self.robot._orient_ = nov_orient

            vdirecte = msg.twist.twist.linear.x * 1000.
            vnormale = msg.twist.twist.linear.y * 1000.
            
            vit = vdirecte * self.robot._orient_ + vnormale * self.robot._orient_.nml
            
            self.robot._eff_ = vit - self.robot._v_

            if majVel:
                self.robot._v_ = vit
                self.robot.w = msg.twist.twist.angular.z
            


    def odomCallbackRAFunc(self, nomRobotAdverse):

        def odomCallbackRA(msg):

            pos = coordsToVec([msg.pose.pose.position.x, msg.pose.pose.position.y])
            
            robot = getattr(self, nomRobotAdverse)
            
            if robot is None:
                robot = self.map.ClasseUnite(pos=pos, Reseau=self.map.graphe, taille=self.robot.rayon)
                setattr(self, nomRobotAdverse, robot)
                
            robot.pos = pos
        
        return odomCallbackRA 

        
#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    node = Simulation2d()
    node.spin()
