#!/usr/bin/env python

__author__ = 'vfdev'

# Mock of arduino card
# - IR rangefinder sensors
# - binary switch
# - starter

##
## Send
##  - key switch state : messages={'KONENO!,'KTWOWT!','KTHREERHT!'}
##  - starter state : message={'RINSIDE!', 'ROUTBOUND!'}
##  - IR rangefinder values : message={'I<Value one char><Value one char><Value one char>!'}
##
## Receive
##  - stop send "key switch state"/"starter state" : message='HSTOP!'
##  - servo motor commands : message={'A<value on one char>!', 'B<value on one char>!', 'C<value on one char>!'}

import signal
import rospy
import serial
import time

from dynamic_reconfigure.server import Server
from rosebot_description.cfg import MockAccCardConfig

MAX_REQUEST_NUMBER=10

class DriverNode():

    def send(self):

        if not self.received_starter_state:
            if self.key_switch_state == 0:
                self.ser.write("KONENO!")
            elif self.key_switch_state == 1:
                self.ser.write("KTWOWT!")
            elif self.key_switch_state == 2:
                self.ser.write("KTHREERHT!")

            if not self.starter_state:
                self.ser.write("RINSIDE!")
            else:
                self.ser.write("ROUTBOUND!")
        else:
            c1 = chr(self.ir_front_left)
            c2 = chr(self.ir_front_right)
            c3 = chr(self.ir_rear)
            print "IR values : {}, {}, {}".format(self.ir_front_left, self.ir_front_right, self.ir_rear)
            print "IR chr values : {}, {}, {}".format(c1, c2, c3)
            msg = "I{}{}{}!".format(c1, c2, c3)
            rospy.loginfo("IR msg to send : {}".format(msg))
            self.ser.write(msg)

    def receive(self):
        while True:
            try:
                line = self.ser.readline()
                if self.verbose:
                    rospy.loginfo(line)
                if len(line) < 3:
                    raise NameError('Message is too short : \'' + line + '\'')
                msgs = line.split("!")
                if len(msgs) == 1:
                    raise NameError('Message is incomplete : \'' + line + '\'')

                for msg in msgs:
                    if len(msg) == 0:
                        continue
                    if msg == 'HSTOP':
                        self.received_starter_state = True
                    else:
                        print "Unknown message :", msg
                        pass

            except (RuntimeError, TypeError, NameError, ValueError) as e:
                # import traceback
                # traceback.print_exc()
                rospy.logerr(e)
                return

    def keySwitchStateChanged(self, value):
        rospy.loginfo("Key switch state changed : {}".format(int(value)))
        self.key_switch_state = value

    def starterStateChanged(self, value):
        rospy.loginfo("Starter state changed : {}".format(int(value)))
        self.starter_state = value

    def irNewValues(self, v1, v2, v3):
        rospy.loginfo("IR new values : {}, {}, {}".format(v1,v2,v3))
        self.ir_front_left = v1
        self.ir_front_right = v2
        self.ir_rear = v3

    def callbackDynamicParam(self, config, level):
        self.keySwitchStateChanged(config['key_switch_state'])
        self.starterStateChanged(config['starter_state'])
        self.irNewValues(config['ir_front_left'], config['ir_front_right'], config['ir_rear'])
        return config

    def stop_node_handler(self, *args, **kwargs):
        self.running = False
        rospy.signal_shutdown("Shutdown the node")
        self.ser.flush()
        self.ser.close()

    def __init__(self):

        self.running = False
        signal.signal(signal.SIGINT, self.stop_node_handler)

        rospy.loginfo("Launching mock accessory card")
        rospy.init_node('mock_acc_card', anonymous=True, disable_signals=True)

        rate = rospy.get_param('~rate', 5)
        serial_port = rospy.get_param('~port', '/dev/tnt1')
        serial_baud = rospy.get_param('~baud', 9600)

        # Define initial states:
        self.key_switch_state = 0
        self.starter_state = False
        self.ir_front_left = 0
        self.ir_front_right = 0
        self.ir_rear = 0

        self.received_starter_state = False

        self.verbose = rospy.get_param('~verbose', False)

        rospy.loginfo( "Ready to listen service")
        rate = rospy.Rate(rate)

        Server(MockAccCardConfig, self.callbackDynamicParam)

        self.ser = serial.Serial(
            port=serial_port,
            baudrate=serial_baud,
            xonxoff=False,
            timeout=0.005
        )

        rospy.loginfo("Openning port ")
        self.ser.close()
        self.ser.open()
        self.ser.flush()
        rospy.loginfo("Port Open  -- Soft Reseting")
        time.sleep(0.1)

        rospy.loginfo("Openning port...")
        self.ser.close()
        self.ser.open()
        self.ser.flush()
        self.running = True

        while self.running:

            if self.verbose:
                rospy.loginfo("==================================")
            self.receive()
            rate.sleep()  # free the cpu
            self.send()


if __name__ == '__main__':

    try:
        ne = DriverNode()
    except rospy.ROSInterruptException:
        pass
