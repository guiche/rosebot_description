
import sys
import json
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
steamHandle = logging.StreamHandler()
steamHandle.setFormatter(logging.Formatter('%(levelname)8s :: %(message)s'))
logger.addHandler(steamHandle)

# from robots import GenericRobot
# from robots.concurrency import action
# from robots.resources import lock
# from robots.resources import Resource
# from robots.concurrency.signals import ActionCancelled


import strategy_actions
import strategy_conditions


def normalize(action_name):
    return action_name.lower().replace(' ', '_')

def check_action_valid(action):
    assert len(action) == 2, \
        "Action should be a vector of two values : [\"action name\", {\"params\":[1,2,3], \"conditions\":[c1, c2]}]"

def check_actions_conditions_consistency(goals):
    def check_conditions_consistency(conditions):
        for condition in conditions:
            condition_name = normalize(condition[0])
            assert hasattr(strategy_conditions, condition_name), \
                "Condition name '%s' is not defined in strategy_conditions.py" % condition_name
    for goal in goals:
        for action in goal['actions']:
            check_action_valid(action)
            action_name = normalize(action[0])
            assert hasattr(strategy_actions, action_name), \
                "Action name '%s' is not defined in strategy_actions.py" % action_name
            try:
                check_conditions_consistency(action[1]['conditions'])
            except KeyError:
                pass
        check_conditions_consistency(goal['conditions'])




if __name__=="__main__":

    if len(sys.argv) != 2:
        logger.error("No strategy goals provided. Set 1st argument as a path to a strategy json file")
        exit(1)

    strategy_goals_file = sys.argv[1]
    strategy_goals = None
    try:
        f = open(strategy_goals_file)
        strategy_goals = json.load(f)
        f.close()
    except IOError:
        logger.error("Failed to open strategy goals file '%s'" % strategy_goals_file)
        exit(1)
    except ValueError as e:
        logger.error("Failed to parse strategy goals file '%s'" % strategy_goals_file)
        logger.error("Parse error : %s" % e)
        exit(1)

    check_actions_conditions_consistency(strategy_goals['goals'])

    # main loop
    try:
        for goal in strategy_goals['goals']:
            print "- Execute : ", goal['name']
            for action in goal['actions']:
                check_action_valid(action)
                foo = getattr(strategy_actions, normalize(action[0]))
                foo(action[1])
    except KeyError as e:
        logger.error("Strategy goals problem : key '%s' is not found" % e.message)
