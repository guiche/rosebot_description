#!/usr/bin/env python

from navigation import Navigation, Twist
from simulation2d import vecToCoords, coordsToVec, vecToAngle, Odometry
from rosebot_description.srv import SendCommandServo
import rospy
import tf
import strategy
import time
from rosebot_description.msg import etat_robot



class MockNavigation(Navigation):


    def __init__(self):

        Navigation.__init__(self)

        self.robot.detectionCollisionActive = True
        self.odom_msg = Odometry()

        self.pub_odom = rospy.Publisher(self.canal_odom, Odometry, queue_size=1)
        self.pub_etat = rospy.Publisher(self.canal_etat, etat_robot, queue_size=1)

        rospy.Subscriber(self.canal_cmd_vit, Twist, self.cmdVelCallBack)

        if time.time() % 2:
            self.robot.pos = strategy.flipSidePos(self.robot.pos)
            self.robot._orient_[1] *= -1

        # pas de temps limite pour la simulation
        self.TEMPS_IMPARTI = None

        #self.send_command_servo_service = None

        rospy.Service(self.send_command_servo_service_name, SendCommandServo, self.send_command_servo)

    def send_command_servo(self, req):
        self.robot.servo_bras = req.value_servo
        return True


    def publieEtatRobot(self):
        msg = etat_robot(self.robot.servo_bras)
        self.pub_etat.publish(msg)

    def cmdVelCallBack(self, msg):

        # pos information
        pos = vecToCoords(self.robot.pos)
        self.odom_msg.pose.pose.position.x = pos[0]
        self.odom_msg.pose.pose.position.y = pos[1]

        qt = tf.transformations.quaternion_from_euler(0, 0, vecToAngle(self.robot._orient_))

        self.odom_msg.pose.pose.orientation.x = qt[0]
        self.odom_msg.pose.pose.orientation.y = qt[1]
        self.odom_msg.pose.pose.orientation.z = qt[2]
        self.odom_msg.pose.pose.orientation.w = qt[3]

        # Vel information
        v = msg.linear.x
        w = msg.angular.z

        self.odom_msg.twist.twist.linear.x = v #self.robot._v_ * self.robot._orient_ / 1000.
        self.odom_msg.twist.twist.linear.y = 0 #self.robot._v_ * self.robot._orient_.nml / 1000.
        self.odom_msg.twist.twist.angular.z = w

        self.pub_odom.publish(self.odom_msg)

    #############################################################
    def odomCallback(self, msg):
        #######################################

        v_idea = self.robot._v_ * self.robot.orient_
        w_idea = self.robot.w

        Navigation.odomCallback(self, msg, majVel=True)

        if self.robot._print_log and False:  # ne pas afficher pour le noeud carte commande
            print 'odom ideal %.3f' % v_idea, 'w %.3f' % w_idea
            print 'odom reele %.3f' % vdirecte, 'w %.3f' % self.robot.w
            print

    def spinOnce(self):

        super(MockNavigation, self).spinOnce()
        self.publieEtatRobot()

if __name__ == '__main__':

    MockNavigation().spin()
