__author__ = 'vfdev'

from functools import wraps
import errno
import os
import signal
import time


class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    """
    Function decorator to setup function execution timeout
    Usage :

        @timeout(seconds=5)
        def my_long_running_function(*args, **kwargs)

    :param seconds:
    :param error_message:
    :return:
    """
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result
        return wraps(func)(wrapper)
    return decorator


# Profile decorator
def time_profiler(enabled=True):
    """
    Function decorator to mesure time passed in function
    Usage :
        @time_profiler(enabled=True)
        def my_long_running_function(*args, **kwargs)
    :param enabled:
    :return:
    """
    def _time_profiler(input_func):
        def output_func(*args, **kwargs):
            if enabled:
                start_time = time.time()
            input_func(*args, **kwargs)
            if enabled:
                elapsed_time = time.time() - start_time
                print "PROFILER : {} elapsed={}".format(input_func.__name__, elapsed_time)
        return output_func
    return _time_profiler




########################################################################
# Temporary here
import signal

class Timeouter:

    def __init__(self):
        signal.signal(signal.SIGALRM, self._internal_handler)
        self.duration = 0
        self.callback = None

    def _internal_handler(self, *args, **kwargs):
        self.stop()
        self.callback()

    def start(self):
        if self.duration > 0 and self.callback is not None:
            signal.alarm(self.duration)

    def stop(self):
        signal.alarm(0)

########################################################################