#!/usr/bin/env python


import signal
import rospy
import time

from rosebot_description.srv import E_Stop, ChangeColor, StartGame


MAX_REQUEST_NUMBER = 5

class DriverNode():


    def requestKeySwitchStateChange(self, value):
        # value == 0 -> green color
        # value == 1 -> purple color
        if self.verbose:
            rospy.loginfo("Request Key Switch State Change : {}".format(value))
        count = MAX_REQUEST_NUMBER
        while count > 0:
            try:
                self.change_color(value)
                time.sleep(0.5)
                break
            except (rospy.ServiceException, rospy.ROSSerializationException, TypeError) as e:
                if self.verbose:
                    rospy.loginfo("Service call failed: %s" % e)
                count-=1


    def requestStartGame(self):
        if self.verbose:
            rospy.loginfo("Request Start Game")
        count = MAX_REQUEST_NUMBER
        while count > 0:
            try:
                self.start_game(True)
                time.sleep(0.5)
                return True
            except rospy.ServiceException, e:
                if self.verbose:
                    rospy.loginfo("Service call failed: %s" % e)
            count-=1
        return False


    def requestEmergencyStop(self, d1, d2, d3):
        if self.verbose:
            rospy.loginfo("Request Emergency Stop : {}, {}, {}".format(d1, d2, d3))
        count = MAX_REQUEST_NUMBER
        while count > 0:
            try:
                self.emergency_stop(d1, d2, d3, True)
                time.sleep(0.5)
                break
            except rospy.ServiceException, e:
                if self.verbose:
                    rospy.loginfo("Service call failed: %s" % e)
                count-=1


    def stop_node_handler(self, *args, **kwargs):
        self.running = False
        rospy.signal_shutdown("Shutdown the node")

    def __init__(self):

        self.running = False
        signal.signal(signal.SIGINT, self.stop_node_handler)

        rospy.loginfo("Launching accessory card driver node")
        rospy.init_node('test_acc_card_driver', anonymous=True, disable_signals=True)

        change_color_service_name = '/rosebot1/change_color'
        emergency_stop_service_name = '/rosebot1/emergency_stop'
        start_game_service_name = '/rosebot1/start_game'

        # Define initial states:
        self.key_switch_state = None
        self.starter_state = None
        self.game_is_started = False

        self.thr_front_left = 150
        self.thr_front_right = 150
        self.thr_rear = 150

        self.verbose = True

        rospy.loginfo("Wait for services : {}, {}, {}".format(emergency_stop_service_name, start_game_service_name, change_color_service_name))
        rospy.wait_for_service(change_color_service_name)
        rospy.wait_for_service(emergency_stop_service_name)
        rospy.wait_for_service(start_game_service_name)

        self.change_color = rospy.ServiceProxy(change_color_service_name, ChangeColor)
        self.emergency_stop = rospy.ServiceProxy(emergency_stop_service_name, E_Stop)
        self.start_game = rospy.ServiceProxy(start_game_service_name, StartGame)

        rospy.loginfo( "Ready to listen service")
        rate = rospy.Rate(2)

        self.running = True

        color_state = 0
        count = 0
        while self.running:
            print "count : ", count
            self.requestKeySwitchStateChange(color_state)
            color_state = 1 if color_state == 0 else 0
            rate.sleep()  # free the cpu
            count += 1


if __name__ == '__main__':

    try:
        ne = DriverNode()
    except rospy.ROSInterruptException:
        pass

