#!/usr/bin/env python

# Interface with arduino card
# - IR rangefinder sensors
# - binary switch
# - starter
# - servos

import signal
import rospy
import time
import mutex
import IOBoardComm_v1

from std_msgs.msg import Int16

from rosebot_description.srv import E_Stop, ChangeColor, StartGame, SendCommandServo
# from dynamic_reconfigure.server import Server
# from rosebot_description.cfg import DistanceCalibrationConfig
import yaml

MAX_REQUEST_NUMBER=1

class DriverNode:

    # THIS METHOD IS PEP8 COMPATIBLE !!!
    def check_for_start_and_color(self):
        color = None
        starter_state = None
        if self.mutexLock.testandset():
            # get color switch state :
            color = self.io_board_comm.readINPin(self.ports['COLOR_SWITCH_PIN'])
            # get starter state :
            starter_state = self.io_board_comm.readINPin(self.ports['STARTER_PIN'])
            self.mutexLock.unlock()

        if color is not None:
            self.interpretColorSwitchState(color)

        if starter_state is not None:
            self.interpretStarterState(starter_state)

    def check_ir_sensors(self):

        ir_fl, ir_fr, ir_rc = None, None, None
        if self.mutexLock.testandset():
            ir_fl = self.io_board_comm.readINPin(self.ports['IR_FRONT_LEFT_PIN'])
            ir_fr = self.io_board_comm.readINPin(self.ports['IR_FRONT_RIGHT_PIN'])
            ir_rc = self.io_board_comm.readINPin(self.ports['IR_REAR_CENTER_PIN'])
            self.mutexLock.unlock()

        if ir_fl is None and ir_fr is None and ir_rc is None:
            rospy.logerr("No IR values received!")
            return

        ir_fl = 0 if ir_fl is None else ir_fl
        ir_fr = 0 if ir_fr is None else ir_fr
        ir_rc = 0 if ir_rc is None else ir_rc
        self.interpretIRData(ir_fl, ir_fr, ir_rc)
        self.publishIRData(ir_fl, ir_fr, ir_rc)

    def interpretColorSwitchState(self, data):
        if data == 0:
            if self.verbose:
                rospy.loginfo("Color switch state is 0")
            if self.color_switch_state != 0:
                self.color_switch_state = 0
                self.requestColorSwitchStateChange(self.color_switch_state)
        elif data == 1:
            if self.verbose:
                rospy.loginfo("Color switch state is 1")
            if self.color_switch_state != 1:
                self.color_switch_state = 1
                self.requestColorSwitchStateChange(self.color_switch_state)
        else:
            rospy.logerr("Color switch state is not recognized")

    def requestColorSwitchStateChange(self, value):
        # value == 0 -> green color
        # value == 1 -> purple color
        if self.verbose:
            rospy.loginfo("Request Key Switch State Change")
        count = MAX_REQUEST_NUMBER
        while count > 0:
            try:
                self.change_color(value)
                time.sleep(0.5)
                break
            except (rospy.ServiceException, rospy.ROSSerializationException, TypeError) as e:
                if self.verbose:
                    rospy.loginfo("Service call failed: %s" % e)
                count -= 1

    def interpretStarterState(self, data):
        if data == 0:
            if self.verbose:
                rospy.loginfo("Starter state is 'inside'")
            self.is_starter_armed = True
        elif data == 1:
            if self.verbose:
                rospy.loginfo("Starter state is 'outside'")
            if not self.game_is_started and self.is_starter_armed:
                self.game_is_started = self.requestStartGame()
                self.is_starter_armed = False
        else:
            rospy.logerr("Starter state is not recognized")

    def requestStartGame(self):
        if self.verbose:
            rospy.loginfo("Request Start Game")
        count = MAX_REQUEST_NUMBER
        while count > 0:
            try:
                self.start_game(True)
                return True
            except (rospy.ServiceException, rospy.ROSSerializationException, TypeError) as e:
                if self.verbose:
                    rospy.loginfo("Service call failed: %s" % e)
            count-=1
        return False

    def interpretIRData(self, d1, d2, d3):
        if self.verbose:
            rospy.loginfo("IR sensors : {}, {}, {}".format(d1, d2, d3))
        if d1 > self.thr_front_left or \
            d2 > self.thr_front_right or \
             d3 > self.thr_rear:
            d1 = float(d1 > self.thr_front_left)
            d2 = float(d2 > self.thr_front_right)
            d3 = float(d3 > self.thr_rear)
            self.requestEmergencyStop(d1, d2, d3)

    def requestEmergencyStop(self, d1, d2, d3):
        if self.verbose:
            rospy.loginfo("Request Emergency Stop : {}, {}, {}".format(d1, d2, d3))
        count = MAX_REQUEST_NUMBER
        while count > 0:
            try:
                self.emergency_stop(d1, d2, d3, True)
                time.sleep(0.5)
                break
            except (rospy.ServiceException, rospy.ROSSerializationException, TypeError) as e:
                if self.verbose:
                    rospy.loginfo("Service call failed: %s" % e)
                count -= 1

    def publishIRData(self, d1, d2, d3):
        self.ir_publisher_fl.publish(Int16(d1))
        self.ir_publisher_fr.publish(Int16(d2))
        self.ir_publisher_rc.publish(Int16(d3))

    # def callbackDynamicParam(self, config, level):
    #     #rospy.loginfo("""Reconfiugre Request: {int_param}, {double_param},\ {str_param}, {bool_param}, {size}""".format(**config))
    #     #print config
    #
    #     if (config['reset_odom'] == True):
    #         self.sendReset()
    #
    #     if (config['motor_ctrl'] == True):
    #         self.enablePower()
    #     else :
    #         self.disablePower()
    #
    #     self.setPIDLorder(config['KpPoLi'],config['KiPoLi'],config['KdPoLi'])
    #     self.setPIDAorder(config['KpPoAng'],config['KiPoAng'],config['KdPoAng'])
    #
    #     return config

    def setup_ir_calibration_data(self, ir_calib_file):
        self.ir_calib_data = load_yaml(ir_calib_file)
        if len(self.ir_calib_data) == 0:
            rospy.loginfo("Setup default calibration")
            self.ir_calib_data = {
                'front_left':
                    {'distance': [999, 0.35, 0.3, 0.25, 0.2, 0.15, 0.1],
                      'signal': [30, 40, 75, 92, 120, 140, 205]},
                'front_right':
                    {'distance': [999, 0.35, 0.3, 0.25, 0.2, 0.15, 0.1],
                     'signal': [30, 40, 75, 92, 120, 140, 205]},
                'rear_center':
                    {'distance': [999, 0.35, 0.3, 0.25, 0.2, 0.15, 0.1],
                     'signal': [30, 40, 75, 92, 120, 140, 205]}
            }
        # print self.ir_calib_data

    def setup_ports(self, ac_ports_file):
        self.ports = load_yaml(ac_ports_file)
        if len(self.ports) == 0:
            rospy.loginfo("Take default port configuration")
            self.ports['COLOR_SWITCH_PIN'] = 4
            self.ports['STARTER_PIN'] = 7
            self.ports['ARM_PIN'] = [3, 55, 180]
            self.ports['LATCH_LEFT_PIN'] = [5, 5, 130]
            self.ports['LATCH_RIGHT_PIN'] = [6, 30, 165]
            self.ports['FUNNY_ACTION_PIN'] = [9, 35, 125]
            self.ports['IR_FRONT_LEFT_PIN'] = 14
            self.ports['IR_FRONT_RIGHT_PIN'] = 15
            self.ports['IR_REAR_CENTER_PIN'] = 16
            self.ports['BATTERY_PIN'] = 17

        if self.verbose:
            rospy.loginfo("Ports : {}".format(self.ports))

    def configure_pins(self):

        self.io_board_comm.enterConfigMode()
        self.io_board_comm.setPinPurpose(self.ports['COLOR_SWITCH_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['STARTER_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['ARM_PIN'][0], IOBoardComm_v1.SERVOMSG)
        self.io_board_comm.setPinPurpose(self.ports['LATCH_LEFT_PIN'][0], IOBoardComm_v1.SERVOMSG)
        self.io_board_comm.setPinPurpose(self.ports['LATCH_RIGHT_PIN'][0], IOBoardComm_v1.SERVOMSG)
        self.io_board_comm.setPinPurpose(self.ports['IR_FRONT_LEFT_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['IR_FRONT_RIGHT_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['IR_REAR_CENTER_PIN'], IOBoardComm_v1.INMSG)
        self.io_board_comm.setPinPurpose(self.ports['FUNNY_ACTION_PIN'][0], IOBoardComm_v1.SERVOMSG)
        self.io_board_comm.exitConfigMode()

    def stop_node_handler(self, *args, **kwargs):
        self.running = False
        rospy.signal_shutdown("Shutdown the node")

    def reinit_servos(self):
        rospy.loginfo("Reinit servos")
        # Close latches
        class Req:
            def __init__(self, pin, value):
                self.pin_nb = pin
                self.value_servo = value
            
        req = Req(11, self.ports['LATCH_LEFT_PIN'][2])
        status = self.send_command_servo(req)
        # reinit funny action lock
        req = Req(self.ports['FUNNY_ACTION_PIN'][0], self.ports['FUNNY_ACTION_PIN'][1])
        status &= self.send_command_servo(req)
        return status    
	
    
    def send_command_servo(self, req):  
        """
        Callback to handle 'Send a command to a servo'.
        argument req containts
        :param pin_nb: pin to command
            pin_nb < 10 => unit servo command
            pin_nb == 11 => both latches
        :param value_servo: value of the command
            for pin == 11, value_servo corresponds to values of LATCH_LEFT_PIN
        :return status: status of the command : true if send, false otherwise
        """
        count = 10
        while not self.mutexLock.testandset() and count > 0:
            time.sleep(0.01)
            count -= 1

        if count == 0 and self.verbose:
            rospy.logerr('Failed to acquire the mutex while calling servo command service')
            return False

        pin_nb, value_servo = req.pin_nb, req.value_servo
        status = False
        servo_pin = [self.ports['ARM_PIN'][0],
                     self.ports['LATCH_LEFT_PIN'][0],
                     self.ports['LATCH_RIGHT_PIN'][0],
                     self.ports['FUNNY_ACTION_PIN'][0],
                     # composed pins :
                     self.ports['LATCH_LEFT_PIN'][0] + self.ports['LATCH_RIGHT_PIN'][0] # = 11 means both latches
                     ]

        if pin_nb not in servo_pin:
            rospy.logwarn("Pin number asked ({0}) is not a servo (servo pin : {1})".format(pin_nb, servo_pin))
        else:

            if pin_nb < 10:
                # unit pin action
                status = self.io_board_comm.setServoPos(pin_nb, value_servo)
            elif pin_nb == 11:

                def y(x, xmin, xmax, ymin, ymax):
                    return int((x-xmin)*1.0/(xmax-xmin)*(ymin-ymax)) + ymax
                value_servo_left = value_servo
                value_servo_right = y(value_servo,
                                      self.ports['LATCH_LEFT_PIN'][1],
                                      self.ports['LATCH_LEFT_PIN'][2],
                                      self.ports['LATCH_RIGHT_PIN'][1],
                                      self.ports['LATCH_RIGHT_PIN'][2])

                status = self.io_board_comm.setServoPos(self.ports['LATCH_LEFT_PIN'][0], value_servo_left)
                status &= self.io_board_comm.setServoPos(self.ports['LATCH_RIGHT_PIN'][0], value_servo_right)

            if status:
                rospy.loginfo("set {0} on pin {1}".format(value_servo, pin_nb))
            else:
                rospy.logerr("Error occured setting servo position")
        self.mutexLock.unlock()
        return status

    def __init__(self):

        self.running = False
        self.mutexLock = mutex.mutex()

        signal.signal(signal.SIGINT, self.stop_node_handler)

        rospy.loginfo("Launching accessory card driver node")
        rospy.init_node('acc_card_driver', anonymous=True, disable_signals=True)

        rate = rospy.get_param('~rate', 5)
        serial_port = rospy.get_param('~port', '/dev/ttyUSB0')
        serial_baud = rospy.get_param('~baud', 9600)

        ir_signal_topic =  rospy.get_param('~ir_signal_topic', 'ir_signal')

        change_color_service_name = rospy.get_param('~change_color_service_name', 'change_color')
        emergency_stop_service_name = rospy.get_param('~emergency_stop_service_name', 'emergency_stop')
        start_game_service_name = rospy.get_param('~start_game_service_name', 'start_war')
        ir_calib_file = rospy.get_param('~ir_calib_file', '')
        ac_ports_file = rospy.get_param('~ac_ports_file', '')

        send_command_servo_service_name = rospy.get_param('~send_command_servo_service_name', 'send_command_servo')
        rospy.Service(send_command_servo_service_name, SendCommandServo, self.send_command_servo)

        # Define initial states:
        self.color_switch_state = None
        self.starter_state = None
        self.is_starter_armed = False
        self.game_is_started = False

        self.verbose = rospy.get_param('~verbose', False)

        self.thr_front_left = rospy.get_param('~threshold_front_left', 150)
        self.thr_front_right = rospy.get_param('~threshold_front_right', 150)
        self.thr_rear = rospy.get_param('~threshold_rear', 150)

        # load serial ports conf:
        self.setup_ports(ac_ports_file)

        # load ir calibration data:
        self.setup_ir_calibration_data(ir_calib_file)

        rospy.loginfo("Wait for services : {}, {}, {}".format(emergency_stop_service_name, start_game_service_name, change_color_service_name))
        rospy.wait_for_service(change_color_service_name)
        rospy.wait_for_service(emergency_stop_service_name)
        rospy.wait_for_service(start_game_service_name)

        self.change_color = rospy.ServiceProxy(change_color_service_name, ChangeColor)
        self.emergency_stop = rospy.ServiceProxy(emergency_stop_service_name, E_Stop)
        self.start_game = rospy.ServiceProxy(start_game_service_name, StartGame)

        rospy.loginfo("Ready to listen service")
        rate = rospy.Rate(rate)

        # Server(DistanceCalibrationConfig, self.callbackDynamicParam)

        rospy.loginfo("Openning port ...")
        self.io_board_comm = IOBoardComm_v1.IOBoardComm()
        if not self.io_board_comm.initBoard(serial_port, serial_baud):
            rospy.logerr("Port is not open")
            return

        self.configure_pins()

        # configure ir publisher
        self.ir_publisher_fl = rospy.Publisher(ir_signal_topic + '_fl', Int16, queue_size=10)
        self.ir_publisher_fr = rospy.Publisher(ir_signal_topic + '_fr', Int16, queue_size=10)
        self.ir_publisher_rc = rospy.Publisher(ir_signal_topic + '_rc', Int16, queue_size=10)

		# Initialise servos :
        if not self.reinit_servos():
		    rospy.logerror('Failed to initialize servos !!!')


        self.running = True
        rospy.loginfo("Port is open and init done.")

        while self.running and not self.game_is_started:
            if self.verbose:
                rospy.loginfo("====== Game not started ==========")

            # Mutex is acquired inside the function
            #if self.mutexLock.testandset():
            self.check_for_start_and_color()
            #self.mutexLock.unlock()
            rate.sleep()  # free the cpu

        # The game is surely started
        while self.running:

            if self.verbose:
                rospy.loginfo("==================================")

            # Mutex is acquired inside the function
            #if self.mutexLock.testandset():
            self.check_ir_sensors()
            #self.mutexLock.unlock()
            rate.sleep()  # free the cpu

        self.io_board_comm = None


def load_yaml(filename):
    try:
        f = open(filename)
        data = yaml.safe_load(f)
        f.close()
        return data
    except IOError:
        rospy.logerr("Failed to load file : {}".format(filename))
        return {}




if __name__ == '__main__':

    try:
        ne = DriverNode()
    except rospy.ROSInterruptException:
        pass
