#!/usr/bin/env python
#

## Simple talker demo that listens to motor board messages and publish them on ROS workspace 

import serial
import time
import signal
# import numpy
from datetime import datetime

from collections import deque
import platform

#from mot_rosbot.srv import *


class DriverNode(object):
        
    def __init__(self):
        
        self.odom_linear_x = 0.0
        self.odom_linear_y = 0.0
        self.odom_angular_theta = 0.0
    
        self.odom_linear_x_speed = 0.0
        self.odom_linear_y_speed = 0.0
        self.odom_angular_theta_speed = 0.0
        
        self.twist_x_speed = 0.0
        self.twist_theta_speed = 0.0

        self.STOP_NODE = False
        self.incomming_message_type = None
        self.sign = False    
        self.inputString = ''

        self.TIME_BUFFER = deque(maxlen=50)
        
        self.TIME_DELTA = 0.050

        self.TIME_STAMP = self.START_TIME = datetime.now()


        # Initializing the port
        if platform.system() == 'Windows':
            Ports = 'COM1', 'COM2', 'COM3', 'COM4', 'COM5', 'COM6'
        else:
            Ports = ['/dev/ttyACM0']
        
        for port in Ports:
            try:
                print port
                self.ser = serial.Serial(
                    port = port,
                    baudrate = 115200,
                    #parity=serial.PARITY_ODD,
                    #stopbits=serial.STOPBITS_TWO,
                    #bytesize=serial.EIGHTBITS,
                    #xonxoff = serial.XOFF,
                    timeout = 0.001
                )
            except serial.SerialException:
                continue
            break
    
        if False:
            self.ser.close()
            self.ser.open()
            self.ser.flush()
            rospy.loginfo("Port Open -- Soft Reseting") 
            self.sendReset()
            time.sleep(1)
            
            rospy.loginfo("Openning port...")
            self.ser.close()
            self.ser.open()
            self.ser.flush()
    
    def loginfo(self, info):
        print info
        
    def spinOnce(self, message_i=0):
        
        #self.sendTtwist(0.5)
        #self.sendVtwist(0.2)
        
        while self.ser.in_waiting > 0:            
            
            test = self.ser.read()
            
            if self.handleData(test):
                break

        self.sendTtwist(message_i)
        self.sendVtwist(message_i)

    def spin(self):
        
        message_i = 0
        while not self.STOP_NODE:
            
            time.sleep(0.1)
            message_i+=1
            self.spinOnce(message_i)
    
            
    def sendXorder(self, Xorder):
        self.ser.write('X'+str(Xorder)+'!')
        
    def sendYorder(self, Yorder):
        self.ser.write('Y'+str(Yorder)+'!')
        
    def sendAorder(self, Torder):
        self.ser.write('A'+str(Torder)+'!')
            
    def sendLspeed(self, Lspeed):
        self.ser.write('L'+str(Lspeed)+'!')
        
    def sendRspeed(self, Rspeed):
        self.ser.write('R'+str(Rspeed)+'!')    
    
    def sendTtwist(self, Twist):
        self.ser.write('T'+str(int(Twist*10000.0))[0:6]+'!')
        #print "sent"+str(int(Twist*10000))
        
    def sendVtwist(self, Vtwist):    
        self.ser.write('V'+str(int(Vtwist*10000.0))[0:6]+'!')

    def sendStart(self):
        self.ser.write('U1!')
        rospy.loginfo("Robot Start")

    def sendStop(self):
        self.ser.write('U0!')
        rospy.loginfo("Robot Stop")
        
    def sendReset(self):
        self.ser.write('S0!')
        rospy.loginfo("Robot Reset")
        
        
    def sendKpPLorder(self, param):        
        self.ser.write('{'+str(int(param*1000))+'!')    
    def sendKdPLorder(self,param):        
        self.ser.write('}'+str(int(param*1000))+'!')
    def sendKiPLorder(self,param):        
        self.ser.write('^'+str(int(param*1000))+'!')
    def sendKiPLSorder(self,param):        
        self.ser.write('='+str(int(param*1000))+'!')
        
    def sendKpPAorder(self,param):        
        self.ser.write('('+str(int(param*1000))+'!')
    def sendKdPAorder(self,param):        
        self.ser.write(')'+str(int(param*1000))+'!')
    def sendKiPAorder(self,param):        
        self.ser.write('_'+str(int(param*1000))+'!')
    def sendKiPASorder(self,param):        
        self.ser.write('|'+str(int(param*1000))+'!')
        
    
    def handleData(self, c):
                
    #     print "char :"+c
    
        if self.incomming_message_type:
            
            if len(self.inputString) < 16:
                if c.isdigit(): # if the character received is a number                    
                    self.inputString += c                 
    #                 print "char"  
                
                elif c == '!':   #if the character received is end of the packet
                    self.interpret_data()
                    self.incomming_message_type = None
                    #pc.printf("\n"); 
    #                 print "finish"  
    #                 print "end"
                    return True
                
                elif c == '-':
                    self.sign = True
                
                elif self.incomming_message_type == 'D':
                    self.inputString += c 
                    
                else:   #character not recognized, we cancel
                    self.incomming_message_type = None
                    print "def_unrecognzed packet ID", c
                    
            else:
                #default, packet overwhelmed
                print "def_packet_size_overwhelmed", self.incomming_message_type, self.inputString
                self.incomming_message_type = None
                
        else: #if it is the first bit of the packet, check if it is a standard message
            if c in 'XYALRTDKGH':
                self.incomming_message_type = c
                self.sign = False    
                self.inputString = ''
        
                # pc.printf("u"); 
                
            else:
                pass #error

        return False
    
    
    def interpret_data(self):

    #     last_time = 0
    #     millis = int(round(time.time() * 1000))
    #     print (millis-last_time)  
    #     last_time = millis
                    
    #     if it as debug message print it as reveived
        if self.incomming_message_type == 'D':
            #print "Debug"
            if True:
                print 'DEBUG', self.inputString
                
            elif rospy.get_param('~debug'):
                print colored (self.inputString,'blue')

        #else if it is a com message, interpret it...
        else:
            
            #print 'input string', self.incomming_message_type, self.inputString
            
            incom_data = 0
            
            for char in self.inputString:              
                incom_byte = ord(char)-48;       
                
                if incom_byte >= 0 and incom_byte < 10:
                    incom_data = incom_data * 10 + incom_byte
                
                else:
                    print "default nbr interpretation"
                    pass# default nbr receive

            print self.incomming_message_type, incom_data
            if self.sign:
                incom_data = -incom_data
                
            if self.incomming_message_type == 'X':
                self.odom_linear_x = incom_data/100.0
                
            elif self.incomming_message_type == 'Y':
                self.odom_linear_y  = incom_data/100.0
                
            elif self.incomming_message_type == 'A':
                self.odom_angular_theta = incom_data/100.0
                
            elif self.incomming_message_type == 'K':
                self.odom_linear_x_speed = incom_data/10000.0
                            
            elif self.incomming_message_type == 'G':
                self.odom_linear_y_speed  = incom_data/10000.0
                
            elif self.incomming_message_type == 'H':
                self.odom_angular_theta_speed = incom_data/100.0                

            elif self.incomming_message_type == 'L':
                self.Lspeed = incom_data
            #             print "L" + str(Lspeed)
            elif self.incomming_message_type == 'R':
                self.Rspeed = incom_data
            #             print "R" + str(Rspeed)
            elif self.incomming_message_type == 'T':
                #self.odom_linear_x = incom_data/10
                pass
            #             print incom_data
            elif self.incomming_message_type == 'V':
                pass
                #self.odom_linear_y  = incom_data/10
            elif self.incomming_message_type == 'S':
                self.Status = incom_data
            
            else:
                raise Exception('Invalid message type:' + self.incomming_message_type)
                
    def stop_node(self, *args, **kwargs):
        print self.TIME_BUFFER
        
        self.STOP_NODE = True
        
        rospy.signal_shutdown("Manual shutdown the node")
        self.ser.flush()
        self.ser.close()
        
        print "Fin du noeud"
    

    def callbackAsserCoeffs(self, data):
        
        value = data.uint
        
        if value == ord('c'):
            self.sendPIDCoeffs(gainsPL, gainsPA)
            print "Coeffs sent"

            
    def sendPIDCoeffs(self, gainsPL, gainsPA):
                        
        self.sendKpPLorder(gainsPL['p'])    
        self.sendKdPLorder(gainsPL['d'])        
        self.sendKiPLorder(gainsPL['i'])        
        self.sendKiPLSorder(gainsPL['s'])        
        self.sendKpPAorder(gainsPA['p'])     #15.15    
        self.sendKdPAorder(gainsPA['d'])        
        self.sendKiPAorder(gainsPA['i'])        
        self.sendKiPASorder(gainsPA['s'])        
        

class DriverNodeROS(DriverNode):
    
    def __init__(self):
        
        from termcolor import colored

        import tf
        import rospy
        
        from std_msgs.msg import String, Int8
        from geometry_msgs.msg import Twist, Point, Quaternion
        from nav_msgs.msg import Odometry
        import PyKDL as kdl

        signal.signal(signal.SIGINT, self.stop_node)
        
        rospy.loginfo("Launching motor board driver node")
        rospy.init_node('motor_driver', anonymous=True, disable_signals=True)
                
        rospy.loginfo("Openning port")
        
        DriverNode.__init__(self)
                        
        rospy.loginfo("Port Open -- Sending Coeff") 
        #full_param_name = rospy.search_param('gainsPL')   ##search path of a parameter by its name
        #param_value = rospy.get_param(full_param_name)
        gainsPL = rospy.get_param('~gainsPL')
        gainsPA = rospy.get_param('~gainsPA')
        self.sendPIDCoeffs(gainsPL, gainsPA)

        rospy.loginfo("Coeff sent")   

        #pub = rospy.Publisher('chatter', String, queue_size=10)
        #pub_odom = rospy.Publisher('mot_rosbot/Odometry', Twist, queue_size=10)
        _pub_odom = rospy.Publisher('/rosebot1/odom', Odometry, queue_size=10)
        rospy.Subscriber('/rosebot1/cmd_vel', Twist, self.callbackTwist)
        rospy.Subscriber('/key', Int8,  self.callbackAsserCoeffs )
        
    
    def loginfo(self, info):
        rospy.loginfo(info)


    #function to be called when a twist message is available
    def callbackTwist(self, data):

        self.twist_x_speed = data.linear.x
        self.twist_theta_speed = data.angular.z
        
        #rospy.loginfo("GotIT")
        #rospy.loginfo(data.linear.x)

        nowTime = datetime.now() #rospy.Time().now().to_sec()
        delta = nowTime - self.TIME_STAMP
        self.TIME_STAMP = nowTime

        if delta < self.TIME_DELTA:
            return    

        if not self.STOP_NODE:
            self.sendVtwist(self.twist_x_speed)
            self.sendTtwist(self.twist_theta_speed)
            #rospy.loginfo("sysRdy")
        else:
            pass

        self.TIME_BUFFER.append(nowTime - self.START_TIME)
            
                    
    #############################################################
    def spin(self):
    #############################################################
            
        ###### main loop ######
        tf_br = tf.TransformBroadcaster()
        
        msg = Odometry()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = '/odom' # i.e. '/odom'
        msg.child_frame_id = '/world' # i.e. '/base_footprint'
        
        rate = rospy.Rate(30) # 10hz

        #while not rospy.is_shutdown() and not sigterm_stop:
        while not self.STOP_NODE:
        
            #self.sendTtwist(0.5)
            #self.sendVtwist(0.2)    
            
            DriverNode.spinOnce(self)
    
    #prepare the odom message     
            msg.header.stamp = rospy.Time.now()
            msg.pose.pose.position.x = self.odom_linear_x
            msg.pose.pose.position.y = self.odom_linear_y            
            msg.pose.pose.orientation = Quaternion(*(kdl.Rotation.RPY(0, 0, self.odom_angular_theta).GetQuaternion()))

            msg.twist.twist.linear.x = self.odom_linear_x_speed
            msg.twist.twist.linear.y = self.odom_linear_y_speed    
            msg.twist.twist.angular.z = self.odom_angular_theta_speed    
            

    ## publish the tf for rviz and further debug
            tf_br.sendTransform((    self.odom_linear_x, 
                                    self.odom_linear_y, 
                                    0),
                                (    msg.pose.pose.orientation.x,
                                    msg.pose.pose.orientation.y,
                                    msg.pose.pose.orientation.z,
                                    msg.pose.pose.orientation.w),
                                 rospy.Time.now(),
                                 "RoseBot",
                                 "world")
                
            pub_odom.publish(msg) #publish the odom message
            rate.sleep()  # free the cpu
            
        
if __name__ == '__main__':

    ne = DriverNode()
    try:
        ne.spin()
    except:
        raise
    #except rospy.ROSInterruptException: 
        pass
