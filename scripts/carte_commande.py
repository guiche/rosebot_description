#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Pose2D, Twist
from simulation2d import Simulation2d, vecToCoords, coordsToVec, angleToVec
import pygame
import math
from rosebot_description.srv import cible_commande, E_Stop
from rosebot_description.msg import etat_robot

class CarteCommande(Simulation2d):

    #############################################################
    def __init__(self):
    #############################################################

        Simulation2d.__init__(self, partie='robomovies_vide.par', Affiche=True)

        self.map.nom = 'Carte Commande'

        self.map.ImgParSec = self.rate
        
        # queue_size=1 : seul le dernier objectif importe
        #self.pub_cible = rospy.Publisher(self.canal_cible, Pose2D, queue_size=1)
        
        RobotClass = type(self.robot)
        origAssBut = RobotClass.AssBut

        self.saute_cible_init = True

        def novAssBut(agent, pos=None, **kwargs):
            # assigne le but et le publie sur le canal ROS

            import pygame

            if pygame.key.get_pressed()[pygame.K_SPACE]:
                self.pubSharp(1,1,1)
                return

            if self.saute_cible_init:
                # publication de cible bidon pour declencher le demarrage de la partie
                self.saute_cible_init = False
            else:
                origAssBut(agent,pos,**kwargs)

            orient = kwargs.get('orient')
            if orient is None:
                theta = self.ANGLE_INVALIDE
            else:
                theta = math.atan2(orient[1],orient[0])
            
            ajout = kwargs.get("ajout", False)

            if pos is not None:
                self.pubCible(vecToCoords(pos), theta=theta, ajout=ajout)
            
        RobotClass.AssBut = novAssBut

        def ControleDirection(robot, avantArriere, gaucheDroite, deltaT):

            w = robot.w -robot.w_max * gaucheDroite
            v = robot.v_max * avantArriere
            
            self.publishVelTwist(v, w)

        RobotClass.ControleDirection = ControleDirection

        rospy.Subscriber(self.canal_etat, etat_robot, self.etatCallback)

        #rospy.Subscriber(self.canal_cmd_vit, Twist, self.callbackCmdVel)
        #self.cmdPlot = CmdPlot()

        if False:
            positions = [((.970, 1.100), math.pi * 3 / 4.), ((.970, .610), math.pi / 2)]
            for posn, angle in positions:
                self.robot.AssBut(coordsToVec(posn), ajout=True, orient=angleToVec(angle))

    def etatCallback(self, msg):
        self.robot.servo_bras = msg.servo_bras

    def odomCallback(self, msg):
        Simulation2d.odomCallback(self, msg, majVel=True)


    def spinOnce(self):
        self.boucleIter.next()


    #######################################################
    def pubSharp(self, left, right, back):
    #######################################################

        rospy.wait_for_service(self.canal_mock_sharp)
        cible_proxy = rospy.ServiceProxy(self.canal_mock_sharp, E_Stop)
        cible_proxy(left, right, back, True)

    #######################################################
    def pubCible(self, pos, theta, ajout):
    #######################################################
        
        rospy.wait_for_service(self.canal_cible)
        cible_proxy = rospy.ServiceProxy(self.canal_cible, cible_commande)
        cible_proxy(pos[0], pos[1], theta, ajout)

    
    def callbackCmdVel(self, data):
                
        self.cmdPlot.update(data.linear.x)
        #self.cmdPlot.update(data.angular.z)
        
    
#############################################################
#############################################################
if __name__ == '__main__':
    """ main """
    node = CarteCommande()
    node.spin()
