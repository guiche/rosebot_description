#!/usr/bin/env python

__author__ = 'vfdev'

# Node to start rosebot services : change_color_service, emergency_stop_service, start_game_service


import rospy
from rosebot_description.srv import E_Stop, StartGame, ChangeColor

def emergencyStopRequest(req):

    rospy.loginfo("- Emergency stop service is requested : args: {}, {}, {}".format(req.sensor_front_left, req.sensor_front_right, req.sensor_back, req.e_stop))
    rospy.loginfo("-- Emergency stop service returns True")
    return True

def startGameRequest(req):
    rospy.loginfo("- Start game service is requested : args: {}".format(req.start_game))
    rospy.loginfo("-- Start game service returns True")
    return True

def changeColorRequest(req):
    rospy.loginfo("Change color service is requested : args: {}".format(req.team_color))
    rospy.loginfo("Change color service returns True")
    return True

if __name__ == "__main__":

    rospy.init_node('Mock services')

    emergency_stop_service_name = rospy.get_param('~emergency_stop_service_name', 'emergency_stop')
    start_game_service_name = rospy.get_param('~start_game_service_name', 'start_war')
    change_color_service_name = rospy.get_param('~change_color_service_name', 'change_color')


    rospy.loginfo("Start services : {}, {}, {}".format(emergency_stop_service_name, start_game_service_name, change_color_service_name))

    rospy.Service(emergency_stop_service_name, E_Stop, emergencyStopRequest)
    rospy.Service(start_game_service_name, StartGame, startGameRequest)
    rospy.Service(change_color_service_name, ChangeColor, changeColorRequest)

    rospy.spin()
